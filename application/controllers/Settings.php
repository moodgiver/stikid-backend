<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class  Settings extends CI_Controller {

  function __construct(){
		parent::__construct();
    date_default_timezone_set("Europe/Rome");
    $this->load->model('Site_model');
		$this->load->model('Order_model');
		$this->load->model('User_model');
		$this->load->model('App_model');
    $this->load->model('Vendite_model');
		$this->load->config('menu');
		$this->load->config('application');
    $this->load->config('schema');
		$this->load->library('uuid');
		$this->load->library('form_validation');
	}

  function index(){
    $action = $_POST['action'];
    $app = $this->config->item('application');
    $data['menus'] = $app['menus'];
    $data['siti'] = $app['siti'];

    switch ( $action ){
      case 'settings-generali':
      $data['settings'] = $this->App_model->settings_generali();
      $this->load->view('settings/settings',$data);
      break;

      case 'settings-save-setting':
      echo $this->App_model->save_setting();
      break;


      case 'settings-meta':
      $data['settings'] = $this->App_model->settings_meta();
      $this->load->view('settings/meta',$data);
      break;

      case 'setting-field-save':
      echo $this->App_model->setting_field_save();
      break;

      case 'settings-prezzi-sticker':
        $data['mode'] = 'prezzi-sticker';
        $data['prezzi'] = $this->Vendite_model->prezzi();
        $this->load->view('settings/prezzi_sticker.php',$data);
      break;

      case 'settings-save-prezzo':
        echo $this->App_model->save_prezzo();
      break;

      case 'settings-fornitori':
        $data['mode'] = 'settings-fornitori';
        $data['action'] = 'settings-save-fornitore';
        $data['fornitori'] = $this->Vendite_model->fornitori();
        $data['schema'] = $this->config->item('schema');
        $data['name'] = 'fornitori';
        if ( isset($_POST['id']) ){
          $data['fornitore'] = $this->Vendite_model->fornitore($_POST);
          //$data['costi'] = $this->Vendite_model->costi($_POST);
        }
        $this->load->view('settings/fornitore.php',$data);
      break;

      case 'settings-save-fornitore':
        echo $this->App_model->save_settings_data();
      break;

      case 'settings-costi-fornitori':
        $data['mode'] = 'settings-costi-fornitori';
        $data['fornitori'] = $this->Vendite_model->fornitori();
        if ( isset($_POST['id']) ){
          $data['mode'] = 'settings-costi-fornitori';
          $data['action'] = 'settings-save-costo-fornitore';
          $data['schema'] = $this->config->item('schema');
          $data['name'] = 'costi-fornitori';
          $data['costi'] = $this->Vendite_model->costi($_POST);
        }
        $this->load->view('settings/costi_fornitori.php',$data);
      break;

      case 'settings-save-costo-fornitore':
        echo $this->App_model->save_settings_data();
      break;

      case 'settings-spedizioni':
        $data['mode'] = 'settings-spedizioni';
        $data['action'] = 'settings-save-spedizione';
        $data['schema'] = $this->config->item('schema');
        $data['name'] = 'spedizioni';
        $data['spedizioni'] = $this->Vendite_model->spedizioni();
        $this->load->view('settings/spedizioni.php',$data);
      break;

      case 'settings-save-spedizione':
        echo $this->App_model->save_settings_data();
      break;

      case 'settings-coupons':
        $data['mode'] = 'settings-coupon';
        $data['action'] = 'settings-save-coupon';
        $data['schema'] = $this->config->item('schema');
        $data['name'] = 'coupons';
        $data['coupons'] = $this->Vendite_model->coupons();
        if ( !isset($_POST['search']) ){
          $_POST['search'] = '';
        }
        if ( isset($_POST['id']) ){
          $data['cp'] = $this->Vendite_model->coupon();
          $this->load->view('settings/coupon.php',$data);
        } else {
          $this->load->view('settings/coupon.php',$data);
        }
      break;

      case 'settings-coupons-search':
        $search = $this->Vendite_model->coupon_search();
        foreach ( $search AS $s ){
          echo '<a href="#" class="coupon-data" data-id="'.$s['id_coupon'].'">'.$s['ac_coupon'].'</a><br>';
        }
      break;

      case 'settings-save-coupon':
        $this->Vendite_model->coupon_save();
        $data['cp'] = $this->Vendite_model->coupon();
        $data['mode'] = 'settings-coupon';
        $data['action'] = 'settings-save-coupon';
        $data['schema'] = $this->config->item('schema');
        $data['name'] = 'coupons';
        $data['coupons'] = $this->Vendite_model->coupons();
      break;


    }
  }


}
