<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class  Ajax extends CI_Controller {

  function __construct(){
 		parent::__construct();
		date_default_timezone_set("Europe/Rome");
    $this->load->model('Site_model');
    $this->load->library('uuid');
  }

  function index(){
    $form = $_POST;
    $mode = $_POST['mode'].split('-');

    switch ($mode[0]){

      case 'ordini':

        switch ($mode[1]){
          case 'ricevuti':
          break;

        }

      break;

      case 'calcolo_prezzo':
        echo calcolo_prezzo ( $form['w'] , $form['h'] , $form['cs'] );
      break;

      case 'session_addtocart':
        $items = $this->session->cart_items;
        $cart_item = array (
          'id'          => $form['id_prodotto'],
          'prodotto'    => $form['prodotto'],
          'w'           => $form['width'],
          'h'           => $form['height'],
          'prezzo'      => $form['prezzo'],
          'colore'      => $form['colore'],
          'collezione'  => $form['collezione'],
          'qty'         => 1,
          'font'        => $form['font'],
          'testo'       => $form['testo'],
          'thumb'       => $form['thumb'],
          'sconto'      => $form['sconto'],
          'telaio'      => $form['telaio'],
          'uuid'        => $this->uuid->v4()

        );

        array_push($items,$cart_item);
        $this->session->set_userdata('cart_items',$items);
        $this->ricalcola_totali();

      break;

      case 'cart-item-qty':
        $items = $_SESSION['cart_items'];
        $renewItems = [];
        //unset($_SESSION['cart_items']);
        $pos = 0;
        foreach ( $items AS $item ){
          if ( $item['uuid'] == $form['uuid'] ){
            if ( (int)$form['qty'] > 0 ){
              $cart_item = array (
              'id'          => $item['id'],
              'prodotto'    => $item['prodotto'],
              'w'           => $item['w'],
              'h'           => $item['h'],
              'prezzo'      => $item['prezzo'],
              'colore'      => $item['colore'],
              'collezione'  => $item['collezione'],
              'qty'         => (int)$form['qty'],
              'font'        => $item['font'],
              'testo'       => $item['testo'],
              'thumb'       => $item['thumb'],
              'sconto'      => $item['sconto'],
              'telaio'      => $item['telaio'],
              'uuid'        => $item['uuid']
              );
              array_push ( $renewItems , $cart_item );
            }
          } else {
            array_push ( $renewItems , $item );
          }

        }
        $this->ricalcola_totali();
        $this->session->set_userdata('cart_items', $renewItems );

      break;

      case 'coupon':
        echo coupon($form['coupon']);
        $ordine = $this->session->order;
        $ordine['COUPON'] = $form['coupon'];
        $ordine['COUPON_VALUE'] = coupon($form['coupon']);
        $this->session->set_userdata('order',$ordine);
        $this->ricalcola_totali();

      break;

      case 'spedizioni':
        $spedizione = calcolo_spedizioni($form['totale']);
        echo $spedizione;
        $ordine = $this->session->order;
        $ordine['SHIPPING'] = $spedizione;
        $this->session->set_userdata('order',$ordine);
        $this->ricalcola_totali();
      break;

      case 'register':
        //check is user exists
        $isUser = false;
        $isUser = check_register($_POST);
        echo $isUser;
      break;


    }

  }

  

  public function ricalcola_totali(){
    $totale = 0;
    $carrello = 0;
    foreach ( $this->session->cart_items AS $item ){
      $carrello += (float)$item['prezzo']*(float)$item['qty'];
    }
    $ordine = $this->session->order;

    $ordine['CART_TOTAL'] = (float)$carrello;
    $spedizione = calcolo_spedizioni($carrello);
    $coupon_value = coupon((float)$this->session->order['COUPON_VALUE']);
    $ordine['COUPON_VALUE'] = $coupon_value;
    $totale = $carrello + $spedizione - $coupon_value;
    $ordine['ORDER_TOTAL'] = $totale;
    $this->session->set_userdata('order',$ordine);
  }

  public function clear_cart_items(){
		$items = [];
		$this->session->set_userdata('cart_items', $items );
	}

  public function clear_session(){
    $this->session->sess_destroy();
    redirect(base_url(), 'refresh');
  }

}
