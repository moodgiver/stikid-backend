<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class  Main extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

  function __construct(){
 		parent::__construct();
		header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		date_default_timezone_set("Europe/Rome");
    $this->load->model('Site_model');
		$this->load->model('Order_model');
		$this->load->model('User_model');
		$this->load->model('App_model');
		$this->load->config('menu');
		$this->load->config('application');
		$this->load->library('uuid');
		$this->load->library('form_validation');
		//$this->session->sess_destroy();
		$this->check_session();

  }

	public function index()
	{
		//load common data queries
		$data = common_data ();
		//mode homepage
		$data['mode'] = 'home';
		//load slides
		$data['fornitori'] = $this->App_model->fornitori();
		$data['ordini'] = $this->Order_model->ordini(1);
		$data['emails'] = $this->App_model->email_logs();
		if ( !$this->session->user['islogged'] ){
			$data['mode'] = 'login';
			$this->load->view('login');
		} else {
			$this->load->view('main',$data);
		}
	}

	public function collection($collezione){
		$data = common_data ();
		$data['mode'] = 'collezione';
		if ( $collezione != 'Sticky-Text' ){
			//query collezione (vecchio nome per mantenere backlink)
			$data['prodotti_collezione'] = $this->Site_model->collezione($collezione);
		} else {
			$data['mode'] = 'sticky-text';
			$data['stickytext_description'] = $this->Site_model->stickytext_description();
			$data['fonts'] = $this->Site_model->fonts();
			$data['colori'] = $this->Site_model->colori();
		}
		$this->load->view('main',$data);

	}

	public function collection_new($collezione){
		$data = common_data ();
		$data['mode'] = 'collezione';
		//query collezione (nuovi nomi)
		$data['prodotti_collezione'] = $this->Site_model->collezione_new($collezione);
		$this->load->view('main',$data);
	}

	public function quadriperbambini($collezione){
		$data = common_data ();
		$data['mode'] = 'collezioneqpb';
		$data['prodotti_collezione'] = $this->Site_model->collezione_qpb($collezione);
		$this->load->view('main',$data);
	}

	public function prodotto($id){
		$data = common_data();
		$data['mode'] = 'prodotto';
		$data['prodotto'] = $this->Site_model->prodotto($id);
		$data['upsell'] = $this->Site_model->prodotti_upsell($id);
		$data['descrizione'] = $this->Site_model->prodotto_descrizione($id);
		$data['gallery'] = $this->Site_model->prodotto_gallery($id);
		$data['colori'] = $this->Site_model->colori();
		$this->load->view('main',$data);
	}

	public function prodottoqpb($id){
		$data = common_data();
		$data['mode'] = 'prodottoqpb';
		$data['prodotto'] = $this->Site_model->prodotto($id);
		$data['descrizione'] = $this->Site_model->prodotto_descrizione($id);
		$data['gallery'] = $this->Site_model->prodotto_gallery($id);
		$this->load->view('main',$data);
	}

	public function offerte(){
		$data = common_data();
		$data['mode'] = 'offerte';
		$data['offerte'] = $this->Site_model->offerte();
		$this->load->view('main',$data);
	}

	public function simulatore($id){
		$data = common_data();
		$data['mode'] = 'simulatore';
		$data['prodotto'] = $this->Site_model->prodotto($id);
		$this->load->view('main',$data);
	}

	public function upload_foto(){
		$tempFile = $_FILES['file']['tmp_name'];
		$myFile = explode ('.' , $_FILES['file']['name']);
		$fileName = $this->session->__ci_last_regenerate.'.'.$myFile[count($myFile)-1];
		$targetPath = getcwd() . "/public/users/upload/temp/";
		$targetFile = $targetPath . $fileName ;
		$this->session->set_userdata('simulatore' , base_url().'public/users/upload/temp/'.$fileName );
		move_uploaded_file($tempFile, $targetFile);
	}

	public function delete_temp_image(){

	}

	public function cart(){
		$data = common_data();
		$data['mode'] = 'cart';
	  $data['view'] = 'detail';
		$this->load->view('main',$data);
	}

	public function cart_spedizione(){
		$data = common_data();
		$data['mode'] = 'cart';
	  $data['view'] = 'spedizione';
		$data['province'] = province();
		if ( count($this->session->cart_items) > 0 ){
			if ( $this->session->user['islogged'] ){
				if ( $this->session->order['id'] == '' ){
					$data['ordine_nr'] = ordine_nr();
					$this->set_session_ordernr($data["ordine_nr"]);
				}
				$this->load->view('main',$data);
			} else {
				$this->load->view('main',$data);
			}
		} else {
			redirect(base_url());
		}
	}

	public function cart_checkout(){
		$data = common_data();
		$data['mode'] = 'cart';
	  $data['view'] = 'checkout';
		if ( $this->session->user['islogged'] &&  count($this->session->cart_items) > 0 ){

			if ($this->form_validation->run('profile') == TRUE){
				$data['view'] = 'spedizione';
				$data['province'] = province();
				$this->load->view('main',$data);
			} else {
				$this->User_model->save_customer();
				$this->Order_model->save_preorder();
				$this->load->view('main',$data);
			}
		} else {
			redirect(base_url());
		}
	}

	public function profile_rules(){

	}

	public function bonifico_conferma(){
		$data = common_data();
		$data['mode'] = 'order';
		$data['view'] = 'bonifico';
		if ( $this->session->order['id'] != '' ){
			$this->Order_model->save_order('BONIFICO');
			if ( send_order_confirm('PAYPAL') ){
				$this->load->view('main',$data);
				$this->reset_order_session();
				$items = [];
				$this->session->set_userdata('cart_items', $items );
			}
		} else {
			redirect(base_url());
		}
 	}

	public function paypal_conferma(){
		$data = common_data();
		$data['mode'] = 'order';
		$data['view'] = 'bonifico';
		if ( $this->session->order['id'] != '' ){

			$this->Order_model->save_order('PAYPAL');
			if ( send_order_confirm('PAYPAL') ){
				$this->load->view('main',$data);
				$this->reset_order_session();
				$items = [];
				$this->session->set_userdata('cart_items', $items );
			}
		} else {
			redirect(base_url());
		}
 	}

	function paypal($str){
		$data = common_data();
		$data['mode'] = 'cart';
		$data['view'] = 'paypal_cancel';
		if ( $this->session->order['id'] != '' ){
			$this->load->view('main',$data);
		} else {
			redirect(base_url());
		}
	}

	function paypal_notify(){
		return true;
	}

	function registrazione(){
		$data = common_data();
	  $data['mode'] = 'registrazione';
	  $data['view'] = 'modulo';
		$data['is_user'] = check_register($_POST['email']);
		$data['profilo_salvato'] = false;
		$data['province'] = province();
		$this->load->view('main',$data);
	}

	function registrami(){
		$data = common_data();
	  $data['mode'] = 'registrazione';
	  $data['view'] = 'modulo';
		$data['profilo_salvato'] = true;
		$data['province'] = province();
		$registrazione = $this->User_model->registra_utente();
		if ( $registrazione ){
				send_email_registrazione();
				if ( $_POST['redirect'] != '' ){
					redirect(base_url().''.$_POST['redirect'] );
				} else {
					$this->load->view('main',$data);
				}

		}

	}

	function profilo($str){
		$data = common_data();
		$data['mode'] = 'profilo';
		$data['view'] = $str;
		$data['province'] = province();
		$data['profilo_salvato'] = false;
		if ( $str == 'ordini' ){
			$data['ordini'] = $this->User_model->ordini_utente();
		}
		if ( $str == 'salva' ){
			if ($this->form_validation->run('profile') == FALSE){
				$data['profilo_salvato'] = true;
				$this->User_model->save_customer();
			}
			$this->load->view('main',$data);
		}
		if ( $this->session->user['islogged'] ){
			$this->load->view('main',$data);
		} else {
			redirect(base_url());
		}
	}

	function ordine($nr,$anno){
		if ( $this->session->user['islogged'] ){
			$ordine_nr = $nr.'/'.$anno;
			$data = common_data();
			$data['mode'] = 'profilo';
			$data['view'] = 'ordine';
			$data['ordine'] = $this->User_model->ordine($ordine_nr);
			$this->load->view('main',$data);
		} else {
			redirect(base_url());
		}
	}

	function pagina($titolo,$id){
		$data = common_data();
		$data['mode'] = 'pagina';
		$data['view'] = 'normale';
		$data['pagina'] = $this->Site_model->pagina($id);
		if ( $data['pagina'][0]['ac_link'] == 'designers' ){
			$data['view'] = 'designers';
			$data['designers'] = $this->Site_model->designers();
		}
		$this->load->view('main',$data);
	}

	function designer($nome,$id){
		$data = common_data();
		$data['mode'] = 'designer';
		$data['view'] = 'scheda';
		$data['designer'] = $this->Site_model->designer($id);
		$this->load->view('main',$data);
	}

	function ricerca(){
		$data = common_data();
		$data['mode'] = 'search';
		$data['view'] = 'ricerca';
		$data['collezione'] = $this->Site_model->search();
		print_r ( $data[0]['collezione'] );
		$this->load->view('main',$data);
	}

	function send_mail(){
		$this->load->library('email'); // load email library
		$this->email->from('no-reply@sticasa.com', 'STICASA / STIKID');
		//$this->email->bcc('tantipremi@indual.it');
		$this->email->to('swina.allen@gmail.com');
		$this->email->subject("ORDINE STICASA/STIKID");
		$message = "TEST";
		$this->email->message($message);
		$noerr = $this->email->send();
		if ( $noerr ){
			echo 'mail test OK';
		} else {
			echo $noerr;
		}
	}


	private function check_session(){
		//imposta sessioni
		if ( !$this->session->userdata('user') ){
			$session_data = array (
				'islogged' => false,
				'email' => '',
				'customer' => '',
				'customer_id' => 0
			);
			$this->session->set_userdata('user', $session_data);
		}
		if ( !$this->session->userdata('order') ){
			$this->create_order_session();
		}
		if ( !$this->session->userdata('cart_items') ){
			$items = [];
			$this->session->set_userdata('cart_items', $items );
		}
	}

	private function create_order_session(){

		$session_order = array(
			'OS_AZIENDA'		=> '',
			'OS_CAP'				=> '',
			'OS_CITTA' 			=> '',
			'OS_COGNOME'		=> '',
			'OS_EMAIL' 			=> '',
			'OS_INDIRIZZO' 	=> '',
			'OS_MOBILE' 		=> '',
			'OS_NAZIONE'		=> '',
			'OS_NOME' 			=> '',
			'OS_PV' 				=> '',
			'OS_TELEFONO' 	=> '',
			'O_AZIENDA' 		=> '',
			'O_CAP' 				=>'',
			'O_CITTA' 			=> '',
			'O_COGNOME' 		=> '',
			'O_EMAIL' 			=> '',
			'O_INDIRIZZO' 	=> '',
			'O_MOBILE' 			=> '',
			'O_NAZIONE' 		=> '',
			'O_NOME' 				=> '',
			'O_PV' 					=> '',
			'O_TELEFONO' 		=> '',
			'SHIPPING' 			=> 0,
			'id' 						=> '',
			'SHIPTO' 				=> false,
			'CART_TOTAL' 		=> 0,
			'COUPON' 				=> '&nbsp;',
			'COUPON_VALUE' 	=> 0,
			'MESSAGE' 			=> '',
			'ORDER_TOTAL' 	=> 0,
			'FATTURA' 			=> false,
			'CF' 						=> '',
			'PIVA' 					=> '',
			'REGALO'				=> false,
			'BIGLIETTO_TESTO'	=> '',
			'MESSAGE' 			=> '',
			'ISCART'				=> 1,
			'items' 				=> []
		);
		$items = [];
		$this->session->set_userdata('order', $session_order);
	}

	private function reset_order_session(){
		$ordine = $this->session->order;
		$ordine['id'] = '';
		$ordine['ORDER_TOTAL'] 	= 0;
		$ordine['SHIPPING'] 		= 0;
		$ordine['SHIPTO'] 			= false;
		$ordine['CART_TOTAL'] 	= 0;
		$ordine['COUPON']				= '';
		$ordine['COUPON_VALUE'] = 0;
		$ordine['MESSAGE'] 			=	0;
		$ordine['FATTURA'] 			= false;
		$this->session->set_userdata('order', $ordine);
	}

	function upload($mode){

		$tempFile = $_FILES['file']['tmp_name'];
		$myFile = explode ('.' , $_FILES['file']['name']);
		$fileName = $myFile[count($myFile)-1];

		$app = $this->config->item('application');
		$workPath = $app['temp_image_path'];
		$workFile = $workPath.$myFile[0].'.'.$fileName;
		move_uploaded_file($tempFile,$workFile);

		$targetPath = $_POST['folder'];

		if ( $targetPath == 'slider' ){
			$this->load->config('application');
			$app = $this->config->item('application');
			$path = $app['slides_path'];
			$uri = $app['slides_uri'];
			$targetPath = $path;
			$targetFile = $targetPath . $myFile[0].'.'.$fileName;
			copy($workFile, $targetFile);
		}

    if ( $targetPath == 'banner' ){
			$this->load->config('application');
			$app = $this->config->item('application');
			$path = $app['banners_path'];
			$uri = $app['banners_uri'];
			$targetPath = $path;
			$targetFile = $targetPath . $myFile[0].'.'.$fileName;
			copy($workFile, $targetFile);
		}

		echo 'Uploading image '.$mode;
	}

	private function set_session_ordernr($nr){
		$ordine_data = $this->session->order;
		$ordine_data['id'] = $nr;
		$this->session->set_userdata('order',$ordine_data);
		return true;
	}

	private function prodImage($lista){
		$aImg = explode(',',$lista);
		return $aImg[0];
	}

	function debug(){
		$this->load->view('debug');
	}
}
