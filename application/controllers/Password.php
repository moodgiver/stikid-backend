<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Password extends CI_Controller {


  function __construct(){
 		parent::__construct();
		date_default_timezone_set("Europe/Rome");
    $this->load->model('Site_model');
		$this->load->model('User_model');
		$this->load->library('uuid');
  }

  function reset(){
    $data = common_data();
		$data['mode'] = 'password';
	  $data['view'] = 'reset';
		$this->load->view('main',$data);
  }

}
