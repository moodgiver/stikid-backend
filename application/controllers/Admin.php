<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class  Admin extends CI_Controller {

  function __construct(){
		parent::__construct();
    date_default_timezone_set("Europe/Rome");
    $this->load->model('Site_model');
		$this->load->model('Order_model');
		$this->load->model('User_model');
		$this->load->model('App_model');
		$this->load->config('menu');
		$this->load->config('application');
		$this->load->library('uuid');
		$this->load->library('form_validation');
	}

  function index(){
    echo 'nothing to see here';
  }

  function cron($action){
    //cron jobs (uri must be submitted as /cron/cb366bm/job_da_eseguire)
    switch ( $action ){

      case 'tracking':
        $this->cb366bm_cron_tracking();
      break;

      case 'test':
      $time = getdate();
      log_message('ERROR','Lanciato CRON DI TEST OK! ===='.$time['hours'].':'.$time['minutes']);
      break;

      default:
      echo 'nothing to see here';
      break;


    }
  }

  function cb366bm_cron_tracking(){
    $data['ordini'] = $this->App_model->ordini_tracking();
    //echo 'sending tracking mail<br>';
    //print_r($_SESSION);
    $this->cb366bm_cron_email_tracking_cliente($data);
    //email_tracking_cliente();
  }

  function cb366bm_cron_email_tracking_cliente($ordine){
    $righe = $ordine['ordini'];
    $app = $this->config->item('application');
    $msg = $app['email_tracking'];
    foreach ( $righe AS $row ){
      $to = $row['ac_email'];
      $nrordine = explode('/',$row['ac_nrordine']);
      $msg = str_replace('_numero_ordine_',$nrordine[0],$msg);
      $data = array (
        'titolo'  => 'AVVISO SPEDIZIONE',
        'from'    => 'servizioclienti@sticasa.com',
        'to'      => $row['ac_email'],
        'bcc'     => 'servizioclienti@sticasa.com,5698be0bd7@invite.trustpilot.com,swina.allen@gmail.com',
        'subject' => 'Spedizione ordine STICASA/STIKID nr. '.$row['ac_nrordine'],
        'msg'     => $msg.'<br>Email inviata a: '.$row['ac_email'],
        'ordine'  => $row['ac_nrordine']
      );
      $result = general_email_sender($data);
      if ( $result ){
        $this->Order_model->aggiorna_ordine_status($row['ac_nrordine'],5);
      }
    }
  }

  function cb366bm_cron_databackup(){
    $app = $this->config->item('application');
    $data = common_data ();
    if ( !$this->session->user['islogged'] ){
      $data['mode'] = 'login';
			$this->load->view('login');
    } else {

    set_time_limit(300);
    // Load the DB utility class
    $this->load->dbutil();

    $prefs = array(
        'tables'        => array('tbl_ordini', 'tbl_ordini_summary','tbl_ordini_temp','tbl_ordini_summary','tbl_nrordini','tbl_registrazioni','tbl_prodotti','tbl_categorie'),   // Array of tables to backup.
        'format'        => 'txt',                       // gzip, zip, txt
        'filename'      => 'stikid_databackup.sql',              // File name - NEEDED ONLY WITH ZIP FILES
        'add_drop'      => TRUE,                        // Whether to add DROP TABLE statements to backup file
        'add_insert'    => TRUE,                        // Whether to add INSERT data to backup file
        'newline'       => "\n"                         // Newline character used in backup file
);

    // Backup your entire database and assign it to a variable
    $backup = $this->dbutil->backup($prefs);

    // Load the file helper and write the file to your server
    $this->load->helper('file');
    write_file($app['backup'].'stikid_databackup.gz', $backup);
    echo 'backup completed';

    $this->backup_ftp($app['backup'].'stikid_databackup.gz','stikid_databackup.gz');
    // Load the download helper and send the file to your desktop
    $this->load->helper('download');
    //force_download('database_backup.gz', $backup);
    }
  }

  function cb366bm_cron_backup_ftp($source,$file){
    echo $source;
    $this->load->library('ftp');

    $config['hostname'] = '178.239.179.16';
    $config['username'] = 'admin';
    $config['password'] = 'cb366bm@5146646';
    $config['debug']    = TRUE;

    $this->ftp->connect($config);

    $this->ftp->upload($source, '/softaculous_backups/linux8/'.$file, 'ascii', 0775);

    $this->ftp->close();
  }

}
