<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {


  function __construct(){
 		parent::__construct();
		date_default_timezone_set("Europe/Rome");
		$this->load->library('uuid');
    $this->load->library('form_validation');
    $this->load->helper(array('form', 'url'));
  }

  function index(){
    $this->form_validation->set_rules('email_login', 'Email', 'email|required');
    $this->form_validation->set_rules('password_login', 'Password', 'required|callback_login_user');
    $this->form_validation->set_message('login_user','Email o password non corretti. Riprovare');
    if ($this->form_validation->run() == FALSE)
      {
        redirect(base_url());
      }
    else
      {
        echo 'ERROR'.$this->form_error();
        $this->reload_page();
      }
  }

  public function signin(){
    $this->form_validation->set_rules('username_login', 'Utente', 'required');
    $this->form_validation->set_rules('password_login', 'Password', 'required|callback_login_user');
    $this->form_validation->set_message('login_user','Nome utente o password non corretti. Riprovare');
    if ($this->form_validation->run() == FALSE)
      {
        redirect(base_url());
      }
    else
      {
        echo '<script>alert("Utente non valido. Controllare email e password")</script>';
        $data['mode'] = 'login';
  			$this->load->view('login');
      }
  }

  public function check_email($str){

    $is_email = check_register($str);
    if ( $is_email ){
      return FALSE;
    }
    else {
      return TRUE;
    }
  }

  public function login_user($str){
    $is_user = check_login($str);
    $roles = [ 'admin' , 'fornitore' , 'designer' ];
    if ( $is_user ){
      $utente = $is_user[0];
      $new_data = array (
        'customer_id' => $utente['id_utente'],
        'customer'    => $utente['ac_nome'].' '.$utente['ac_cognome'],
        'islogged'    => true,
        'email'       => $utente['ac_email'],
        'session_id'  => session_id(),
        'ip'          => $_SERVER['REMOTE_ADDR'],
        'user_agent'  => $this->input->user_agent(),
        'role'        => $roles[$utente['bl_tipo']]
      );

      $this->session->set_userdata('user',$new_data);
      return FALSE;
    } else {
      $is_user = check_designer($str);
      if ( $is_user ){
        $utente = $is_user[0];
        $new_data = array (
          'customer_id' => $utente['id_designer'],
          'customer'    => $utente['ac_nome_designer'].' '.$utente['ac_cognome_designer'],
          'islogged'    => true,
          'email'       => $utente['ac_email'],
          'session_id'  => session_id(),
          'ip'          => $_SERVER['REMOTE_ADDR'],
          'user_agent'  => $this->input->user_agent(),
          'role'        => 'designer'
        );
        $this->session->set_userdata('user',$new_data);
        return FALSE;
      } else {
        return TRUE;
      }
    }
  }

  public function reload_page(){
    $data = common_data();
    $data['mode'] = 'cart';
    $data['view'] = 'spedizione';
    $this->load->view('main',$data);
  }

}
