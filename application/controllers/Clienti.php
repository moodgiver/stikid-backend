<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class  Clienti extends CI_Controller {

  function __construct(){
 		parent::__construct();
		date_default_timezone_set("Europe/Rome");
    $this->load->model('Order_model');
    $this->load->model('Customers_model');
    $this->load->model('Vendite_model');
    $this->load->model('App_model');
    $this->load->config('menu');
    $this->load->config('application');
    $this->load->library('uuid');
    $this->load->library('form_validation');
  }

  function index(){
    $mode = $_POST['action'];

    switch ($mode){
      case 'clienti-gestione':
        if ( !isset($_POST['search']) ){
          $_POST['search'] = '';
        }
        $data['mode'] = 'clienti-gestione';
        if ( !isset($_POST['from']) ){
          $period = $this->set_from_to();
          $data['from'] = $period['from'];
          $data['to'] = $period['to'];
        } else {
          $data['from'] = $_POST['from'];
          $data['to'] = $_POST['to'];
        }
        $data['clienti'] = $this->Customers_model->clienti($data['from'],$data['to']);
        $this->load->view('clienti/clienti.php',$data);
      break;

      case 'cliente-cerca':
        if ( !isset($_POST['search']) ){
          $_POST['search'] = '';
        }
        $data['mode'] = 'clienti-gestione';
        $period = $this->set_from_to();
        $data['from'] = $period['from'];
        $data['to'] = $period['to'];
        $data['clienti'] = $this->Customers_model->clienti_search();
        $this->load->view('clienti/clienti_ricerca_result.php',$data);
      break;


      case 'cliente-vedi-ordini':
        $data['clienti'] = $this->Customers_model->cliente_ordini();
        $this->load->view('clienti/cliente_ordini.php',$data);
      break;

      case 'cliente-vedi':
        $data['cliente'] = $this->Customers_model->cliente();
        $this->load->view('clienti/cliente.php',$data);
      break;

      case 'cliente-genera-password':
        $password = genera_password();
        echo $password;
      break;

      case 'cliente-save':
        if ( $this->Customers_model->cliente_save() ){
          echo true;
        } else {
          echo false;
        }
      break;

      case 'clienti-registrazioni':
      if ( !isset($_POST['search']) ){
        $_POST['search'] = '';
      }
      $data['mode'] = 'clienti-registrazioni';
      if ( !isset($_POST['from']) ){
        $period = $this->set_from_to();
        $data['from'] = $period['from'];
        $data['to'] = $period['to'];
      } else {
        $data['from'] = $_POST['from'];
        $data['to'] = $_POST['to'];
      }
      $data['clienti'] = $this->Customers_model->registrazioni($data['from'],$data['to']);
      $this->load->view('clienti/registrati.php',$data);
      break;

    case 'registrato-cerca':
      if ( !isset($_POST['search']) ){
        $_POST['search'] = '';
      }
      $data['mode'] = 'clienti-gestione';
      $period = $this->set_from_to();
      $data['from'] = $period['from'];
      $data['to'] = $period['to'];
      $data['clienti'] = $this->Customers_model->registrati_search();
      $this->load->view('clienti/registrati_ricerca_result.php',$data);
    break;
  }

  }

  function set_from_to(){
    if ( !isset($_POST['from']) ){
      $data['from'] = date("Ymd", strtotime( date( "Ymd", strtotime( date("Ymd") ) ) . "-1 month" ) );
      $data['to'] = date("Ymd", strtotime( date( "Ymd")));
      //$period = $this->set_from_to();
    } else {
      $data['from'] = $_POST['from'];
      $data['to'] = $_POST['to'];
    }
    return $data;
  }

}
