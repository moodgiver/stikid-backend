<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class  Prodotti extends CI_Controller {

  function __construct(){
 		parent::__construct();
		date_default_timezone_set("Europe/Rome");
    $this->load->model('Prodotti_model');
    $this->load->model('Site_model');
    $this->load->model('App_model');
    $this->load->config('application');
    $this->load->library('uuid');
  }

  function index(){
    $form = $_POST;
    $action = $_POST['action'];

    switch ($action){

      case 'negozio-collezioni':
        $data['collezioni'] = $this->Prodotti_model->collezioni();
        $this->load->view('prodotti/collezioni.php',$data);
      break;

      case 'categoria-modifica':
        $data['collezioni'] = $this->Prodotti_model->collezioni();
        $data['collezione'] = $this->Prodotti_model->collezione();
        $this->load->view('prodotti/collezioni.php',$data);
      break;

      case 'negozio-prodotti':
        $data['mode'] ='lista';
        $data['collezioni'] = $this->Prodotti_model->collezioni_sito();
        if ( isset($_POST['id']) ){
          $data['prodotti'] = $this->Prodotti_model->prodotti();
        }
        $this->load->view('prodotti/prodotti.php',$data);
      break;

      case 'negozio-prodotti-upsell':
        $data['mode'] = 'upsell';
        $data['prodotti'] = $this->Prodotti_model->prodotti_upsell_list();
        $this->load->view('prodotti/prodotti_upsell',$data);
      break;

      case 'negozio-prodotto-ricerca':
        //$data['prodotti_search'] = $this->Prodotti_model->prodotto_search();
        $this->load->view('prodotti/prodotto_search');
      break;

      case 'negozio-prodotto-search':
        $data['prodotti_search'] = $this->Prodotti_model->prodotto_search();
        $this->load->view('prodotti/prodotti_cerca_result',$data);
      break;

      case 'negozio-prodotto-dati':
        $data['prodotto'] = $this->Prodotti_model->prodotto();
        $this->load->view('prodotti/prodotto_info',$data);
      break;

      case 'negozio-prodotto-scheda':

        $data['mode'] = 'modifica';
        $data['collezioni'] = $this->Prodotti_model->collezioni_sito();
        $data['prodotti'] = $this->Prodotti_model->prodotti();
        $data['designers'] = $this->Site_model->designers();
        $data['prodotto'] = $this->Prodotti_model->prodotto();

        $this->load->view('prodotti/prodotti.php',$data);

      break;

      case 'negozio-prodotto-modifica':
        $data['mode'] = 'modifica';
        $data['collezioni'] = $this->Prodotti_model->collezioni_sito();
        $data['prodotti'] = $this->Prodotti_model->prodotti();
        $data['designers'] = $this->Site_model->designers();
        $data['prodotto'] = $this->Prodotti_model->prodotto();
        $this->load->view('prodotti/prodotti.php',$data);
      break;

      case 'negozio-prodotto-save-image':
        return $this->Prodotti_model->prodotto_save_image();
      break;

      case 'negozio-prodotto-immagini':
        $data['mode'] = 'modifica';
        $data['prodotto'] = $this->Prodotti_model->prodotto();
        $data['gallery'] = $this->Prodotti_model->prodotto_gallery();
        $data['simulatore'] = $this->Prodotti_model->prodotto_simulatore();
        $this->load->view('prodotti/prodotto_immagini.php',$data);
      break;

      case 'add-image-simulatore':
        $lastid = $this->Prodotti_model->simulatore_add_image();
        echo $lastid;
      break;

      case 'remove-image-simulatore':
        $this->Prodotti_model->simulatore_remove_image();
      break;

      case 'delete-temp-image':
        $app = $this->config->item('application');
        $image = $app['temp_image_path'].''.$_POST['image'];
        unlink ( $image );
      break;

      case 'negozio-prodotto-copy':
        return $this->Prodotti_model->prodotto_copy();
      break;
      
      case 'prodotto-save':
      return $this->Prodotti_model->prodotto_save();
      break;

    }
  }//.index

  function save(){
    $action = $_POST['action'];

    switch ( $action ){

      case 'collezione':
        if ( $_POST['id'] != 0 ){
          $this->Prodotti_model->save_categoria();
        }
      break;
    }
  }

}//.prodotti controller
