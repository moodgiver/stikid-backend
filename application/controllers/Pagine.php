<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class  Pagine extends CI_Controller {

  function __construct(){
		parent::__construct();
    date_default_timezone_set("Europe/Rome");
    $this->load->model('Site_model');
		$this->load->model('App_model');
		$this->load->config('menu');
		$this->load->config('application');
		$this->load->library('uuid');
		$this->load->library('form_validation');
	}

  function index(){
    $mode = $_POST['action'];
    switch ( $mode ){

      case 'contenuti-menu':
      $this->menu();
      break;

      case 'contenuti-menu-add-pagina':
      $this->menu_add_pagina();
      break;

      case 'contenuti-menu-remove-pagina':
      $this->menu_remove_pagina();
      break;

      case 'contenuti-menu-ordine-pagina':
      $this->menu_ordine_pagina();
      break;



      case 'contenuti-pagine':
      $this->pagine();
      break;

      case 'pagina_testo':
      $this->pagina_testo();
      break;

      case 'pagina-save':
      $this->pagina_content_save();
      break;

      case 'pagina-field-save':
      $this->pagina_field_save();
      break;

      case 'pagina-duplica':
      $this->pagina_duplicate();
      break;

      case 'pagina-elimina':
      $this->pagina_elimina();
      break;

    }
  }

  function menu(){
    $data = common_data ();
    $app = $this->config->item('application');
    $data['menus'] = $app['menus'];
    $data['siti'] = $app['siti'];
    $data['pagine'] = $this->Site_model->pagine();
    if ( isset($_POST['position']) ){
      $data['positions'] = $this->Site_model->menu_pagine();
    }
    //$data['pagine'] = $this->Site_model->pagine();
    $this->load->view('pagine/menu.php',$data);
  }

  function menu_add_pagina(){
    $this->Site_model->menu_pagine_add();
    $this->menu();
  }

  function menu_remove_pagina(){
    $this->Site_model->menu_pagine_remove();
    $this->menu();
  }

  function menu_ordine_pagina(){
    $this->Site_model->menu_pagine_ordine();
    $this->menu();
  }

  function pagine(){
    $data = common_data ();
    $app = $this->config->item('application');
    $data['menus'] = $app['menus'];
    $data['siti'] = $app['siti'];
    $data['pagine'] = $this->Site_model->pagine();
    $this->load->view('pagine/pagine.php',$data);
  }

  function pagina_testo(){
    $testo = $this->Site_model->pagina_testo();
    echo $testo;
  }

  function pagina_content_save(){
    if ( $_POST['id'] != 0 ){
      echo $this->Site_model->pagina_content_save();
    } else {
      echo $this->Site_model->pagina_add();
    }
  }

  function pagina_field_save(){
    echo $this->Site_model->pagina_field_save();
  }

  function pagina_duplicate(){
    echo $this->Site_model->pagina_duplicate();
  }

  function pagina_elimina(){
    $this->Site_model->pagina_delete();
    $this->pagine();
  }

}
