<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signup extends CI_Controller {


  function __construct(){
 		parent::__construct();
		date_default_timezone_set("Europe/Rome");
    $this->load->model('Site_model');
		$this->load->model('User_model');
		$this->load->library('uuid');
    $this->load->library('form_validation');
    $this->load->helper(array('form', 'url'));
  }

  function index(){
    $this->form_validation->set_rules('email', 'Email', 'email|valid_email|required|callback_check_email');
    $this->form_validation->set_rules('password', 'Password', 'required');
    $this->form_validation->set_message('check_email','Questo indirizzo email e\' gi&agrave; registrato. Effettuare il login o richiedi una nuova password');

    if ($this->form_validation->run() == TRUE)
      {
        redirect(base_url().'cart/spedizione', 'refresh');
      }
    else
      {
        $this->reload_page();
        //main_page ( 'main','cart','spedizione' );
      }
  }



  function aggiungi_utente(){
    $this->form_validation->set_rules('email', 'Email', 'email|valid_email|required|callback_check_email');
    $this->form_validation->set_rules('password', 'Password', 'required');
    $this->form_validation->set_message('check_email','Questo indirizzo email e\' gi&agrave; registrato. Effettuare il login o richiedi una nuova password');
    $this->form_validation->set_rules('passwordconf','Conferma Password','required|matches[password]');
    if ($this->form_validation->run() == TRUE)
      {
        redirect(base_url().'profilo', 'refresh');
      }
    else
      {
        $this->reload_page();
      }
  }



  public function check_email($str)
       {
         $is_user = check_register($str);

               if ( $is_user )
               {
                  return FALSE;
               }
               else
               {
                  return TRUE;
               }
       }

       public function reload_page(){
         $data = common_data();
         $data['mode'] = 'cart';
         $data['view'] = 'spedizione';
         $this->load->view('main',$data);
       }

}
