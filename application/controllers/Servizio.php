<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Servizio extends CI_Controller {

  function __construct(){
		parent::__construct();
    date_default_timezone_set("Europe/Rome");
    $this->load->model('Site_model');
		$this->load->model('Order_model');
		$this->load->model('User_model');
		$this->load->model('App_model');
    $this->load->model('Vendite_model');
		$this->load->config('menu');
		$this->load->config('application');
    $this->load->config('schema');
		$this->load->library('uuid');
		$this->load->library('form_validation');
	}

  function index(){
    $action = $_POST['action'];
    $app = $this->config->item('application');
    $data['menus'] = $app['menus'];
    $data['siti'] = $app['siti'];
    switch ($action){

      case 'servizio-log-current':
      application_log_read(); //funzione in => stickers_helper
      break;

    }
  }
}
