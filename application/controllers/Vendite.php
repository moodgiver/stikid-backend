<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class  Vendite extends CI_Controller {

  function __construct(){
 		parent::__construct();
		date_default_timezone_set("Europe/Rome");
    $this->load->model('Vendite_model');
    $this->load->model('Site_model');
    $this->load->model('App_model');
    $this->load->config('application');
    $this->load->library('uuid');
  }

  function index(){
    $form = $_POST;
    $action = $_POST['action'];

    switch ($action){

      case 'vendite-periodo':
        $data['mode'] = 'periodo';
        $period = $this->set_from_to();
        $data['from'] = $period['from'];
        $data['to'] = $period['to'];
        $data['controller'] = 'vendite-periodo';
        $data['vendite'] = $this->Vendite_model->vendite($data['from'],$data['to']);
        $this->load->view('vendite/vendite.php',$data);
      break;

      case 'vendite-coupon':
        $data['mode'] = 'coupon';
        $period = $this->set_from_to();
        $data['from'] = $period['from'];
        $data['to'] = $period['to'];
        $data['controller'] = 'vendite-coupon';
        $data['vendite'] = $this->Vendite_model->vendite_coupon($data['from'],$data['to']);
        $this->load->view('vendite/vendite.php',$data);
      break;

      case 'vendite-prodotti':
        $data['mode'] = 'prodotti';
        $period = $this->set_from_to();
        $data['from'] = $period['from'];
        $data['to'] = $period['to'];
        $data['controller'] = 'vendite-prodotti';
        $data['vendite'] = $this->Vendite_model->vendite_prodotti($data['from'],$data['to']);
        $this->load->view('vendite/vendite_prodotti.php',$data);
      break;

      case 'vendite-clienti':
        $data['mode'] = 'clienti';
        $period = $this->set_from_to();
        $data['from'] = $period['from'];
        $data['to'] = $period['to'];
        $data['controller'] = 'vendite-clienti';
        $data['vendite'] = $this->Vendite_model->vendite_clienti($data['from'],$data['to']);
        $this->load->view('vendite/vendite_clienti.php',$data);
      break;

      case 'vendite-costi-fornitore':
        $data['mode'] = 'costi-fornitore';
        $period = $this->set_from_to();
        $data['from'] = $period['from'];
        $data['to'] = $period['to'];
        $data['controller'] = 'vendite-costi';
        $data['fornitori'] = $this->Vendite_model->fornitori();
        if ( isset($_POST['id']) ){
          $form = $_POST;
          $data['vendite'] = $this->Vendite_model->costi_fornitore($form);
          $data['costi'] = $this->Vendite_model->costi($form);
        }
        $this->load->view('vendite/vendite_costi_fornitore.php',$data);
      break;

      case 'costi-simulatore':
        $data['mode'] = 'costi-simulatore';
        $data['prezzi'] = $this->Vendite_model->prezzi();
        $this->load->view('vendite/vendite_prezzi_simulatore.php',$data);
      break;
    }
  }

  function set_from_to(){
    if ( !isset($_POST['from']) ){
      $data['from'] = date("Ymd", strtotime( date( "Ymd", strtotime( date("Ymd") ) ) . "-1 month" ) );
      $data['to'] = date("Ymd", strtotime( date( "Ymd")));
      //$period = $this->set_from_to();
    } else {
      $data['from'] = $_POST['from'];
      $data['to'] = $_POST['to'];
    }
    return $data;
  }

  function calcola_costo(){
		$totale = 0;
		$wMin = 0;
		$nImballo = 0;
    if ( !isset($stickytext) ){
      $calcoloAreaMQ = (float)$height/100*(float)$width/100;
    } else {
      $stw = explode($width,'|');
      $total_width = 0;
      foreach ( $stw AS $ws ){
        $total_width += $ws;
      }
      $calcoloAreaMQ = (float)$height/100*(float)$total_width/100;
    }
    /*
		<cfif calcoloAreaMQ LTE arguments.form.misura_minima>
			<cfset costoSticker = arguments.form.costo_minimo*calcoloAreaMQ>
		<cfelse>
			<cfset costoSticker = arguments.form.costo*calcoloAreaMQ>
		</cfif>
		<cfif arguments.form.width LT arguments.form.height>
			<cfif arguments.form.width GT wMin>
				<cfset wMin = arguments.form.width>
			</cfif>
		<cfelse>
			<cfif arguments.form.height GT wMin>
				<cfset wMin = arguments.form.height>
			</cfif>
		</cfif>

		<cfif calcoloAreaMQ GT 1.11>
			<cfif bl_colore EQ 1>
				<cfset costoSticker = calcoloAreaMQ * arguments.form.ac_costo>
			<cfelse>
				<cfset costoSticker = calcoloAreaMQ * arguments.form.ac_costo>
			</cfif>
			<cfif costoSticker LT 20>
				<cfset costoSticker = 20>
			</cfif>
		<cfelse>
			<cfif bl_colore NEQ 1>
				<cfset costoSticker = calcoloAreaMQ * arguments.form.ac_costo_quadri>
			<cfelse>
				<cfset costoSticker = calcoloAreaMQ * arguments.form.ac_costo>
			</cfif>
		</cfif>

		<cfif costoSticker LT 6 AND arguments.form.id_fornitore EQ 3>
			<cfset costoSticker = 6>
		</cfif>*/
  }

}
