<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class  Moduli extends CI_Controller {

  function __construct(){
		parent::__construct();
    date_default_timezone_set("Europe/Rome");
    $this->load->model('Site_model');
		$this->load->model('Order_model');
		$this->load->model('User_model');
		$this->load->model('App_model');
    $this->load->model('Vendite_model');
		$this->load->config('menu');
		$this->load->config('application');
    $this->load->config('schema');
		$this->load->library('uuid');
		$this->load->library('form_validation');
	}

  function index(){
    $action = $_POST['action'];
    $app = $this->config->item('application');
    $data['menus'] = $app['menus'];
    $data['siti'] = $app['siti'];

    switch ($action){

      case 'moduli-slider-stikid':
        $data['mode'] = $action;
        $data['action'] = 'moduli-save-slider';
        $data['slides'] = $this->App_model->slides(4);
        $data['schema'] = $this->config->item('schema');
        $data['name'] = 'slider';
        $data['path'] = $this->config->item('application');
        $this->load->view('moduli/slider.php',$data);
      break;

      case 'moduli-slider-sticasa':
        $data['mode'] = $action;
        $data['action'] = 'moduli-save-slider';
        $data['slides'] = $this->App_model->slides(1);
        $data['schema'] = $this->config->item('schema');
        $data['name'] = 'slider';
        $data['path'] = $this->config->item('application');
        $this->load->view('moduli/slider.php',$data);
      break;

      case 'moduli-slider-qpb':
        $data['mode'] = $action;
        $data['action'] = 'moduli-save-slider';
        $data['slides'] = $this->App_model->slides(8);
        $data['schema'] = $this->config->item('schema');
        $data['name'] = 'slider';
        $data['path'] = $this->config->item('application');
        $this->load->view('moduli/slider.php',$data);
      break;

      case 'moduli-save-slider':
        echo $this->App_model->save_settings_data();
      break;

      case 'moduli-banners':
        $data['mode'] = $action;
        $data['action'] = 'moduli-save-banner';
        $data['banners'] = $this->App_model->banners();
        $data['schema'] = $this->config->item('schema');
        $data['name'] = 'banner';
        $data['path'] = $this->config->item('application');
        $this->load->view('moduli/banner.php',$data);
      break;

      case 'moduli-save-banner':
        echo $this->App_model->save_settings_data();
      break;

    }
  }



}
