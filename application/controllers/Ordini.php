<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class  Ordini extends CI_Controller {

  function __construct(){
 		parent::__construct();
		date_default_timezone_set("Europe/Rome");
    $this->load->model('Order_model');
    $this->load->model('App_model');
    $this->load->config('application');
    $this->load->library('uuid');
  }

  function index(){
    $form = $_POST;
    $action = $_POST['action'];
    $data['fornitori'] = $this->App_model->fornitori();

    switch ($action){

      case 'ordini-ricevuti':
        $status = 1;
        $this->render_status($status);
      break;

      case 'ordini-bonifico':
        $status = -1;
        $this->render_status($status);
      break;

      case 'ordini-fornitore':
        $status = 2;
        $this->render_status($status);
      break;

      case 'ordini-lavorazione':
        $status = 3;
        $this->render_status($status);
      break;

      case 'ordini-spedizione':
        $status = 4;
        $this->render_status($status);
      break;

      case 'ordini-spediti':
        $status = 5;
        $this->render_status($status);
      break;

      case 'ordini-evasi':
        $status = 6;
        $this->render_status($status);
      break;

      case 'ordine-dettaglio':
        $nr = $_POST['ordine'];
        $data['ordine'] = $this->Order_model->ordine($nr);
        $this->load->view('ordini/ordine_dettaglio.php',$data);
      break;

      case 'ordini-cerca':
        $this->load->view('ordini/ordini_filter.php');
      break;

      case 'ordine-cerca':
        $data['ricerca'] = true;
        $data['ordini'] = $this->Order_model->ordini_filter();
        $this->load->view('ordini/ordini_ricerca.php',$data);
      break;

      case 'ordini-recupero':
        $this->render_status_recupero();
      break;

      case 'ordine-recupero-dettaglio':
        $nr = $_POST['id'];
        $data['ordine'] = $this->Order_model->ordine_recupero_dettaglio($nr);
        $this->load->view('ordini/ordine_recupero_dettaglio.php',$data);
      break;

      case 'recupera-ordine':
        $this->Order_model->recupera_ordine();
      break;

      case 'stato-ordine':
        $data['ricerca'] = false;
        $data['ordini'] = $this->Order_model->ordine_status();
        $this->load->view('ordini/ordini_ricerca.php',$data);
      break;

      case 'ordine-aggiorna-fornitore':
        $this->Order_model->aggiorna_ordine_fornitore();
        $ordine = $this->Order_model->ordine($_POST['id']);
        email_ordine_fornitore($ordine);
      break;

      case 'ordini-emails':
        $data['emails'] = $this->App_model->email_logs();
        $this->load->view('service/logs_email.php',$data);
      break;

      case 'ordine-elimina':
        return $this->Order_model->elimina_ordine();
      break;

      case 'ordine-temporaneo-elimina':
        return $this->Order_model->elimina_ordine_temporaneo();
      break;

      case 'ordine-sub-fornitore':
        return $this->Order_model->subfornitore_ordine();
      break;

      case 'ordini-etichette':
      $data['etichette'] = $this->Order_model->etichette();
      //load the view and saved it into $html variable
      //$this->load->view('ordini/etichette', $data, false);

      $html = $this->load->view('ordini/etichette', $data, true);
      //this the the PDF filename that user will get to download
      $pdfFilePath = "etichette.pdf";

      //load mPDF library
      $this->load->library('m_pdf');

      //generate the PDF from the given html
      $this->m_pdf->pdf->WriteHTML($html);

      //download it.
      $this->m_pdf->pdf->Output($pdfFilePath, "D");
      break;

      case 'ordini-bordero':
      $data['bordero'] = $this->Order_model->etichette();
      //load the view and saved it into $html variable
      $html = $this->load->view('ordini/bordero', $data, true);
      //this the the PDF filename that user will get to download
      $pdfFilePath = "bordero.pdf";

      //load mPDF library
      $this->load->library('m_pdf');

      //generate the PDF from the given html
      $this->m_pdf->pdf->WriteHTML($html);

      //download it.
      $this->m_pdf->pdf->Output($pdfFilePath, "D");
      break;

      case 'ordini-salva-allegato':
        $this->Order_model->ordine_salva_allegato();
      break;

      case 'ordini-elimina-allegato':
        $this->Order_model->ordine_elimina_allegato();
      break;

      case 'ordini-allegati':
        $data['allegati'] = $this->Order_model->ordini_allegati();
        $this->load->view('ordini/ordini_allegati.php',$data);
      break;

    }

  }

  function etichette(){
    $data['etichette'] = $this->Order_model->etichette();
    //load the view and saved it into $html variable
    //$this->load->view('ordini/etichette', $data, false);
    $html = $this->load->view('ordini/etichette', $data, true);
    //this the the PDF filename that user will get to download
    $pdfFilePath = "etichette.pdf";

    //load mPDF library
    $this->load->library('m_pdf');

    //generate the PDF from the given html
    $this->m_pdf->pdf->WriteHTML($html);

    //download it.
    $this->m_pdf->pdf->Output($pdfFilePath, "D");
  }

  function bordero(){
    $data['bordero'] = $this->Order_model->etichette();
    //load the view and saved it into $html variable
    $html = $this->load->view('ordini/bordero', $data, true);
    //this the the PDF filename that user will get to download
    $pdfFilePath = "bordero.pdf";

    //load mPDF library
    $this->load->library('m_pdf');

    //generate the PDF from the given html
    $this->m_pdf->pdf->WriteHTML($html);

    //download it.
    $this->m_pdf->pdf->Output($pdfFilePath, "D");
  }

  function render_status($status){
    $data['fornitori'] = $this->App_model->fornitori();
    $data['ordini'] = $this->Order_model->ordini($status);
    $this->load->view('ordini/ordini_status.php',$data);
  }

  function render_status_recupero(){
    $data['fornitori'] = $this->App_model->fornitori();
    $data['ordini'] = $this->Order_model->ordini_recupero();
    $this->load->view('ordini/ordini_status_recupero.php',$data);
  }
}
