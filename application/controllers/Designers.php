<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class  Designers extends CI_Controller {

  function __construct(){
		parent::__construct();
    date_default_timezone_set("Europe/Rome");
    $this->load->model('Site_model');
		$this->load->model('Order_model');
		$this->load->model('User_model');
		$this->load->model('App_model');
    $this->load->model('Vendite_model');
    $this->load->model('Designers_model');
		$this->load->config('menu');
		$this->load->config('application');
    $this->load->config('schema');
		$this->load->library('uuid');
		$this->load->library('form_validation');
	}

  function index(){
    $action = $_POST['action'];
    switch ( $action ){

      case 'designers-profilo':
        $data['schema'] = $this->config->item('schema');
        $data['name'] = 'designers';
        $data['action'] = 'designers-profilo-salva';
        if ( $_SESSION['user']['role'] == 'admin' ){
          $data['designers'] = $this->Designers_model->designers();
        } else {
          $_POST['id'] = $_SESSION['user']['customer_id'];
        }

        if ( isset($_POST['id']) ) {
          $data['designer'] = $this->Designers_model->designer();
        }
        $this->load->view('designers/designer.php',$data);
      break;

      case 'designers-vendite':
        $this->designers_vendite();
      break;

      case 'designers-vendite-result':
        $this->designers_vendite();
      break;

    }
  }

  function designers_vendite(){
    $period = set_period_1_month();
    $data['from'] = $period['from'];
    $data['to'] = $period['to'];
    if ( isset($_POST['from']) ){
      $data['from'] = $_POST['from'];
      $data['to'] = $_POST['to'];
    }
    $data['vendite'] = [];
    $data['designers'] = $this->Designers_model->designers();
    if ( isset($_POST['id']) ) {
      $data['vendite'] = $this->Designers_model->designers_vendite();
      $this->load->view('designers/designers_vendite_result',$data);
    } else {
      $this->load->view('designers/designers_vendite',$data);
    }

  }

}
