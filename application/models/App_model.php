<?php

class App_Model extends CI_Model {

	function __construct(){
		parent::__construct();
		$this->load->config('schema');
		$this->load->config('application');
	}

	function runQuery($sql,$filter){
    $query = $this->db->query($sql,$filter);
    return($query->result_array());
	}

  function fornitori(){
    $this->db->select('*');
    $this->db->from('tbl_utenti');
    $query = $this->db->get();
		return($query->result_array());
  }

  function email_hook($hook){
    $this->db->select('*');
    $this->db->from('tbl_emails');
    $this->db->where('hook',$hook);
    $query = $this->db->get();
    return($query->result_array());
  }

  function register_mail($data){
    $this->db->insert('tbl_emails_logs',$data);
  }

  function email_logs(){
    $this->db->select('*');
    $this->db->from('tbl_emails_logs');
    $this->db->order_by('data_email','DESC');
    $query = $this->db->get();
    return($query->result_array());
  }

	function ordini_tracking(){
		$today = date("Y-m-d", strtotime( date( "Ymd")));
		$sql = "SELECT
			tbl_registrazioni.ac_email,
			tbl_ordini.ac_nrordine,
			tbl_ordini.dt_ordine,
			tbl_ordini.bl_status,
			tbl_ordini_summary.id_ordine_summary
		FROM tbl_registrazioni
		INNER JOIN tbl_ordini ON tbl_registrazioni.idregistrazione = tbl_ordini.id_utente
		INNER JOIN tbl_ordini_summary ON tbl_ordini.ac_nrordine = tbl_ordini_summary.ac_nrordine
		WHERE
			tbl_ordini.bl_status = ?
			AND
			(
			dt_spedizione = ?
			)
		GROUP BY ac_nrordine";
		$filter = array ( 4 , $today );
		$query = $this->runQuery($sql,$filter);
		return $query;
	}

	function set_ordine_spedito($nrordine){
		$params = array (
			'bl_status' => 5
		);
		$this->db->where('ac_nrordine',$nrordine);
		$this->db->update('tbl_ordini',$params);
		return true;
	}

	function settings_generali(){
		$this->db->select('*');
		$this->db->order_by('int_sito');
		$query = $this->db->get('tbl_settings');
		return($query->result_array());
	}

	function settings_meta(){
		$this->db->select('*');
		$this->db->where('type',0);
		$this->db->order_by('int_sito');
		$query = $this->db->get('tbl_settings');
		return($query->result_array());
	}

	function save_setting(){
		$params = array (
			'ac_setting' 		=> $_POST['setting'],
			'ac_content'		=> $_POST['content'],
			'int_position'	=> $_POST['posizione'],
			'type'					=> $_POST['tipo'],
			'bl_attivo'			=> $_POST['attivo']
		);
		$this->db->where('id_setting',$_POST['id']);
		return $this->db->update('tbl_settings',$params);
	}

	function setting_field_save(){
		$params = array (
			$_POST['field'] => $_POST['value']
		);
		$this->db->where('id_setting',$_POST['id']);
		return $this->db->update('tbl_settings',$params);
  }

	function save_prezzo(){
		$params = array (
			'ac_area' 				=> $_POST['area'],
			'ac_prezzo' 			=> $_POST['prezzo'],
			'ac_prezzo_reale'	=> $_POST['prezzo_reale']
		);
		$this->db->where('id_prezzo',$_POST['id']);
		return $this->db->update('tbl_pricing',$params);
	}

	function slides($sito){
		$this->db->select('*');
		$this->db->where('int_sito',$sito);
		$this->db->order_by( 'bl_image_type,int_order' );
		$query = $this->db->get('tbl_slider');
		return ( $query->result_array() );
	}

	function banners(){
		$this->db->select('*');
		$this->db->order_by('int_sito,banner');
		$query = $this->db->get('tbl_banners');
		return ( $query->result_array() );
	}
	
	function save_settings_data(){
		//$this->load->config('schema');
		$schema = $this->config->item('schema');
		$table = $schema['database'][$_POST['table']];
		$table_name = $table['table'];
		$fields = $table['fields'];
		$field_key = $table['key'];
	  $params = [];
		if ( isset($_POST[$field_key]) ){
			foreach ( $fields AS $key=>$value ){
				$f = array ( $key => $_POST[$key] );
				$this->db->where($field_key,$_POST[$field_key]);
				$this->db->update($table_name,$f);
			}
		} else {
			foreach ( $fields AS $key=>$value ){
				if ( $key != $field_key ){
					$params[$key] = $_POST[$key];
				}
			}
			$this->db->insert($table_name,$params);
		}
		return true;
	}
}
