<?php

class Order_Model extends CI_Model {

	function __construct(){
		parent::__construct();
	}

	function runQuery($sql,$filter){
    $query = $this->db->query($sql,$filter);
    return($query->result_array());
	}

	public function ordini($status){
		$sql = "SELECT
			tbl_ordini_summary.*,
			tbl_ordini_summary.ac_coupon AS coupon_usato,
			DATE_FORMAT(tbl_ordini_summary.dt_pagamento,'%d-%m-%Y %H:%I') AS data_ordine,
			tbl_registrazioni.idregistrazione,
			tbl_registrazioni.ac_email,
			tbl_registrazioni.ac_nome,
			tbl_registrazioni.ac_cognome,
			tbl_registrazioni.ac_indirizzo,
			tbl_registrazioni.ac_citta,
			tbl_registrazioni.ac_cap,
			tbl_registrazioni.ac_pv,
			tbl_registrazioni.ac_azienda,
			tbl_registrazioni.ac_telefono,
			tbl_registrazioni.ac_cellulare,
			tbl_ordini_allegati.ac_allegato
		FROM tbl_ordini_summary
		INNER JOIN tbl_registrazioni ON tbl_ordini_summary.id_utente = tbl_registrazioni.idregistrazione
		LEFT JOIN tbl_ordini_allegati ON tbl_ordini_summary.id_ordine_summary = tbl_ordini_allegati.id_ordine_summary
		WHERE
			tbl_ordini_summary.bl_stato = ? OR tbl_ordini_summary.bl_stato = ?
			AND
			tbl_ordini_summary.dt_pagamento IS NOT NULL
			AND
			YEAR(tbl_ordini_summary.dt_pagamento) > 2013
			AND
			tbl_ordini_summary.ac_nrordine <> ''
		GROUP BY tbl_ordini_summary.ac_nrordine
		ORDER BY dt_pagamento DESC
		LIMIT 0,10";
		if ( $status == 1 ){
			$filter = array ( $status , -1 );
		} else {
			$filter = array ( $status , $status );
		}
		$query = $this->runQuery($sql,$filter);
		return $query;
	}

	public function ordini_filter(){
		$this->db->select('tbl_ordini_summary.*,tbl_ordini_summary.id_ordine_summary,tbl_ordini_summary.ac_nrordine,tbl_ordini_summary.ac_coupon AS coupon_usato,DATE_FORMAT(tbl_ordini_summary.dt_pagamento,"%d-%m-%Y %H:%I") AS data_ordine,tbl_ordini_allegati.ac_allegato,tbl_registrazioni.*');
		$this->db->from('tbl_ordini_summary');
		$this->db->join('tbl_registrazioni','tbl_registrazioni.idregistrazione = tbl_ordini_summary.id_utente');
		$this->db->join('tbl_ordini_allegati','tbl_ordini_summary.id_ordine_summary = tbl_ordini_allegati.id_ordine_summary','left');

		if ( isset($_POST['ordine']) && $_POST['ordine'] != '' ){
			$filter = array ( $_POST['ordine'] );
			$this->db->like('tbl_ordini_summary.ac_nrordine',$_POST['ordine']);
		}
		if ( isset($_POST['cliente']) && $_POST['cliente'] != '' ){
			$this->db->like('ac_cognome',$_POST['cliente']);
		}
		if ( isset($_POST['email']) && $_POST['email'] != '' ){
			$this->db->like('ac_email',$_POST['email']);
		}
		$this->db->where('YEAR(dt_pagamento) > 2013');
		$query = $this->db->get();
		return($query->result_array());

	}

  public function ordini_recupero(){
		$sql = "SELECT
			tbl_ordini_summary_temp.*,
			tbl_ordini_summary_temp.ac_coupon AS coupon_usato,
			DATE_FORMAT(tbl_ordini_summary_temp.dt_pagamento,'%d-%m-%Y %H:%I') AS data_ordine,
			tbl_registrazioni.*,
			tbl_ordini_allegati.ac_allegato,
			tbl_ordini_summary.id_ordine_summary AS id_presente
		FROM tbl_ordini_summary_temp
		INNER JOIN tbl_registrazioni ON tbl_ordini_summary_temp.id_utente = tbl_registrazioni.idregistrazione
		LEFT JOIN tbl_ordini_allegati ON tbl_ordini_summary_temp.id_ordine_summary = tbl_ordini_allegati.id_ordine_summary
		LEFT JOIN tbl_ordini_summary ON tbl_ordini_summary_temp.ac_nrordine = tbl_ordini_summary.ac_nrordine
		WHERE
			YEAR(tbl_ordini_summary_temp.dt_pagamento) = ?
			AND
			tbl_ordini_summary.id_ordine_summary IS NULL
		GROUP BY tbl_ordini_summary_temp.ac_nrordine
		ORDER BY tbl_ordini_summary_temp.id_ordine_summary DESC
		LIMIT 0,10";
		$filter = array ( date('Y') );
		$query = $this->runQuery($sql,$filter);
		return $query;
	}

	function ordine($nr){
		$sql = "SELECT
			tbl_ordini_summary.*,
			tbl_ordini.*,
			tbl_ordini.ac_colore AS colore,
			tbl_prodotti.*,
			tbl_utenti.ac_email AS 'fornitore_email',
			tbl_utenti.ac_cognome AS 'fornitore',
			tbl_registrazioni.*,
			tbl_registrazioni.ac_email AS 'cliente_email'
		FROM tbl_ordini
		INNER JOIN tbl_ordini_summary ON tbl_ordini.ac_nrordine = tbl_ordini_summary.ac_nrordine
		INNER JOIN tbl_prodotti ON tbl_ordini.id_prodotto = tbl_prodotti.id_prodotto
		LEFT JOIN tbl_utenti ON tbl_ordini.id_fornitore = tbl_utenti.id_utente
		INNER JOIN tbl_registrazioni ON tbl_ordini_summary.id_utente = tbl_registrazioni.idregistrazione
		WHERE
			tbl_ordini_summary.id_ordine_summary = ?";
		$filter = array ( $nr );
		$query = $this->runQuery($sql,$filter);
		return $query;
	}

  function ordine_recupero_dettaglio($nr){
		$sql = "SELECT
			tbl_ordini_summary_temp.*,
			tbl_ordini_temp.*,
			tbl_prodotti.*,
			tbl_utenti.ac_email AS 'fornitore_email',
			tbl_utenti.ac_cognome AS 'fornitore',
			tbl_registrazioni.*
		FROM tbl_ordini_temp
		INNER JOIN tbl_ordini_summary_temp ON tbl_ordini_temp.ac_nrordine = tbl_ordini_summary_temp.ac_nrordine
		INNER JOIN tbl_prodotti ON tbl_ordini_temp.id_prodotto = tbl_prodotti.id_prodotto
		LEFT JOIN tbl_utenti ON tbl_ordini_temp.id_fornitore = tbl_utenti.id_utente
		INNER JOIN tbl_registrazioni ON tbl_ordini_summary_temp.id_utente = tbl_registrazioni.idregistrazione
		WHERE
			tbl_ordini_summary_temp.id_ordine_summary = ?";
		$filter = array ( $nr );
		$query = $this->runQuery($sql,$filter);
		return $query;
	}

  function recupera_ordine(){
		$result = $this->save_order_recuperato($_POST['ordine'],$_POST['pagamento']);
		return $result;
	}

  public function numero_ordine(){
    $this->db->query('LOCK TABLES tbl_nrordini  WRITE');
    $sql = "SELECT * FROM tbl_nrordini";
    $query = $this->db->query($sql);
    $orderdata = $query->result_array();
    $id = $orderdata[0]['id_nrordine'];
    $nrordine = (int)$orderdata[0]['ac_nrordine'];
    $nrordine++;
    $params = array (
      'ac_nrordine' => (string)$nrordine
    );
    $this->db->set(	$params );
		$this->db->where ( 'id_nrordine' , $id );
		$this->db->update ( 'tbl_nrordini' );
    $this->db->query('UNLOCK TABLES');
    return $nrordine;
  }

	public function ordine_status(){
		if ( $_POST['status'] == -1 ){
			$status = 1;
		} else {
			$status = (int)$_POST['status'] + 1;
		}
		$params = array (
			'bl_stato' => $status
		);
		$this->db->set($params);
		$this->db->where('id_ordine_summary',$_POST['id']);
		$this->db->update('tbl_ordini_summary');

		if ( $status !=4 ){
			$params = array(
				'bl_status' => $status
			);
		} else {
			$params = array (
				'bl_status' => $status,
				'dt_spedizione' => adesso()
			);
		}

		$this->db->set($params);
		$this->db->where('ac_nrordine',$_POST['ordine']);
		$this->db->update('tbl_ordini');
		$query = $this->ordini_filter();
		return $query;

	}

	function aggiorna_ordine_status($ordine,$status){
		$data = array ( 'bl_stato' => $status );
		$this->db->set($data);
		$this->db->where('ac_nrordine',$ordine);
		$this->db->update('tbl_ordini_summary');
		$data = array ( 'bl_status' => $status );
		$this->db->set($data);
		$this->db->where('ac_nrordine',$ordine);
		$this->db->update('tbl_ordini');
		return true;
	}

	function aggiorna_ordine_fornitore(){
		$params = array (
			'id_fornitore' => $_POST['fornitore']
		);
		$this->db->where('ac_nrordine',$_POST['ordine']);
		$this->db->update('tbl_ordini',$params);
		return true;
	}

	function elimina_ordine(){
		$this->db->where('ac_nrordine',$_POST['ordine']);
		$this->db->delete('tbl_ordini_summary');
		$this->db->where('ac_nrordine',$_POST['ordine']);
		$this->db->delete('tbl_ordini');
		return true;
	}

  function elimina_ordine_temporaneo(){
		$this->db->where('ac_nrordine',$_POST['ordine']);
		$this->db->delete('tbl_ordini_summary_temp');
		$this->db->where('ac_nrordine',$_POST['ordine']);
		$this->db->delete('tbl_ordini_temp');
		return true;
	}

	function subfornitore_ordine(){
		$params = array (
			'bl_subfornitore' => true
		);
		$this->db->where('id_ordine_summary',$_POST['id']);
		$this->db->update('tbl_ordini_summary',$params);
		return true;
	}

  public function save_preorder(){
    $ordine_delete = (string)$this->session->order['id'].'/'.mdate('%Y',time());
    $ordine_data = mdate('%Y-%m-%d %h:%i %a',time());
    $this->db->where('ac_nrordine',$ordine_delete);
    $this->db->delete('tbl_ordini_temp');
    foreach ( $this->session->cart_items AS $item ){

      $cart_line = array (
      'id_lingua'     => 2,
      'ac_nrordine'   => (string)$this->session->order['id'].'/'.mdate('%Y',time()),
      'ac_uuid'       => (string)$this->session->user['customer_id'],
      'ac_cfid'       => (string)$this->session->user['ip'],
      'dt_ordine'     => adesso(),
      'id_utente'     => (string)$this->session->user['customer_id'],
      'id_prodotto'   => $item['id'],
      'ac_width'      => $item['w'],
      'ac_height'     => $item['h'],
      'ac_ratio'      => '',
      'ac_area'       => (float)$item['w']*(float)$item['h'],
      'ac_prezzo'     => $item['prezzo'],
      'ac_cambio'     => '1',
      'ac_colore'     => $item['colore'],
      'bl_flip'       => false,
      'ac_testo'      => $item['testo'],
      'ac_telaio'     => $item['telaio'],
      'ac_fontsize'   => $item['font'],
      'ac_qty'        => $item['qty'],
      'ac_totale'     => (string)(float)$item['prezzo']*(float)$item['qty'],
      'bl_status'     => 1,
      'ac_crop'       => 'nocrop',
      'ac_biglietto'  => ''
      );
      $this->db->insert('tbl_ordini_temp', $cart_line );
    }
		/*
    if ( !isset($this->session->order['COUPON']) ){
      $coupon = '';
      $coupon_value = 0;
    } else {
      $coupon = $this->session->order['COUPON'];
      $coupon_value = $this->session->order['COUPON_VALUE'];
    }
		*/
    $shipto = $_POST['o_shipto'];

    $this->db->where('ac_nrordine',$ordine_delete);
    $this->db->delete('tbl_ordini_summary_temp');
		/*if ( $coupon_value == '' ){
			$coupon_value = '0';
		}*/

    $myordine = array (
      'id_utente'					    => $this->session->user['customer_id'],
      'ac_nrordine'					  => (string)$this->session->order['id'].'/'.mdate('%Y',time()),
      'ac_totale'					    => $this->session->order['ORDER_TOTAL'],
      'ac_spesespedizione'		=> $this->session->order['SHIPPING'],
      'ac_sconto'					    => $this->session->order['COUPON_VALUE'],//(string)$coupon_value,
      'ac_sconto_riservato'		=> '0',
      'ac_coupon'					    => $this->session->order['COUPON'],
      'ac_pagamento'				  => '',
      'dt_pagamento'			    => adesso(),
      'bl_fattura'					  => false,
      'ac_azienda_fattura'		=> $this->session->order['O_AZIENDA']	,
      'ac_indirizzo_fattura'	=> $this->session->order['O_INDIRIZZO'],
      'ac_citta_fattura'			=> $this->session->order['O_CITTA'],
      'ac_prov_fattura'				=> $this->session->order['O_PV'],
      'ac_cap_fattura'				=> $this->session->order['O_CAP'],
      'ac_country_fattura'		=> $this->session->order['O_NAZIONE']	,
      'ac_cf_fattura'				  => '',
      'ac_piva_fattura'				=> '',
      'bl_delivery_delivery'	=> $shipto,
      'ac_firstname_delivery'	=> $this->session->order['OS_NOME']	,
      'ac_lastname_delivery'	=> $this->session->order['OS_COGNOME']	,
      'ac_company_delivery'		=> $this->session->order['OS_AZIENDA']	,
      'ac_address_delivery'		=> $this->session->order['OS_INDIRIZZO'],
      'ac_city_delivery'			=> $this->session->order['OS_CITTA'],
      'ac_state_delivery'			=> $this->session->order['OS_PV'],
      'ac_country_delivery'		=> $this->session->order['OS_NAZIONE']	,
      'ac_zip_delivery'				=> $this->session->order['OS_CAP']	,
      'ac_phone_delivery'			=> $this->session->order['OS_TELEFONO']	,
      'ac_mobile_delivery'		=> $this->session->order['OS_MOBILE']	,
      'bl_stato'					    => 1,
      'bl_regalo'					    => 0,
      'ac_biglietto'          => ''
    );
    $this->db->insert('tbl_ordini_summary_temp', $myordine );
    return true;
  }

  function save_order_recuperato($nrordine,$pagato_con){
    $sql = "INSERT INTO tbl_ordini
            SELECT d.*
            FROM tbl_ordini_temp d
            WHERE ac_nrordine = ?";

    $filter = array ($nrordine);
    $this->db->query($sql,$filter);
    $pagamento = array (
        'ac_pagamento' => $pagato_con
    );
    $this->db->where('ac_nrordine',$nrordine);
    $this->db->update('tbl_ordini',$pagamento);

    $sql = "INSERT INTO tbl_ordini_summary
            SELECT d.*
            FROM tbl_ordini_summary_temp d
            WHERE ac_nrordine = ?";

    $filter = array ($nrordine);
    $this->db->query($sql,$filter);
		$status = 1;
		if ( $pagato_con == 'BONIFICO' ) {
			$status = -1;
		}
    $pagamento = array (
        'ac_pagamento' 	=> $pagato_con,
				'bl_stato'			=> $status
    );
    $this->db->where('ac_nrordine',$nrordine);
    $this->db->update('tbl_ordini_summary',$pagamento);

		$this->db->where('ac_nrordine',$nrordine);
		$stato = array ( 'bl_stato' => 99 );
		$this->db->update('tbl_ordini_summary_temp',$stato);

		$this->db->where('ac_nrordine',$nrordine);
		$stato = array ( 'bl_status' => 99 );
		$this->db->update('tbl_ordini_temp',$stato);
    return true;

  }


	function etichette(){
		if ( $this->session->user['role'] != 'admin'){
		$sql = "SELECT
	tbl_ordini.* ,
	tbl_prodotti.*,
	tbl_registrazioni.*,
	tbl_coupons_used.ac_coupon AS coupon_usato,
	tbl_coupons.*,
	tbl_ordini_summary.ac_firstname_delivery AS nome_d,
	tbl_ordini_summary.ac_lastname_delivery AS cognome_d,
	tbl_ordini_summary.ac_company_delivery AS company_d,
	tbl_ordini_summary.ac_address_delivery AS address_d,
	tbl_ordini_summary.ac_zip_delivery AS zip_d,
	tbl_ordini_summary.ac_city_delivery AS city_d,
	tbl_ordini_summary.ac_state_delivery AS state_d,
	tbl_ordini_summary.ac_country_delivery AS country_d,
	tbl_ordini_summary.ac_phone_delivery AS phone_d,
	tbl_ordini_summary.ac_mobile_delivery AS mobile_d,
	tbl_ordini_summary.bl_delivery_delivery AS extra_delivery
FROM
	tbl_ordini
INNER JOIN tbl_prodotti ON tbl_ordini.id_prodotto = tbl_prodotti.id_prodotto
INNER JOIN tbl_registrazioni ON tbl_ordini.id_utente = tbl_registrazioni.idregistrazione
INNER JOIN tbl_ordini_summary ON tbl_ordini.ac_nrordine = tbl_ordini_summary.ac_nrordine
LEFT JOIN tbl_coupons_used ON tbl_ordini.ac_nrordine = tbl_coupons_used.ac_nrordine
LEFT JOIN tbl_coupons ON tbl_coupons_used.ac_coupon = tbl_coupons.ac_codice
WHERE
	tbl_ordini.bl_status = 4
	AND
	tbl_ordini.id_fornitore = ?
	AND
	(
		YEAR(tbl_ordini.dt_spedizione) = ?
		AND
		MONTH(tbl_ordini.dt_spedizione) = ?
		AND
		DAY(tbl_ordini.dt_spedizione) = ?
	)
GROUP BY tbl_ordini.ac_nrordine
ORDER BY tbl_ordini.dt_ordine ASC , tbl_ordini.ac_uuid";

	$filter = array (
		$this->session->user['customer_id'] , date('Y'),date('m'),date('d')
	);

	} else {
		$sql = "SELECT
	tbl_ordini.* ,
	tbl_prodotti.*,
	tbl_registrazioni.*,
	tbl_coupons_used.ac_coupon AS coupon_usato,
	tbl_coupons.*,
	tbl_ordini_summary.ac_firstname_delivery AS nome_d,
	tbl_ordini_summary.ac_lastname_delivery AS cognome_d,
	tbl_ordini_summary.ac_company_delivery AS company_d,
	tbl_ordini_summary.ac_address_delivery AS address_d,
	tbl_ordini_summary.ac_zip_delivery AS zip_d,
	tbl_ordini_summary.ac_city_delivery AS city_d,
	tbl_ordini_summary.ac_state_delivery AS state_d,
	tbl_ordini_summary.ac_country_delivery AS country_d,
	tbl_ordini_summary.ac_phone_delivery AS phone_d,
	tbl_ordini_summary.ac_mobile_delivery AS mobile_d,
	tbl_ordini_summary.bl_delivery_delivery AS extra_delivery
FROM
	tbl_ordini
INNER JOIN tbl_prodotti ON tbl_ordini.id_prodotto = tbl_prodotti.id_prodotto
INNER JOIN tbl_registrazioni ON tbl_ordini.id_utente = tbl_registrazioni.idregistrazione
INNER JOIN tbl_ordini_summary ON tbl_ordini.ac_nrordine = tbl_ordini_summary.ac_nrordine
LEFT JOIN tbl_coupons_used ON tbl_ordini.ac_nrordine = tbl_coupons_used.ac_nrordine
LEFT JOIN tbl_coupons ON tbl_coupons_used.ac_coupon = tbl_coupons.ac_codice
WHERE
	tbl_ordini.bl_status = 4
	AND
	(
		YEAR(tbl_ordini.dt_spedizione) = ?
		AND
		MONTH(tbl_ordini.dt_spedizione) = ?
		AND
		DAY(tbl_ordini.dt_spedizione) = ?
	)
GROUP BY tbl_ordini.ac_nrordine
ORDER BY tbl_ordini.dt_ordine ASC , tbl_ordini.ac_uuid";
$filter = array (
	date('Y'),date('m'),date('d')
);
	}

		$query = $this->runQuery($sql,$filter);
		return $query;
	}

	function ordini_allegati(){
		$sql = "SELECT tbl_ordini_summary.id_ordine_summary,
		tbl_ordini_summary.ac_nrordine,
		tbl_ordini_allegati.id_allegato,
		tbl_ordini_allegati.ac_allegato
		FROM tbl_ordini_summary
		INNER JOIN tbl_ordini_allegati ON tbl_ordini_summary.id_ordine_summary = tbl_ordini_allegati.id_ordine_summary";
		$query = $this->db->query($sql);
		return ($query->result_array());
	}

	function ordine_salva_allegato(){
		$app = $this->config->item('application');
		$temp_file 	= $app['temp_image_path'].''.$_POST['file'];
		$new_file = $app['ordini_allegati_path'].''.$_POST['file'];
		copy ( $temp_file , $new_file);
		$data = array(
			'id_ordine_summary'	=> $_POST['ordine'],
			'ac_allegato'	=> $_POST['file']
		);
		return $this->db->insert('tbl_ordini_allegati',$data);
	}

	function ordine_elimina_allegato(){
		$app = $this->config->item('application');
		$file_to_delete = $app['ordini_allegati_path'].''.$_POST['allegato'];
		unlink ( $file_to_delete );
		$this->db->where('id_ordine_summary',$_POST['ordine']);
		return $this->db->delete('tbl_ordini_allegati');
	}


}
