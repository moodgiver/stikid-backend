<?php

class User_Model extends CI_Model {

	function __construct(){
		parent::__construct();

	}

  function check_email($email){
    $sql = "SELECT * FROM tbl_registrazioni WHERE ac_email = ?";
    $filter = array ( $email );
    $query = $this->runQuery ( $sql , $filter );
    return count($query);
  }

	function login_user($form){
		$sql = "SELECT * FROM tbl_utenti WHERE ac_username = ? AND ac_password = ?";
		$filter = array ( $form['username_login'] , $form['password_login'] );
		$query = $this->runQuery ( $sql , $filter );
		return $query;
	}

	function login_designer($form){
		$sql = "SELECT * FROM tbl_designers WHERE ac_username = ? AND ac_password = ?";
		$filter = array ( $form['username_login'] , $form['password_login'] );
		$query = $this->runQuery ( $sql , $filter );
		return $query;
	}

	function registra_utente(){
		$data = array (
			'ac_nome'			=> $_POST['o_nome'],
			'ac_cognome'	=> $_POST['o_cognome'],
			'ac_azienda'	=> $_POST['o_azienda'],
			'ac_indirizzo'=> $_POST['o_indirizzo'],
			'ac_citta'		=> $_POST['o_citta'],
			'ac_pv'				=> $_POST['o_pv'],
			'ac_nazione'	=> $_POST['o_nazione'],
			'ac_cap'			=> $_POST['o_cap'],
			'ac_telefono'	=> $_POST['o_telefono'],
			'ac_cellulare'=> $_POST['o_mobile'],
			'ac_cf'				=> $_POST['o_cf'],
			'ac_piva'			=> $_POST['o_piva'],
			'ac_email'		=> $_POST['o_email'],
			'ac_pwd'			=> $_POST['o_password'],
			'ac_uuid_registrazione' => MD5($_POST['o_password']),
			'dt_data_registrazione'	=> date('Y-m-d H:i:s')
		);
		$this->db->insert('tbl_registrazioni',$data);
		$sql = "SELECT * FROM tbl_registrazioni WHERE ac_email = ? AND ac_pwd = ?";
		$filter = array ( $_POST['o_email'] , $_POST['o_password'] );
		$query = $this->runQuery($sql,$filter);
		$utente = $query[0];

		$new_data = array (
			'customer_id' => $utente['idregistrazione'],
			'customer'    => $utente['ac_nome'].' '.$utente['ac_cognome'],
			'islogged'    => true,
			'email'       => $utente['ac_email'],
			'session_id'  => session_id(),
			'ip'          => $_SERVER['REMOTE_ADDR'],
			'user_agent'  => $this->input->user_agent()
		);
		$this->session->set_userdata('user',$new_data);

		$order = $this->session->userdata('order');

		$order['O_NOME']       = $utente['ac_nome'];
		$order['O_COGNOME']    = $utente['ac_cognome'];
		$order['O_AZIENDA']    = $utente['ac_azienda'];
		$order['O_INDIRIZZO']  = $utente['ac_indirizzo'];
		$order['O_CITTA']      = $utente['ac_citta'];
		$order['O_CAP']        = $utente['ac_cap'];
		$order['O_PV']         = $utente['ac_pv'];
		$order['O_NAZIONE']    = $utente['ac_nazione'];
		$order['O_TELEFONO']   = $utente['ac_telefono'];
		$order['O_MOBILE']     = $utente['ac_cellulare'];
		$order['O_EMAIL']      = $utente['ac_email'];
		$order['O_CF']         = $utente['ac_cf'];
		$order['O_PIVA']       = $utente['ac_piva'];

		$order['OS_NOME']      = $utente['ac_nome'];
		$order['OS_COGNOME']   = $utente['ac_cognome'];
		$order['OS_AZIENDA']   = $utente['ac_azienda'];
		$order['OS_INDIRIZZO'] = $utente['ac_indirizzo'];
		$order['OS_CITTA']     = $utente['ac_citta'];
		$order['OS_CAP']       = $utente['ac_cap'];
		$order['OS_PV']        = $utente['ac_pv'];
		$order['OS_NAZIONE']   = $utente['ac_nazione'];
		$order['OS_TELEFONO']  = $utente['ac_telefono'];
		$order['OS_MOBILE']    = $utente['ac_cellulare'];

		$this->session->set_userdata('order',$order);
		return true;
	}

	function province(){
		$sql = "SELECT * FROM tbl_province WHERE ac_sigla_nazione = ? ORDER BY ac_provincia";
		$filter = array ( $this->config->item('lang') );
		$query = $this->runQuery ( $sql , $filter );
		return $query;
	}

	function save_customer(){
		$ordine = $this->session->order;
		$data = array (
			'ac_nome'			=> $_POST['o_nome'],
			'ac_cognome'	=> $_POST['o_cognome'],
			'ac_azienda'	=> $_POST['o_azienda'],
			'ac_indirizzo'=> $_POST['o_indirizzo'],
			'ac_citta'		=> $_POST['o_citta'],
			'ac_pv'				=> $_POST['o_pv'],
			'ac_nazione'	=> $_POST['o_nazione'],
			'ac_cap'			=> $_POST['o_cap'],
			'ac_telefono'	=> $_POST['o_telefono'],
			'ac_cellulare'=> $_POST['o_mobile'],
			'ac_cf'				=> $_POST['o_cf'],
			'ac_piva'			=> $_POST['o_piva']
		);
		$ordine['O_NOME'] 			= $_POST['o_nome'];
		$ordine['O_COGNOME'] 		=  $_POST['o_cognome'];
		$ordine['O_AZIENDA']		=  $_POST['o_azienda'];
		$ordine['O_INDIRIZZO'] 	=  $_POST['o_indirizzo'];
		$ordine['O_CITTA'] 			=  $_POST['o_citta'];
		$ordine['O_PV'] 				=  $_POST['o_pv'];
		$ordine['O_NAZIONE'] 		=  $_POST['o_nazione'];
		$ordine['O_CAP'] 				=  $_POST['o_cap'];
		$ordine['O_TELEFONO'] 	=  $_POST['o_telefono'];
		$ordine['O_MOBILE'] 		=  $_POST['o_mobile'];
		$ordine['O_CF'] 				=  $_POST['o_cf'];
		$ordine['O_PIVA'] 			=  $_POST['o_piva'];
		$this->db->where('ac_email',$_POST['o_email']);
		$this->db->update('tbl_registrazioni',$data);
	}

	function ordini_utente(){
		$sql = "SELECT
			tbl_ordini_summary.ac_nrordine,
			tbl_ordini_summary.ac_pagamento,
			DATE_FORMAT(tbl_ordini_summary.dt_pagamento,'%d-%m-%Y') AS data_ordine,
			tbl_ordini_summary.ac_totale,
			tbl_ordini.bl_status

		FROM tbl_ordini_summary
		INNER JOIN tbl_ordini ON tbl_ordini_summary.ac_nrordine = tbl_ordini.ac_nrordine

		WHERE
			tbl_ordini_summary.id_utente = ?
			AND
			tbl_ordini.bl_status > 0
			AND
			tbl_ordini_summary.dt_pagamento IS NOT NULL
			AND
			YEAR(tbl_ordini_summary.dt_pagamento) > 2013
			AND
			tbl_ordini_summary.ac_nrordine <> ''
		GROUP BY tbl_ordini_summary.ac_nrordine
		ORDER BY dt_pagamento DESC";
		$filter = array ( $this->session->user['customer_id'] );
		$query = $this->runQuery($sql,$filter);
		return $query;
	}

	function ordine($nr){
		$sql = "SELECT
			tbl_ordini_summary.ac_nrordine,
			tbl_ordini_summary.ac_pagamento,
			tbl_ordini_summary.dt_pagamento,
			tbl_ordini_summary.ac_totale,
			tbl_ordini_summary.ac_sconto,
			tbl_ordini_summary.ac_spesespedizione,
			tbl_ordini.*,
			tbl_prodotti.*

		FROM tbl_ordini
		INNER JOIN tbl_ordini_summary ON tbl_ordini.ac_nrordine = tbl_ordini_summary.ac_nrordine
		INNER JOIN tbl_prodotti ON tbl_ordini.id_prodotto = tbl_prodotti.id_prodotto
		WHERE
			tbl_ordini_summary.ac_nrordine = ? AND tbl_ordini_summary.id_utente = ?";
		$filter = array ( $nr , $this->session->user['customer_id'] );
		$query = $this->runQuery($sql,$filter);
		return $query;
	}

  function runQuery($sql,$filter){
    $query = $this->db->query($sql,$filter);
    return($query->result_array());
	}

	function users (){
		$sql = "SELECT * FROM tbl_registrazioni WHERE ac_uuid_registrazione IS NOT NULL AND YEAR(dt_data_registrazione) > 2015 ORDER BY idregistrazione DESC";
		$query = $this->db->query($sql);
		return($query->result_array());
	}
}
