<?php

class Customers_Model extends CI_Model {

	function __construct(){
		parent::__construct();
		$app = $this->config->item('application');
	}

  function clienti($from,$to){
    $sql="SELECT
			tbl_registrazioni.*	,
			tbl_ordini_summary.*,
			DATE_FORMAT(tbl_ordini_summary.dt_pagamento,'%d-%m-%Y %H:%i') AS dt_pagamento
		FROM
			tbl_registrazioni
		INNER JOIN tbl_ordini_summary ON tbl_registrazioni.idregistrazione = tbl_ordini_summary.id_utente
		WHERE
				tbl_ordini_summary.bl_stato > 0
				AND
        (
  				DATE_FORMAT(dt_pagamento,'%Y%m%d') >= ?
  			AND
  				DATE_FORMAT(dt_pagamento,'%Y%m%d') <= ? )
		ORDER BY
			tbl_registrazioni.ac_cognome, tbl_registrazioni.ac_nome, tbl_ordini_summary.dt_pagamento DESC";
    $filter = array ( $from , $to );
    $query = $this->Vendite_model->runQuery($sql,$filter);
    return $query;
  }

  function cliente_ordini(){
    $sql="SELECT
			tbl_registrazioni.*	,
			tbl_ordini_summary.*,
			DATE_FORMAT(tbl_ordini_summary.dt_pagamento,'%d-%m-%Y %H:%i') AS dt_pagamento
		FROM
			tbl_registrazioni
		INNER JOIN tbl_ordini_summary ON tbl_registrazioni.idregistrazione = tbl_ordini_summary.id_utente
		WHERE
				tbl_ordini_summary.bl_stato > 0
		AND
  	 tbl_registrazioni.idregistrazione = ?
		ORDER BY
			tbl_registrazioni.ac_cognome, tbl_registrazioni.ac_nome, tbl_ordini_summary.dt_pagamento DESC";
    $filter = array ( $_POST['id'] );
    $query = $this->Vendite_model->runQuery($sql,$filter);
    return $query;
  }

  function clienti_search(){
    $sql="SELECT
			tbl_registrazioni.*	,
			tbl_ordini_summary.*,
			DATE_FORMAT(tbl_ordini_summary.dt_pagamento,'%d-%m-%Y %H:%i') AS dt_pagamento
		FROM
			tbl_registrazioni
		INNER JOIN tbl_ordini_summary ON tbl_registrazioni.idregistrazione = tbl_ordini_summary.id_utente
		WHERE
				tbl_ordini_summary.bl_stato > 0
		    and
        (
          tbl_registrazioni.ac_cognome LIKE ?
          OR
          tbl_registrazioni.ac_email LIKE ?
        )
		ORDER BY
			tbl_registrazioni.ac_cognome, tbl_registrazioni.ac_nome, tbl_ordini_summary.dt_pagamento DESC";
    $filter = array ( '%'.$_POST['search'].'%' , '%'.$_POST['search'].'%' );
    $query = $this->Vendite_model->runQuery($sql,$filter);
    return $query;
  }

  function cliente(){
    $this->db->select('*');
    $this->db->where('idregistrazione',$_POST['id']);
    $query = $this->db->get('tbl_registrazioni');
    return($query->result_array());
  }

  function cliente_save(){
    if ( $_POST['pw'] != '' ){
      $pwd = $this->encryption->encrypt($_POST['pw']);
      $params = array (
        'ac_nome'       => $_POST['nome'],
        'ac_cognome'    => $_POST['cognome'],
        'ac_email'      => $_POST['email'],
        'ac_password'   => $pwd,
        'ac_indirizzo'  => $_POST['indirizzo'],
        'ac_citta'      => $_POST['citta'],
        'ac_pv'         => $_POST['pv'],
        'ac_cap'        => $_POST['cap'],
        'ac_nazione'    => $_POST['nazione'],
        'ac_cf'         => $_POST['cf'],
        'ac_piva'       => $_POST['piva']
      );
    } else {
      $params = array (
        'ac_nome'       => $_POST['nome'],
        'ac_cognome'    => $_POST['cognome'],
        'ac_email'      => $_POST['email'],
        'ac_indirizzo'  => $_POST['indirizzo'],
        'ac_citta'      => $_POST['citta'],
        'ac_pv'         => $_POST['pv'],
        'ac_cap'        => $_POST['cap'],
        'ac_nazione'    => $_POST['nazione'],
        'ac_cf'         => $_POST['cf'],
        'ac_piva'       => $_POST['piva']
      );
    }
    $this->db->where('idregistrazione',$_POST['id']);
    return $this->db->update('tbl_registrazioni',$params);
  }

  function registrazioni($from,$to){
    $sql="SELECT
      tbl_registrazioni.*,
      DATE_FORMAT(dt_data_registrazione,'%d-%m-%Y - %H:%i') AS registrazione
    FROM
      tbl_registrazioni
    WHERE
        ac_nome IS NULL AND ac_cognome IS NULL
        AND
        (
  				DATE_FORMAT(dt_data_registrazione,'%Y%m%d') >= ?
  			AND
  				DATE_FORMAT(dt_data_registrazione,'%Y%m%d') <= ?
        )
    ORDER BY
      tbl_registrazioni.ac_email";
    $filter = array ( $from , $to );
    $query = $this->Vendite_model->runQuery($sql,$filter);
    return $query;
  }

	function registrati_search(){
    $sql="SELECT
			tbl_registrazioni.*	,
      DATE_FORMAT(dt_data_registrazione,'%d-%m-%Y - %H:%i') AS registrazione
		FROM
			tbl_registrazioni
		WHERE
        (
          tbl_registrazioni.ac_cognome LIKE ?
          OR
          tbl_registrazioni.ac_email LIKE ?
        )
		ORDER BY
			tbl_registrazioni.ac_cognome, tbl_registrazioni.ac_nome";
    $filter = array ( '%'.$_POST['search'].'%' , '%'.$_POST['search'].'%' );
    $query = $this->runQuery($sql,$filter);
    return $query;
  }

	function runQuery($sql,$filter){
    $query = $this->db->query($sql,$filter);
    return($query->result_array());
	}

}
