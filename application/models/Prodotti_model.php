<?php

class Prodotti_Model extends CI_Model {

	function __construct(){
		parent::__construct();
		$app = $this->config->item('application');
	}

	function runQuery($sql,$filter){
    $query = $this->db->query($sql,$filter);
    return($query->result_array());
	}

  function collezioni(){
    $sql = "SELECT *
      FROM tbl_categorie
      WHERE
      ac_categoria <> ?

      ORDER BY id_famiglia , bl_attivo DESC , int_ordine,
      ac_categoria";
		$filter = array ( '' );
		$query = $this->runQuery ( $sql , $filter );
		return $query;
  }

  function collezione(){
    $sql = "SELECT *
      FROM tbl_categorie
      WHERE
      id_categoria = ?
      ";
		$filter = array ( $_POST['id'] );
		$query = $this->runQuery ( $sql , $filter );
		return $query;
  }

  function collezioni_sito(){
    $sql = "SELECT *
      FROM tbl_categorie
      WHERE
      ac_categoria <> ?
      AND
      id_famiglia = ?
      ORDER BY id_famiglia , bl_attivo DESC , int_ordine,
      ac_categoria";
    $filter = array ( '' , $_POST['sito'] );
    $query = $this->runQuery ( $sql , $filter );
    return $query;
  }

  function save_categoria(){
    $data = array (
      'ac_categoria_lang' 	=> $_POST['collezione'],
      'ac_image'          	=> $_POST['image'],
      'ac_meta_title'     	=> $_POST['meta_title'],
      'ac_meta_description' => $_POST['meta_description'],
      'ac_meta_keys'      	=> $_POST['meta_keys'],
      'bl_attivo'         	=> $_POST['attiva'],
			'ac_link'							=> $_POST['link']
    );
    $this->db->where('id_categoria',$_POST['id']);
    $this->db->update('tbl_categorie',$data);
    return true;
  }

  function prodotti(){
    $sql = "SELECT * FROM tbl_prodotti WHERE id_categoria = ? AND ac_prodotto <> '' ORDER BY ac_prodotto";
    $filter = array ( $_POST['id'] );
    $query = $this->runQuery($sql,$filter);
    return $query;
  }

  function prodotto(){
    $sql = "SELECT
				tbl_prodotti.* ,
				tbl_categorie.ac_categoria,
				tbl_categorie.ac_categoria_lang,
				tbl_categorie.id_famiglia,
				CONCAT(tbl_designers.ac_nome_designer , ' ' , tbl_designers.ac_cognome_designer) AS designer,
				tbl_prodotti_descrizione.ac_descrizione AS descrizione
			FROM tbl_prodotti
			LEFT JOIN tbl_categorie ON tbl_prodotti.id_categoria = tbl_categorie.id_categoria
			LEFT JOIN tbl_designers ON tbl_prodotti.id_designer = tbl_designers.id_designer
			LEFT JOIN tbl_prodotti_descrizione ON tbl_prodotti.id_prodotto = tbl_prodotti_descrizione.id_prodotto
			WHERE
				tbl_prodotti.id_prodotto 	= ?

			GROUP BY tbl_prodotti.id_prodotto";
      $filter = array ( $_POST['id_prodotto'] );
      $query = $this->runQuery($sql,$filter);
      return $query;
  }

	function prodotto_save(){

		$id = $_POST['id'];
		$prodotti_upsell = explode(',',$_POST['prodotti_upsell']);
		if ( count($prodotti_upsell) > 0 ){
			$this->prodotti_upsell($id,$prodotti_upsell);
		}
		$params = array (
			'id_categoria' 				=> $_POST['id_categoria'],
			'id_designer'					=> $_POST['id_designer'],
			'ac_prodotto'					=> $_POST['ac_prodotto'],
			'ac_codice_prodotto'	=> $_POST['ac_codice_prodotto'],
			'ac_W_min'						=> $_POST['ac_W_min'],
			'ac_H_min'						=> $_POST['ac_H_min'],
			'ac_W_max'						=> $_POST['ac_W_max'],
			'ac_H_max'						=> $_POST['ac_H_max'],
			'ac_ratio'						=> (int)$_POST['ac_W_min']/(int)$_POST['ac_H_min'],
			'bl_colore'						=> $_POST['bl_colore'],
			'bl_simulatore'				=> $_POST['bl_simulatore'],
			'ac_descrizione'			=> $_POST['descrizione'],
			'ac_meta_title'				=> $_POST['meta_title'],
			'ac_meta_description'	=> $_POST['meta_description'],
			'ac_meta_keys'				=> $_POST['meta_keywords'],
			'bl_stato'						=> $_POST['bl_stato'],
			'ac_coefficiente_sconto' => $_POST['ac_coefficiente_sconto']
		);
		$this->db->where('id_prodotto',$id);
		$this->db->update('tbl_prodotti',$params);
		$desc_params = array (
			'ac_descrizione' => $_POST['descrizione']
		);
		$this->db->where('id_prodotto',$id);
		return $this->db->update('tbl_prodotti_descrizione',$desc_params);
	}

  function prodotto_copy(){
		$sql = "SELECT * FROM tbl_prodotti WHERE id_prodotto = ?";
		$qry = $this->db->query($sql,$_POST['id_prodotto']);
		$query = $qry->result_array();
		$params = array(
			'id_categoria' 				=> $query[0]['id_categoria'],
			'id_designer'	 				=> $query[0]['id_designer'],
			'ac_prodotto'	 				=> $_POST['prodotto'].' (copy)',
			'ac_codice_prodotto'	=> $query[0]['ac_codice_prodotto'],
			'ac_ratio'						=> $query[0]['ac_ratio'],
			'ac_W'								=> $query[0]['ac_W'],
			'ac_H'								=> $query[0]['ac_H'],
			'ac_W_min'						=> $query[0]['ac_W_min'],
			'ac_H_min'						=> $query[0]['ac_H_min'],
			'ac_W_max'						=> $query[0]['ac_W_max'],
			'ac_H_max'						=> $query[0]['ac_H_max'],
			'ac_prezzo'						=> $query[0]['ac_prezzo'],
			'bl_colore'						=> $query[0]['bl_colore'],
			'ac_meta_title'				=> $query[0]['ac_meta_title'],
			'ac_meta_description' => $query[0]['ac_meta_description'],
			'ac_meta_keys'				=> $query[0]['ac_meta_keys'],
		 	'ac_meta_description' => $query[0]['ac_meta_description'],
			'ac_coefficiente_sconto'	=> $query[0]['ac_coefficiente_sconto'],
			'bl_stato'						=> 0
		);
		$this->db->insert('tbl_prodotti',$params);
		$insert_id = $this->db->insert_id();
		$descr	= array (
			'id_prodotto'			=> $insert_id,
			'id_lingua'				=> 2,
			'ac_descrizione' 	=> ''
		);
		$this->db->insert('tbl_prodotti_descrizione',$descr);
		return true;
	}

	function prodotto_save_image(){
		$app = $this->config->item('application');
		$temp_file 	= $app['temp_image_path'].''.$_POST['image'];
		$resizer = false;
		switch ($_POST['tipo']){

			case 'evidenza':
			$new_file 	= $app['product_image_path'].'large/'.$_POST['image'];
			$uri 	= $app['product_image_uri'].'medium/'.$_POST['image'];
			$resizer = true;
			$data = array (
					'uri_img' 		=> $uri,
					'ac_immagine'	=> $_POST['image']
			);
			$this->db->where('id_prodotto',$_POST['id_prodotto']);
			$res = $this->db->update('tbl_prodotti',$data);
			print_r($res);
			break;

			case 'vettoriale':
			$resizer = false;
			$new_file 	= $app['vettoriali_path'].''.$_POST['image'];
			$uri 	= $app['vettoriali_uri'].''.$_POST['image'];
			$data = array (
					'uri_eps' 		=> $uri,
					'ac_sticker'	=> $_POST['image']
			);
			$this->db->where('id_prodotto',$_POST['id_prodotto']);
			$this->db->update('tbl_prodotti',$data);
			break;

			case 'sticker':
			$resizer = false;
			$new_file 	= $app['product_sticker_path'].''.$_POST['image'];
			$uri 	= $app['product_sticker_uri'].''.$_POST['image'];
			$data = array (
					'uri_sticker' 		=> $uri,
					'ac_image'	=> $_POST['image']
			);
			$this->db->where('id_prodotto',$_POST['id_prodotto']);
			$this->db->update('tbl_prodotti',$data);
			break;

		}
		copy ( $temp_file , $new_file);
		if ( $resizer ){
			$this->doResizeImage($new_file,$_POST['image'],$_POST['tipo']);
		}
		return true;
	}

	function doResizeImage($source,$name,$tipo){
		$app = $this->config->item('application');
		if ( $tipo == 'evidenza' ){
			$this->load->library('image_lib');
			$config['image_library'] = 'gd2';
			$config['source_image']	= $source;
			$config['create_thumb'] = FALSE;
			$config['maintain_ratio'] = TRUE;
			$config['height']	= "600";
			$config['new_image'] = $app['product_image_path'].'medium/'.$name;
			$this->image_lib->initialize($config);
			$this->image_lib->resize();


			$config['source_image']	= $source;
			$config['height']	= "300";
			$config['new_image'] = $app['product_image_path'].'small/'.$name;
			$this->image_lib->initialize($config);
			$this->image_lib->resize();
		}
		return true;
	}

	function prodotti_upsell($id,$prodotti){
		$this->db->where('id_prodotto',$id);
		$this->db->delete('tbl_prodotti_upsell');
		foreach ( $prodotti AS $prod ){
			$params = array (
				'id_prodotto'				=> $id ,
				'id_prodotto_upsell'=> $prod
			);
			$this->db->insert('tbl_prodotti_upsell',$params);
		}
	}

	function prodotti_upsell_list(){
		$sql = "SELECT
			pu.id_prodotto AS id,
			a.ac_prodotto AS prodotto,
			c.ac_categoria AS collezione,
			c.ac_categoria_lang AS collezione_extra,
			pu.id_prodotto_upsell,
			b.ac_prodotto AS prodotto_upsell,
			d.ac_categoria AS collezione_upsell,
			d.ac_categoria_lang AS collezione_upsell_extra
			From
			tbl_prodotti_upsell AS pu
			Inner Join tbl_prodotti AS a ON pu.id_prodotto = a.id_prodotto
			Inner Join tbl_prodotti AS b ON pu.id_prodotto_upsell = b.id_prodotto
			Inner Join tbl_categorie AS c ON a.id_categoria = c.id_categoria
			Inner Join tbl_categorie AS d ON b.id_categoria = d.id_categoria";
		$filter = array();
		$query = $this->runQuery($sql,$filter);
		return $query;
	}

	function prodotto_search(){
		$array = array ( 'ac_prodotto' => $_POST['search'] , 'ac_codice_prodotto' => $_POST['search'] );
		$this->db->select('*');
		$this->db->or_like($array);
		$query = $this->db->get('tbl_prodotti');
		return($query->result_array());
	}


  function prodotto_gallery(){
    $sql = "SELECT * FROM tbl_prodotti_immagini WHERE id_prodotto = ?";
    $filter = array ( $_POST['id_prodotto'] );
    $query = $this->runQuery($sql,$filter);
    return $query;
  }

  function prodotto_simulatore(){
    $sql = "SELECT * FROM tbl_prodotti_simulatore WHERE id_prodotto = ?";
    $filter = array ( $_POST['id_prodotto'] );
    $query = $this->runQuery($sql,$filter);
    return $query;
  }

	function simulatore_add_image(){
		$app = $this->config->item('application');
		$image_uri 	= '//cdn.stikid.com/images/simulatore/'.''.$_POST['ac_immagine'];
		$new_file 	= $app['simulatore_path'].''.$_POST['ac_immagine'];
		$temp_file 	= $app['temp_image_path'].''.$_POST['ac_immagine'];
		$data = array (
			'id_prodotto' 	=> $_POST['id_prodotto'],
			'ac_immagine' 	=> $image_uri,
			'ac_width_cm'	 	=> $_POST['ac_w_cm'],
			'ac_height_cm'	=> $_POST['ac_h_cm'],
			'ac_width_px'	 	=> $_POST['ac_w_px'],
			'ac_height_px'	=> $_POST['ac_h_px'],
		);
		copy ( $temp_file , $new_file);
		$this->db->insert('tbl_prodotti_simulatore',$data);
		$last_id = $this->db->insert_id();
    return $last_id;
	}

	function simulatore_remove_image(){
		$this->db->where('id_simulatore',$_POST['id']);
		$this->db->delete('tbl_prodotti_simulatore');
		return true;
	}

}
