<?php

class Vendite_Model extends CI_Model {

	function __construct(){
		parent::__construct();
	}

	function runQuery($sql,$filter){
    $query = $this->db->query($sql,$filter);
    return($query->result_array());
	}

  function vendite($from,$to){
    $sql = "SELECT
			ac_nrordine,
			SUM(ac_sconto) AS sconto ,
			SUM(ac_totale) AS totale ,
			COUNT(id_ordine_summary) AS nr ,
			DATE_FORMAT(dt_pagamento,'%d-%m-%Y') AS dt_pagamento , ac_pagamento
			FROM tbl_ordini_summary
			WHERE (
				DATE_FORMAT(dt_pagamento,'%Y%m%d') >= ?
			AND
				DATE_FORMAT(dt_pagamento,'%Y%m%d') <= ? )
			GROUP BY date_format(dt_pagamento,'%m-%d-%Y')
			ORDER BY DATE_FORMAT(dt_pagamento,'%Y%m%d') ASC";
    	$filter = array ( $from , $to );
    	$query = $this->runQuery($sql,$filter);
			return $query;
  }

	function vendite_coupon($from,$to){
    $sql = "SELECT
			ac_nrordine,
			SUM(ac_sconto) AS sconto ,
			SUM(ac_totale) AS totale ,
			COUNT(id_ordine_summary) AS nr ,
			DATE_FORMAT(dt_pagamento,'%d-%m-%Y') AS dt_pagamento , ac_pagamento
			FROM tbl_ordini_summary
			WHERE (
				DATE_FORMAT(dt_pagamento,'%Y%m%d') >= ?
			AND
				DATE_FORMAT(dt_pagamento,'%Y%m%d') <= ? )
			AND
				ac_sconto <> '0'
			GROUP BY date_format(dt_pagamento,'%m-%d-%Y')
			ORDER BY DATE_FORMAT(dt_pagamento,'%Y%m%d') ASC";
    	$filter = array ( $from , $to );
    	$query = $this->runQuery($sql,$filter);
			return $query;
  }

	function vendite_prodotti($from,$to){
		$sql = "SELECT
			COUNT(tbl_ordini.id_prodotto) AS venduti ,
			tbl_prodotti.*,
			tbl_categorie.ac_categoria,
			tbl_categorie.ac_categoria_lang,
			CONCAT(tbl_designers.ac_nome_designer , ' ' , tbl_designers.ac_cognome_designer) AS designer
			FROM tbl_ordini
				INNER JOIN tbl_prodotti ON tbl_ordini.id_prodotto = tbl_prodotti.id_prodotto
				INNER JOIN tbl_categorie ON tbl_prodotti.id_categoria = tbl_categorie.id_categoria
				LEFT JOIN tbl_designers ON tbl_prodotti.id_designer = tbl_designers.id_designer
			WHERE (
				DATE_FORMAT(dt_ordine,'%Y%m%d') >= ?
			AND
				DATE_FORMAT(dt_ordine,'%Y%m%d') <= ? )
				AND
						tbl_ordini.bl_status > 1
				GROUP BY tbl_prodotti.id_prodotto
				ORDER BY venduti DESC";
		$filter = array ( $from , $to );
		$query = $this->runQuery($sql,$filter);
		return $query;
	}

	function vendite_clienti($from,$to){
		$sql = "SELECT
				COUNT(id_ordine_summary) AS ordini,
				tbl_registrazioni.idregistrazione,
				tbl_registrazioni.ac_nome,
				tbl_registrazioni.ac_cognome,
				tbl_registrazioni.ac_email
				From
					tbl_registrazioni
				Inner Join tbl_ordini_summary ON tbl_ordini_summary.id_utente =         tbl_registrazioni.idregistrazione
				Where
					tbl_ordini_summary.bl_stato > 1
				AND
		 			(
						DATE_FORMAT(dt_pagamento,'%Y%m%d') >= ?
						AND
						DATE_FORMAT(dt_pagamento,'%Y%m%d') <= ?
					)
					Group By tbl_registrazioni.idregistrazione
					ORDER BY ordini DESC";
		$filter = array ( $from , $to );
		$query = $this->runQuery($sql,$filter);
		return $query;
	}

	function costi_fornitore($form){
		$sql = "SELECT
			tbl_ordini.* ,
			tbl_prodotti.ac_codice_prodotto,
			tbl_prodotti.ac_prodotto,
			tbl_prodotti.ac_immagini_demo,
			tbl_prodotti.ac_image,
			tbl_prodotti.ac_sticker,
			tbl_prodotti.bl_colore,
			tbl_prodotti.ac_coefficiente_sconto,
			DATE_FORMAT(tbl_ordini_summary.dt_pagamento,'%d-%m-%Y') AS dt_pagamento,
			tbl_ordini_summary.ac_totale AS TOTALE,
			tbl_ordini_summary.ac_sconto AS SCONTO
		FROM tbl_ordini
		INNER JOIN tbl_prodotti ON tbl_ordini.id_prodotto = tbl_prodotti.id_prodotto
		INNER JOIN tbl_ordini_summary ON tbl_ordini.ac_nrordine = tbl_ordini_summary.ac_nrordine
		WHERE
			(YEAR(dt_pagamento) = ? AND MONTH(dt_pagamento) = ?)
			AND tbl_ordini.id_fornitore = ?
			AND tbl_ordini.bl_status > 3
		ORDER BY dt_ordine";
		$filter = array ( $form['anno'] , $form['mese'], $form['id'] );
		$query = $this->runQuery($sql,$filter);
		return $query;
	}

	function prezzi(){
		$sql = 'SELECT * FROM tbl_pricing';
		$query = $this->db->query($sql);
		return($query->result_array());
	}
	function costi($form){
		$sql = "SELECT * FROM tbl_costi_sticker WHERE id_fornitore = ?";
		$filter = array ( $form['id'] );
		$query = $this->runQuery($sql,$filter);
		return $query;
	}

	function fornitori(){
		$this->db->select('*');
		$this->db->where('bl_tipo',1);
		$this->db->order_by('ac_nickname');
		$query = $this->db->get('tbl_utenti');
		return $query->result_array();
	}

	function fornitore(){
		$this->db->select('*');
		$this->db->where('id_utente',$_POST['id']);
		$query = $this->db->get('tbl_utenti');
		return $query->result_array();
	}

	function spedizioni(){
		$this->db->select('*');
		$query = $this->db->get('tbl_spedizioni');
		return $query->result_array();
	}

	function coupons(){
		$this->db->select('*');
		$this->db->order_by('ac_coupon');
		$query = $this->db->get('tbl_coupons');
		return $query->result_array();
	}

	function coupon(){
		$sql = "SELECT * ,
		DATE_FORMAT(dt_start,'%d-%m-%Y') AS dt_start,
		DATE_FORMAT(dt_end,'%d-%m-%Y') AS dt_end
		FROM tbl_coupons WHERE id_coupon = ?";
		$filter = array ( $_POST['id'] );
		$query = $this->runQuery($sql,$filter);
		return $query;
	}

	function coupon_search(){
		$sql = "SELECT * ,
		DATE_FORMAT(dt_start,'%d-%m-%Y') AS dt_start,
		DATE_FORMAT(dt_end,'%d-%m-%Y') AS dt_end
		FROM tbl_coupons WHERE ac_coupon LIKE ?";
		$filter = array ( '%'.$_POST['search'].'%' );
		$query = $this->runQuery($sql,$filter);
		return $query;
	}

  function coupon_save(){
    $params = array (
      'ac_coupon' => $_POST['ac_coupon'],
      'dt_start'  => $_POST['dt_start'],
      'dt_end'    => $_POST['dt_end'],
      'ac_valore' => $_POST['ac_valore'],
      'bl_valore' => $_POST['bl_valore'],
      'ac_codice' => $_POST['ac_codice'],
      'bl_attivo' => $_POST['bl_attivo']
    );
    return $this->db->insert('tbl_coupons',$params);
  }

}
