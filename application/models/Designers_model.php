<?php

class Designers_Model extends CI_Model {

	function __construct(){
		parent::__construct();
		$app = $this->config->item('application');
	}

  function designers(){
    $this->db->select('*');
    $this->db->order_by('ac_cognome_designer,ac_nome_designer');
    $query = $this->db->get('tbl_designers');
    return ( $query->result_array() );
  }
  function designer(){
    $this->db->select('*');
    $this->db->where('id_designer',$_POST['id']);
    $query = $this->db->get('tbl_designers');
    return ( $query->result_array() );
  }

	function designers_vendite(){
		$sql = "SELECT
				tbl_prodotti.id_prodotto,
				tbl_prodotti.ac_codice_prodotto,
				tbl_prodotti.ac_prodotto,
				tbl_designers.id_designer,
				CONCAT(tbl_designers.ac_nome_designer,' ',tbl_designers.ac_cognome_designer) AS designer,
				SUM(tbl_ordini.ac_qty) AS venduti_2016,
				tbl_designers.ac_value*(SUM(tbl_ordini.ac_qty)) AS royalties_2016,
				tbl_prodotti.ac_costo AS royalties_al_2015,
				tbl_designers.ac_nome_designer,
				tbl_designers.ac_cognome_designer,
				tbl_designers.ac_value_max,
				IF(CONVERT(tbl_prodotti.ac_costo,SIGNED INTEGER) > CONVERT(ac_value_max,SIGNED INTEGER),0,CONVERT(ac_value_max,SIGNED INTEGER)-CONVERT(tbl_prodotti.ac_costo,SIGNED INTEGER)) AS residuo,
				IF(
	CONVERT(tbl_designers.ac_value*(SUM(tbl_ordini.ac_qty)),SIGNED INTEGER)  < CONVERT(ac_value_max,SIGNED INTEGER)-CONVERT(tbl_prodotti.ac_costo,SIGNED INTEGER) ,
	tbl_designers.ac_value*(SUM(tbl_ordini.ac_qty)),
				IF(CONVERT(ac_value_max,SIGNED INTEGER)-CONVERT(tbl_prodotti.ac_costo,SIGNED INTEGER) > 0 ,
				CONVERT(ac_value_max,SIGNED INTEGER)-CONVERT(tbl_prodotti.ac_costo,SIGNED INTEGER),
	0)) AS pagare_ora
			From
				tbl_ordini
				Inner Join tbl_prodotti ON tbl_ordini.id_prodotto = tbl_prodotti.id_prodotto
				Inner Join tbl_designers ON tbl_prodotti.id_designer = 						tbl_designers.id_designer

				WHERE
						(
						DATE_FORMAT(tbl_ordini.dt_ordine,'%Y%m%d') >= ?
						AND
						DATE_FORMAT(tbl_ordini.dt_ordine,'%Y%m%d') <= ?
						)
				AND
					tbl_designers.id_designer = ?
				AND tbl_ordini.bl_status > 2
				GROUP BY tbl_prodotti.id_prodotto
				ORDER BY tbl_designers.id_designer,tbl_prodotti.ac_prodotto";
				$filter = array (
					$_POST['from'] , $_POST['to'] , $_POST['id']
				);
		$query = $this->db->query($sql,$filter);
		return ( $query->result_array() );
	}

	function designers_vendite_export(){
		$sql = "SELECT
				tbl_prodotti.id_prodotto,
				tbl_prodotti.ac_codice_prodotto,
				tbl_prodotti.ac_prodotto,
				tbl_designers.id_designer,
				CONCAT(tbl_designers.ac_nome_designer,' ',tbl_designers.ac_cognome_designer) AS designer,
				SUM(tbl_ordini.ac_qty) AS venduti_2016,
				tbl_designers.ac_value*(SUM(tbl_ordini.ac_qty)) AS royalties_2016,
				tbl_prodotti.ac_costo AS royalties_al_2015,
				tbl_designers.ac_nome_designer,
				tbl_designers.ac_cognome_designer,
				tbl_designers.ac_value_max,
				IF(CONVERT(tbl_prodotti.ac_costo,SIGNED INTEGER) > CONVERT(ac_value_max,SIGNED INTEGER),0,CONVERT(ac_value_max,SIGNED INTEGER)-CONVERT(tbl_prodotti.ac_costo,SIGNED INTEGER)) AS residuo,
				IF(
	CONVERT(tbl_designers.ac_value*(SUM(tbl_ordini.ac_qty)),SIGNED INTEGER)  < CONVERT(ac_value_max,SIGNED INTEGER)-CONVERT(tbl_prodotti.ac_costo,SIGNED INTEGER) ,
	tbl_designers.ac_value*(SUM(tbl_ordini.ac_qty)),
				IF(CONVERT(ac_value_max,SIGNED INTEGER)-CONVERT(tbl_prodotti.ac_costo,SIGNED INTEGER) > 0 ,
				CONVERT(ac_value_max,SIGNED INTEGER)-CONVERT(tbl_prodotti.ac_costo,SIGNED INTEGER),
	0)) AS pagare_ora
			From
				tbl_ordini
				Inner Join tbl_prodotti ON tbl_ordini.id_prodotto = tbl_prodotti.id_prodotto
				Inner Join tbl_designers ON tbl_prodotti.id_designer = 						tbl_designers.id_designer

		WHERE
			(
			DATE_FORMAT(tbl_ordini.dt_ordine,'%Y%m%d') >= ?
			AND
			DATE_FORMAT(tbl_ordini.dt_ordine,'%Y%m%d') <= ?
			)
		AND
		tbl_designers.id_designer = ?
		AND tbl_ordini.bl_status > 2
		GROUP BY tbl_prodotti.id_prodotto
		ORDER BY tbl_designers.id_designer,tbl_prodotti.ac_prodotto";
		$filter = array (
			$_POST['from'] , $_POST['to'] , $_POST['id']
		);
		$query = $this->db->query($sql,$filter);
		return $query;
	}
}
