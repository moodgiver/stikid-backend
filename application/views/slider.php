
		<div class="container slider-desktop hide" style="margin-left:-15px">
			<div class="row">
				<div class="col-sm-12" style="background:##fff;opacity:.8">
					<div id="slider-carousel" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
              <?php
                $active = 'active';
                foreach ( $slides AS $sl ){
                  if ( $sl['bl_image_type'] == 0 ){
                  echo '<li data-target="#slider-carousel" data-slide-to="#i#" class="'.$active.'"></li>';
                  $active = '';
                  }
                }
              ?>
						</ol>

						<div class="carousel-inner text-center" role="listbox">
              <?php
                $active = 'active';
                foreach ( $slides AS $sl ){
                  if ( $sl['bl_image_type'] == 0 ){
                  echo '
                  <div class="item '.$active.'" style="padding:0;">
                    <img src="//cdn.stikid.com/images/slides/'.$sl['ac_slide_image'].'" style="width:100%	;height:auto;">
                  </div>
                  ';
                  $active = '';
                  }
                }
              ?>
						</div>
						<a href="##slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
							<i class="fa fa-angle-left"></i>
						</a>
						<a href="##slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
							<i class="fa fa-angle-right"></i>
						</a>
					</div>

				</div>
			</div>
		</div>

    <div class="container slider-mobile hide">
      <div class="row">
        <div class="col-sm-12" style="background:##fff;opacity:.8">
          <div id="slider-carousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
              <?php
                $active = 'active';
                foreach ( $slides AS $sl ){
                  if ( $sl['bl_image_type'] == 1 ){
                  echo '<li data-target="#slider-carousel" data-slide-to="#i#" class="'.$active.'"></li>';
                  $active = '';
                  }
                }
              ?>
            </ol>

            <div class="carousel-inner text-center" role="listbox">
              <?php
                $active = 'active';
                foreach ( $slides AS $sl ){
                  if ( $sl['bl_image_type'] == 1 ){
                  echo '
                  <div class="item '.$active.'" style="padding:0;">
                    <img src="//cdn.stikid.com/images/slides/'.$sl['ac_slide_image'].'" style="width:100%	;height:auto;">
                  </div>
                  ';
                  $active = '';
                  }
                }
              ?>
            </div>
            <a href="##slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
              <i class="fa fa-angle-left"></i>
            </a>
            <a href="##slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
              <i class="fa fa-angle-right"></i>
            </a>
          </div>

        </div>
      </div>
    </div>

  <script>
  $(document).ready(function(){
    var w = $(window).width();
    if ( w < 640 ){
      $('.slider-desktop').remove();
      $('.slider-mobile').removeClass('hide');
    } else {
      $('.slider-mobile').remove();
      $('.slider-desktop').removeClass('hide');
    }
  });
  </script>
