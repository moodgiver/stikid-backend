<?php
  include_once('coupons.php');
  
  _create_empty_form_record ( $schema , $name , true,  $action );

  if ( isset($_POST['id']) ){
    echo '<br><br>';
    //print_r($cp);
    $row = $cp[0];
    echo '<div class="col-lg-12">';
    _create_form_record ( $schema , $name , $row , false , 0 , $action);
    echo '</div>';
}
?>


<script>
$(document).ready(function(){

  $('body').delegate('.coupon-data','click',function(){
    var id = $(this).data('id');
    if ( id != '' ){
      $.post ( 'ajax/settings' ,
        {
          action: 'settings-coupons',
          id: id
        }, function(result){
          $('.content').html(result);
        }
      )
    }
  })
  $('.coupon').on('change',function(){
    var id = $(this).val();
    if ( id != '' ){
      $.post ( 'ajax/settings' ,
        {
          action: 'settings-coupons',
          id: id
        }, function(result){
          $('.content').html(result);
        }
      )
    }
  })

  $('.coupon-search').on('keyup',function(){
    var search = $(this).val();
    if ( search.length > 3 ){
      $.post ( 'ajax/settings' ,
        {
          action: 'settings-coupons-search',
          search: search
        }, function(result){
          $('.search-result').html(result);
        }
      )
    }
  })
})
</script>
