<div class="row">
  <div class="col-lg-12">
    <div class="col-lg-4">
      <strong>Sito</strong> <select class="form-control filtra-sito">
        <option value="-1" selected>seleziona ...</option>
        <?php
        $tipi = [ 'script','html' ];
        $posizioni = ['head','body'];
        $status = ['non attivo','attivo'];
        $selected = '';
        $currentSito = -1;
        if ( isset($_POST['sito'] ) ){
          $currentSito = $_POST['sito'];
        }
        foreach ( $siti AS $sito ){
          $selected = '';
          $site = $sito['sito'];
          if ( $currentSito == $sito['id'] ){
            $selected = 'selected';
          }
          echo '<option value="'.$sito['id'].'" '.$selected.'>'.$site.'</option>';
        }
        ?>
      </select>
    </div>
  </div>
  <div class="col-lg-12">
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
          <th>Nome</th>
          <th>Posizione</th>
          <th>Tipo</th>
          <th>Stato</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <?php
        foreach ( $settings AS $row ){
          $color = 'green';
          if ( $row['bl_attivo'] == 0 ){
            $color = 'red';
          } 
          echo '
          <tr class="riga hide riga_sito_'.$row['int_sito'].'">
            <td style="color:'.$color.'" class="pointer edit-content" data-id="'.$row['id_setting'].'"><span class="titolo_setting_'.$row['id_setting'].'">'.$row['ac_setting'].'</span></td>
            <td>
            <select class="form-control posizione posizione_'.$row['id_setting'].'" data-id="'.$row['id_setting'].'">';
            $n = 1;
            foreach ( $posizioni AS $pos ){
              $selected = '';
              if ( $row['int_position'] == $n ){
                $selected = 'selected';
              }
              echo '<option value="'.$n.'" '.$selected.'>'.$pos.'</option>';
              $n++;
            }
            echo '</select></td>
            <td><select class="form-control tipo tipo_'.$row['id_setting'].'" data-id="'.$row['id_setting'].'">';
            $n = 1;
            foreach ( $tipi AS $tipo ){
              $selected = '';
              if ( $n == $row['type'] ){
                $selected = 'selected';
              }
              echo '<option value="'.$n.'" '.$selected.'>'.$tipo.'</option>';
              $n++;
            }
            echo '</select></td>
            <td><select class="form-control attivo attivo_'.$row['id_setting'].'" data-id="'.$row['id_setting'].'">';
            for ( $n=0 ; $n<2 ; $n++ ){
              $selected = '';
              if ( $n == $row['bl_attivo'] ){
                $selected = 'selected';
              }
              echo '<option value="'.$n.'" '.$selected.'>'.$status[$n].'</option>';
            }
            echo '</select></td>
            <td><button class="btn btn-sm btn-primary btn-save-setting" data-id="'.$row['id_setting'].'">Salva</button></td>
          </tr>
          <tr class="hide riga_content" id="content_'.$row['id_setting'].'" data-visible="0">
            <td colspan="5">
              <input type="text" class="form-control setting setting_'.$row['id_setting'].'" value="'.$row['ac_setting'].'">
              <textarea class="form-control content-textarea textarea_'.$row['id_setting'].'" data-id="'.$row['id_setting'].'">'.$row['ac_content'].'</textarea>
            </td>
          </tr>
          ';
        }
        ?>
      </tbody>
    </table>
  </div>
</div>

<script>
$(document).ready(function(){

  $('.filtra-sito').on('change',function(){
    var sito = $(this).val();
    $('.riga').addClass('hide');
    $('.riga_sito_' + sito).removeClass('hide');
  })

  $('.edit-content').on('click',function(){
    var e = $('#content_' + $(this).data('id'));
    $('.riga_content').addClass('hide');
    console.log ( e.data('visible') );
    if ( e.data('visible') == 0 ){
      e.removeClass('hide');
      e.attr('data-visible',1);
    } else {
      e.addClass('hide');
      e.attr('data-visible',0);
    }
  })

  $('.btn-save-setting').on('click',function(){
    var id = $(this).data('id');
    $.post ( 'ajax/settings' ,
      {
        action: 'settings-save-setting',
        id: id,
        setting: $('.setting_' + id).val(),
        posizione: $('.posizione_'+id).val(),
        tipo: $('.tipo_'+id).val(),
        attivo: $('.attivo_'+id).val(),
        content: $('.textarea_'+id).val()
      }, function(result){
        $('.titolo_setting_'+id).html ( $('.setting_'+id).val() );
        doNotification('Impostazioni','Dati salvati correttamente');
      }
    )
  })


})
</script>
