<div class="row">
  <div class="col-lg-6">
    <table class="table table-bordered table-striped" width="100%">
      <tr>
        <th>Area</th>
        <th>Prezzo MQ</th>
        <th>Prezzo BASE Sticker</th>
        <th></th>
      </tr> 
      <?php
      foreach ( $prezzi AS $row ){
        echo '
        <tr class="row_'.$row['ac_area'].'">
        <td><input type="text" class="form-control area_'.$row['id_prezzo'].'" value="'.$row['ac_area'].'"></td>
        <td><input type="text" class="form-control prezzomq_'.$row['id_prezzo'].'" value="'.$row['ac_prezzo'].'"></td>
        <td><input type="text" class="form-control prezzoreale_'.$row['id_prezzo'].'" value="'.$row['ac_prezzo_reale'].'"></td>
        <td><button class="btn btn-sm btn-primary btn-save-prezzo" data-id="'.$row['id_prezzo'].'">Salva</button></td>
        </tr>
        ';
      }
      ?>
    </table>
  </div>
</div>

<script>
$(document).ready(function(){
  $('.btn-save-prezzo').on('click',function(){
    var id = $(this).data('id');
    $.post ( 'ajax/settings' ,
      {
        action: 'settings-save-prezzo',
        id: id,
        area: $('.area_'+id).val(),
        prezzo: $('.prezzomq_'+id).val(),
        prezzo_reale: $('.prezzoreale_'+id).val()
      }, function(result){
        if ( result ){
          doNotification('Impostazioni','Prezzo salvato correttamente');
        } else {
          doNotification('Impostazioni','Errore nel salvataggio');
        }
      }
    )
  })
})
</script>
