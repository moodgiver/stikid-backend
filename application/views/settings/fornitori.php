<div class="row">
<div class="col-lg-4">
  <label>Fornitore</label>
  <select class="form-control fornitore" data-mode="<?=$mode?>">
    <option value="">Fornitore ...</option>
    <?php
    $id = 0;
    if ( isset($_POST['id']) ){
      $id = $_POST['id'];
    }
    foreach ( $fornitori AS $row ){
      $selected = '';
      if ( $id == $row['id_utente'] ){
        $selected = 'selected';
      }
      echo '<option value="'.$row['id_utente'].'" '.$selected.'>'.$row['ac_cognome'].' '.$row['ac_nome'].'</option>';
    }
    ?>
  </select>
</div>
</div>
<script>
$(document).ready(function(){
  $('.fornitore').on('change',function(){
    var id = $(this).val();
    $.post ( 'ajax/settings' ,
      {
        action: $(this).data('mode'),
        id: id
      }, function(result){
        $('.content').html(result);
      }
    )
  })
})
</script>
