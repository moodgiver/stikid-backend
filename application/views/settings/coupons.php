<div class="col-lg-6">
  <select class="form-control coupon">
    <option value="">Seleziona...</option>
  <?php
    foreach ( $coupons AS $coupon ){
      $selected = '';
      if ( isset($_POST['id']) ){
        if ( $_POST['id'] == $coupon['id_coupon'] ){
          $selected = 'selected';
        }
      }
      echo '<option value="'.$coupon['id_coupon'].'" '.$selected.'>'.$coupon['ac_coupon'].'</option>';
    }
  ?>
  </select>
</div>
<div class="col-lg-4">
  <input class="form-control coupon-search" value="<?=$_POST['search']?>" placeholder="Cerca coupon ...">
</div>
<div class="col-lg-2">
  <button class="btn btn-sm btn-primary pull-right btn-new-record" data-form="new-record-form">Aggiungi</button>
</div>
<div class="row">
  <div class="col-lg-12 search-result">
  </div>
</div>
