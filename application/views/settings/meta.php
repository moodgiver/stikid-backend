<div class="row">
  <div class="col-lg-12">
    <div class="col-lg-4">
      <strong>Sito</strong> <select class="form-control filtra-sito">
        <option value="-1" selected>seleziona ...</option>
        <?php
        $tipi = [ 'script','html' ];
        $posizioni = ['head','body'];
        $status = ['non attivo','attivo'];
        $selected = '';
        $currentSito = -1;
        if ( isset($_POST['sito'] ) ){
          $currentSito = $_POST['sito'];
        }
        foreach ( $siti AS $sito ){
          $selected = '';
          $site = $sito['sito'];
          if ( $currentSito == $sito['id'] ){
            $selected = 'selected';
          }
          echo '<option value="'.$sito['id'].'" '.$selected.'>'.$site.'</option>';
        }
        ?>
      </select>
    </div>
  </div>
  <div class="col-lg-12">
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
          <th>Nome</th>
          <th>Content</th>
          <th>Stato</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <?php
        foreach ( $settings AS $row ){
          $color = 'green';
          if ( $row['bl_attivo'] == 0 ){
            $color = 'red';
          }
          echo '
          <tr class="riga hide riga_sito_'.$row['int_sito'].'">
            <td style="color:'.$color.'" class="pointer edit-content" data-id="'.$row['id_setting'].'">'.$row['ac_setting'].'</td>
            <td>
              <textarea class="form-control content-textarea textarea_'.$row['id_setting'].'" data-id="'.$row['id_setting'].'">'.$row['ac_content'].'</textarea>
            </td>
            <td><button class="btn btn-sm btn-primary btn-save-setting" data-id="'.$row['id_setting'].'">Salva</button></td>
          </tr>
          ';
        }
        ?>
      </tbody>
    </table>
    <p><small>Impostazioni di default. Per le pagine collezioni e prodotti, nel caso siano state definite specifici valori META queste impostazioni saranno sovrascritte.</small></p> 
  </div>
</div>
<style>
.content-textarea { min-height:80px;max-height:80px;}
</style>
<script>
$(document).ready(function(){

  $('.filtra-sito').on('change',function(){
    var sito = $(this).val();
    $('.riga').addClass('hide');
    $('.riga_sito_' + sito).removeClass('hide');
  })

  $('.btn-save-setting').on('click',function(){
    var id = $(this).data('id');
    $.post ( 'ajax/settings' ,
      {
        action: 'setting-field-save',
        id: id,
        field: 'ac_content',
        value: $('.textarea_' + id).val()
      }, function ( result ){
        if ( result ){
          doNotification('META','Valore salvato correttamente');
        } else {
          doNotification('META','Errore nel salvataggio dati');
        }
      }
    )
  })

});
</script>
