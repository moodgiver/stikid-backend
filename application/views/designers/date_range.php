<?php
  
  $periodo_corrente = substr($from,6,2).'-'.substr($from,4,2).'-'.substr($from,0,4).' -  '.substr($to,6,2).'-'.substr($to,4,2).'-'.substr($to,0,4);
 ?>
  <div class="col-lg-5">
    <label>Periodo</label>
    <div class="input-group">
      <div class="input-group-addon">
        <i class="fa fa-calendar"></i>
      </div>
      <input type="text" class="form-control pull-right active" name="daterange" id="daterange" value="<?=$periodo_corrente?>">
    </div>
  </div>


<script>
$(document).ready(function(){
  $('#daterange').daterangepicker({
    "locale" : "it"
  });


});

</script>
