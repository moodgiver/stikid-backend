<div class="col-lg-4">
<?php
if ( $_SESSION['user']['role'] == 'admin' ){
  echo '<label>Designer</label>
  <select class="form-control select-designer">
    <option value="">seleziona...</option>';
      if ( !isset($_POST['id']) ){
        $id = 0;
      } else {
        $id = $_POST['id'];
      }
      foreach ( $designers AS $row ){
        $selected = '';
        if ( $row['id_designer'] == $id ){
          $selected = 'selected';
        }
        echo '<option value="'.$row['id_designer'].'" '.$selected.'>'.$row['ac_cognome_designer'].' '.$row['ac_nome_designer'].'</option>';
      }
  echo '
  </select>';
} else {
  echo '<input type="hidden" class="select-designer" value="'.$_SESSION['user']['customer_id'].'">';
}
?>
</div>

<?php
  include_once('date_range.php');
?>


<div class="col-lg-2">
<br>

  <label>&nbsp;</label>
  <button class="btn btn-primary btn-vendite-designer" data-mode="view">Cerca</button>

</div>

<div class="clearfix"></div>
<div class="row vendite"></div>
<script>
$(document).ready(function(){

  $('.btn-vendite-designer').on('click',function(){
    var from = $('#daterange').data('daterangepicker').startDate.format('YYYYMMDD')
    var to = $('#daterange').data('daterangepicker').endDate.format('YYYYMMDD')
    $.post ( 'ajax/designers',
      {
        action: 'designers-vendite-result',
        id: $('.select-designer').val(),
        from: from,
        to: to
      },function(result){
        $('.vendite').html ( result );
      }

      )
  /*  } else {
      $.post ( 'ajax/designers',
      {
        action: 'designers-vendite-export',
        id: $('.select-designer').val(),
        from: picker.startDate.format('YYYYMMDD'),
        to: picker.endDate.format('YYYYMMDD')
      },function(result){
        //$('.vendite').html ( result );
      }
      )
    }*/
  })



})
</script>
