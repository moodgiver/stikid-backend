<div class="col-lg-8">
<select class="form-control select-designer">
  <option value="">seleziona...</option>
  <?php
    if ( !isset($_POST['id']) ){
      $id = 0;
    } else {
      $id = $_POST['id'];
    }
    foreach ( $designers AS $row ){
      $selected = '';
      if ( $row['id_designer'] == $id ){
        $selected = 'selected';
      }
      echo '<option value="'.$row['id_designer'].'" '.$selected.'>'.$row['ac_cognome_designer'].' '.$row['ac_nome_designer'].'</option>';
    }
   ?>
</select>
</div>
<div class="col-lg-4">
<button class="btn btn-sm btn-primary btn-new-record" data-form="new-record-form">Aggiungi</button>
</div>
<div class="clearfix"></div>
<script>
$(document).ready(function(){
  $('.select-designer').on('change',function(){
    if ($(this).val() != ''){
      $.post ( 'ajax/designers' ,
        {
          action: 'designers-profilo',
          id: $(this).val()
        }, function ( result ){
          $('.content').html(result);
        }
      )
    }
  })
})
</script>
