<div class="row" style="padding:5px">
<div class="col-lg-12">
    <table class="table table-bordered table-striped" id="designers_vendite">
        <thead>
          <tr>
        <th>Codice</th>
        <th>Prodotto</th>
		    <th>Q.tà</th>
		    <th>Credito EUR</th>
		    <th>Credito Max</th>
		    <th>Credito Max Residuo</th>
      </tr>
        </thead>
        <tbody>

        <?php
        if ( count($vendite) > 0 ){
        $totale_designer = 0;
        $totale_generale = 0;
        $myVal = 0;
        $myValMax = $vendite[0]['ac_value_max'];
        $id_designer = 0;
        foreach ( $vendite AS $qry ){
          if ( $id_designer != $qry['id_designer'] ){
          echo '
          <tr>
          <td colspan="9" style="background:#a9a9a9;color:##fff">
          Designer: <strong>'.$qry['ac_nome_designer'].' '. $qry  ['ac_cognome_designer'].'</strong>
          &raquo; Credito x Prodotto: '.$myVal.' EUR / Credito Max  = '.$myValMax.' EUR
          </td>
          </tr>';
          }
          $id_designer = $qry['id_designer'];
          $totale_prodotto = 0;
          $totale_designer = $totale_designer + $qry['pagare_ora'];
          $totale_generale = $totale_generale + $qry['pagare_ora'];
          echo '
          <tr id="intestazione_#n#">
           <td>
           '.$qry['ac_codice_prodotto'].'
           </td>
           <td>
           '.$qry['ac_prodotto'].'
           &nbsp;
           </td>
           <td>
           '.$qry['venduti_2016'].'
           </td>
           <td class="text-right">
           '.number_format($qry['pagare_ora'],2).'
           </td>
           <td>
           '.number_format($qry['ac_value_max'],2).'
           </td>
           <td>
           '.number_format($qry['residuo'],2).'
           </td>
           </tr>';
         }
         if ( $_SESSION['user']['role'] == 'admin' ){
           echo '
           <tr style="background:#ff8448;">
            <td colspan="3" align="right">Totale</td>
            <td align="right">
            <strong>'.number_format($totale_designer,2).'</strong>
           </td>
           <td colspan="2">&nbsp;</td>

           </tr>
           ';
         }
        } 
        ?>
        </tbody>
    </table>
</div>

</div>

<script  src="//cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>

<style>
.dt-button {
  background: #3c8dbc;
    padding: 5px;
    border-radius: 4px;
    color: #fff;
    position: absolute;
    right: 20px;
}
</style>
<script>
$(document).ready(function(){
  $('#designers_vendite').DataTable( {
    dom: 'Bfrtip',
     buttons: [

         'csvHtml5',

     ],
      "paging":   false,
      "ordering": false,
      "searching"  : false,
      "info":     false,
      "columnDefs": [{
      "defaultContent": "-",
      "targets": "_all",
  }]
   } );
})
</script>
