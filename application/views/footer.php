
<footer class="main-footer" id="footer"><!--Footer-->

</footer>
<div class="notification"></div>

</div>
<div class="waitEnd hide" style="width:100vw;height:100vh;position:fixed;top:0;left:0;background:#333;opacity:.8;margin:0 auto;z-index:99999999999999">
<div class="col-lg-12 text-center">
  <h3 style="color:#fff;"><span class="fa fa-spinner fa-spin"></span> Operazione in corso ...</h3>
</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modalCommon">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title modal-title-common"></h4>
      </div>
      <div class="modal-body modal-body-common">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>
        <button type="button" class="btn btn-primary btn-modal-save" data-action="" data-id="">Salva </button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php
  $app = $this->config->item('application');
  $map = directory_map($app['temp_image_path']);
  $temp_URI = $app['temp_image_url'];
  asort($map);
?>

<div class="modal fade" tabindex="-1" role="dialog" id="modalDropzone">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Carica immagine></h4>
      </div>
      <div class="modal-body modal-body-dropzone">
        <small>Trascina o clicca dentro l'area sottostante per caricare i file. Solo i file utilizzati realmente saranno salvati. Gli altri file rimarranno in una cartella temporanea.</small>
        <div class="jumbotron" style="min-height:100vh;margin:0 auto;">
          <form action="<?php echo base_url();?>upload/temp" id="imageDropzone" class="dropzone">
            <input type="hidden" name="imagePreview" id="imagePreview" value="" data-uri="public/images/temp/">
            <input type="hidden" name="folder" id="folder" value="<?=$app['temp_image_path']?>">
            <input type="hidden" name="fieldData" id="fieldData" value="">
            <input type="hidden" name="fileUploaded" id="fileUploaded" value="">
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>
          <button type="button" class="btn btn-primary btn-modal-save" data-action="" data-id="">Salva </button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
</div>

<input type="hidden" id="temp_image_uri" value="<?=$temp_URI?>">
<!-- AdminLTE App -->
<script src="public/dist/js/app.min.js"></script>
<script src="public/plugins/datatables/jquery.dataTables.min.js"></script>
<!-- Include Date Range Picker -->
<script src="public/dist/js/dropzone.js"></script>
<script type="text/javascript" src="public/plugins/daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="public/plugins/daterangepicker/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="public/plugins/daterangepicker/daterangepicker-bs3.css" />
<link rel="stylesheet" href="public/plugins/bootstrap-wysihtml5/dist/bootstrap3-wysihtml5.min.css">
<script type="text/javascript" src="public/plugins/bootstrap-wysihtml5/dist/bootstrap3-wysihtml5.all.min.js"></script>
<!--<script src="//cdn.ckeditor.com/4.5.8/standard/ckeditor.js"></script>-->
<script src="public/js/functions.js"></script>
<script src="public/dist/js/shopper.js"></script>-
<!--<script src="public/dist/js/ordini.js"></script>
<script src="public/dist/js/update.js"></script><br>-->
<!--<?php
  print_r ( $_SESSION );
 ?>-->
<!--<?php
  echo $_SERVER['SERVER_ADDR'];
  $this->config->set_item('site','STIKID');
  foreach ( $this->config->item('settings') AS $setting ){
    echo $setting.' => ';
    print_r($this->config->item($setting));
    echo '<br>';
  }
 ?>-->
</body>
</html>
