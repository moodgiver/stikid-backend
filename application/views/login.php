<?php
  include_once('header.php');
  echo '
  <div class="row" style="padding:0px!important;">
  </div>
  ';

?>
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog"  style="background:rgba(126,126,126,1);" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document" style="width:30vw;">
    <div class="modal-content">
      <div class="modal-header" style="background:#96bf0d">
        <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
        <h4 class="modal-title">STIKID/STICASA Manager</h4>
      </div>
      <div class="modal-body">
        <form action="<?=base_url()?>signin" method="post">
        <div class="signup-form"><!--sign up form-->
          <div class="form-group">
            Utente<br>
            <div class="input-group">
            <input type="text" name="username_login" class="form-control" placeholder="nome utente" value="<?php echo set_value('username_login'); ?>" />
            <div class="input-group-addon email-cart-input"><span class="fa fa-user"></span></div>
            </div>
            <br>

            Password<br>
            <div class="input-group">
              <input type="password" name="password_login" class="form-control"/>
              <div class="input-group-addon"><span class="fa fa-lock"></span></div>
            </div>

          </div>
        </div><!--/sign up form-->


        <h5 class="msg-danger msg_register_cart"><?php echo form_error('email'); ?></h5>
        <p>Password dimenticata? <a href="password/reset">Richiedi una nuova password</a></p>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn get">Accedi</button>


      </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php
  include_once('footer.php');
?>

<script>
$(document).ready(function(){
  $('#loginModal').modal('show');
})
</script>
