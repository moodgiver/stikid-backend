<div class="row">

<?php
echo '<div class="col-lg-12" style="margin-bottom:20px;">
<button class="btn btn-sm btn-primary pull-right btn-new-record" data-form="new-record-form">Aggiungi</button><br><br>';

//create a new record empty form
//@schema : database schema
//@name: schema name
//true/false: multirow/single record
//@action: ajax action to save record
_create_empty_form_record ( $schema , $name , true,  $action );

if ( $slides ){

  //for each slide create an update form
  $n = 0;
  $row = $slides[0];
  foreach ( $slides AS $row ){
    _create_form_record ( $schema , $name , $row , true , $n , $action );
    $n++;
  }

}

echo '</div>';
?>
</div>
