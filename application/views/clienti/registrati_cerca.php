<div class="row">
  <div class="col-lg-5">
    <label>Cerca cliente/email</label>
    <input type="text" class="form-control registrato-search" placeholder="inserire il cognome o l'indirizzo email" value="<?=$_POST['search']?>">
    <div class="cliente-search-result"></div>
  </div>
  <div class="col-lg-7 text-right">
    <br>
    <button class="btn btn-sm btn-primary push-right" id="export_to_xls" data-title="clienti-ordini"  data-table="table-clienti">Excel</button>
  </div>
</div>

<script>
$(document).ready(function(){

  $('.registrato-search').on('keyup',function(){
    if ($(this).val().length > 3){
      $.post ( 'ajax/clienti' ,
        {
          action: 'registrato-cerca',
          search: $(this).val()
        }, function(result){
          $('.table-content').html(result);
        }
      )
    }
  })

})
</script>
