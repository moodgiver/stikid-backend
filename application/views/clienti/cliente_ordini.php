<thead>

  <tr role="row">

  <th>
  Ordine
  </th>
  <th>
  Data/ora
  </th>
  <th class="text-right">
  Totale Ordine &euro;
  </th>

  </tr>
</thead>
<?php
  $totale = 0;
  foreach ( $clienti AS $qry ){
    echo '
    <tr>

    <td>
      <strong>'.$qry['ac_nrordine'].'</strong><br>
    </td>
    <td>
      '.$qry['dt_pagamento'].'
    </td>
    <td align="right">
      '.number_format($qry['ac_totale'],2).'
    </td>
    </tr>
    ';
    $totale += (float)$qry['ac_totale'];
  }
 ?>
 <tr>
   <td colspan="3" align="right">
     <h4>Totale &euro; <?=number_format($totale,2)?></h4>
   </td>
 </tr>
