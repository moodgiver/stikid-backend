<?php
  $cl = $cliente[0];
  $selected = '';
  $stato = ['Non attivo','Attivo'];

?>
<div class="row">
<div class="col-lg-12">
  <form action="ajax/clienti" method="post" id="cliente-form">
    <input type="hidden" name="action" value="cliente-save">
    <input type="hidden" name="id" value="<?=$cl['idregistrazione']?>">
    <div class="col-lg-6">
      <label>Nome</label>
      <input class="form-control nome" name="nome" value="<?=$cl['ac_nome']?>">
    </div>
    <div class="col-lg-6">
      <label>Cognome</label>
      <input class="form-control cognome" name="cognome" value="<?=$cl['ac_cognome']?>">
  </div>
  <div class="col-lg-6">
    <label>Email</label>
    <input class="form-control email" name="email" value="<?=$cl['ac_email']?>">
  </div>
  <div class="col-lg-6">
    <label>Registrato</label>
    <input class="form-control registrato" value="<?=$cl['dt_data_registrazione']?>" readonly>
  </div>
  <div class="col-lg-6">
    <label>Indirizzo</label>
    <input class="form-control indirizzo" name="indirizzo" value="<?=$cl['ac_indirizzo']?>">
  </div>
  <div class="col-lg-6">
    <label>Comune</label>
    <input class="form-control citta" name="citta" value="<?=$cl['ac_citta']?>">
  </div>
  <div class="col-lg-2">
    <label>Prov.</label>
    <input class="form-control pv" name="pv" value="<?=$cl['ac_pv']?>">
  </div>
  <div class="col-lg-2">
    <label>CAP</label>
    <input class="form-control cap" name="cap" value="<?=$cl['ac_cap']?>">
  </div>
  <div class="col-lg-4">
    <label>Nazione</label>
    <input class="form-control nazione" name="nazione" value="<?=$cl['ac_nazione']?>">
  </div>
  <div class="clearfix"></div>
  <div class="col-lg-6">
    <label>C.F.</label>
    <input class="form-control cf" name="cf" value="<?=$cl['ac_cf']?>">
  </div>
  <div class="col-lg-6">
    <label>P.IVA</label>
    <input class="form-control piva" name="piva" value="<?=$cl['ac_piva']?>">
  </div>
  <div class="col-lg-6">
    <label>Password <i class="fa fa-eye btn-view-password" data-pwd="<?=$cl['ac_pwd']?>"></i></label>
    <input class="form-control pw" name="pw" value="" placeholder="clicca sul pulsante genera per creare una nuova password">
  </div>

  <div class="col-lg-3">
    <label><br></label><br>
    <a href="javascript:void(0)" class="btn btn-primary btn-sm btn-genera-password">Genera Password</a>
  </div>
  <div class="col-lg-3">
    <label>Stato</label>
    <select class="form-control attivo" name="attivo">
    <?php
      $n = 0;
      foreach ( $stato AS $st ){
        $selected = '';
        if ( $cl['bl_attivo'] == $n ){
          $selected = 'selected';
        }
        echo '<option value="'.$n.'" '.$selected.'>'.$st.'</option>';
        $n++;
      }
     ?>
  </div>
  </form>
</div>
</div>

<script>
$(document).ready(function(){
  $('.btn-genera-password').on('click',function(){
    $.post ( 'ajax/clienti' ,
      {
        action: 'cliente-genera-password'
      }, function ( result ){
        $('.pw').val(result);
      }
    )
  })

  $('.btn-view-password').on('click',function(){
    alert ( $(this).data('pwd') );
  })
})
</script>
