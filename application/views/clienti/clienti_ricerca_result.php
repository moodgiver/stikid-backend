
  <div class="col-lg-12">
    <table class="table table-striped table-bordered dataTable col-sm-10 whiteBG" role="grid" aria-describedby="example1_info" id="table-clienti">
		<thead>

      <tr role="row">
			<th class="sorting"aria-controls="example1"aria-label="Rendering engine: activate to sort column ascending">
			Cliente
			</th>
      <th>
        Data
      </th>
      <th>
        Sconto
      </th>
      <th>
        Coupon
      </th>
      <th>
        Spedizione
      </th>
      <th>
        Totale
      </th>
			</tr>
		</thead>
		<tbody>
      <?php
      $id = 0;
      $n = 0;
      $totale = 0;
      $ordini = $clienti;
      foreach ( $clienti AS $qry ){
        if ( $id != $qry['idregistrazione'] ){

        echo '
			    <tr class="cliente-vedi pointer" data-id="'.$qry['idregistrazione'].'">
				    <td colspan="6">
                <strong>'.strtoupper($qry['ac_cognome']).' '.strtoupper($qry['ac_nome']).'</strong> -
                '.strtoupper($qry['ac_indirizzo']).' -
                '.strtoupper($qry['ac_cap']).' '.strtoupper($qry['ac_citta']).'  '.strtoupper($qry['ac_pv']).'<br>
                <span class="fa fa-phone"></span> '.$qry['ac_telefono'].' - '.$qry['ac_cellulare'].'<br>
                <span class="fa fa-envelope"></span> '.$qry['ac_email'].'
				    </td>';
            $id = $qry['idregistrazione'];
            foreach ( $ordini AS $ordine ){

              if ( $id == $ordine['idregistrazione'] ){
                  $spedizione = $ordine['ac_spesespedizione'];
                  $sconto = $ordine['ac_sconto'];
                  if ( $ordine['ac_spesespedizione'] == '' ){
                    $spedizione = 0;
                  }
                  if ( $sconto == '' ){
                    $sconto = 0;
                  }
                  echo '
                  <tr>

                  <td>
                    <strong>'.$ordine['ac_nrordine'].'</strong><br>
                    <label class="label label-default">'.$ordine['ac_pagamento'].'</label>
                  </td>
                  <td>
                    '.$ordine['dt_pagamento'].'
                  </td>
                  <td align="right">
                    '.number_format($sconto,2).'
                  </td>
                  <td align="right">
                    '.$ordine['ac_coupon'].'
                  </td>
                  <td align="right">

                    '.number_format($spedizione,2).'
                  </td>
                  <td align="right">
                    '.number_format($ordine['ac_totale'],2).'
                  </td>
                  </tr>
                  ';
                  $totale += (float)$ordine['ac_totale'];
              }
            }
            echo '
            <tr>
              <td align="right" colspan="5"><h4>Totale EUR</h4></td>
              <td class="text-right"><h4>'.number_format($totale,2).'</h4></td>
            </tr>
            ';
            $totale = 0;
            /*
          </tr>
          <tr class="riga riga_'.$qry['idregistrazione'].'">
            <td colspan="4">
              <table class="table table-bordered table-striped" id="table_'.$qry['idregistrazione'].'">
              </table>
            </td>
          </tr>';*/
        }
        $id = $qry['idregistrazione'];
        $n++;
    }
    ?>
		</tbody>
	</table>
  </div>

<script>
$(document).ready(function(){

  $('.cliente-vedi').on('click',function(){
    var id =  $(this).data('id');
    $.post ( 'ajax/clienti' ,
      {
        action: 'cliente-vedi',
        id: id
      }, function ( result ){
        $('.modal-title-common').html('Modifica Cliente');
        $('.modal-body-common').html(result);
        $('.btn-modal-save').attr('data-action','cliente-save');
        $('.btn-modal-save').attr('data-id',id);
        $('#modalCommon').modal('show');
      }
    )
  })
})
</script>
