<?php
  include_once('date_range.php');
  include_once('registrati_cerca.php');
?>

<div class="row table-content">
  <div class="col-lg-12">

  </div>
  <div class="col-lg-12">
    <table class="table table-striped table-bordered dataTable col-sm-10 whiteBG" role="grid" aria-describedby="example1_info" id="table-clienti">
		<thead>

			<tr role="row">
			<th class="sorting"aria-controls="example1"aria-label="Rendering engine: activate to sort column ascending">
			Cliente
			</th>
      <th>
        Registrato il
      </th>

			</tr>
		</thead>
		<tbody>
      <?php
      foreach ( $clienti AS $qry ){
        echo '
			    <tr class="cliente-vedi pointer" data-id="'.$qry['idregistrazione'].'">
				    <td>
                <span class="fa fa-envelope"></span> '.$qry['ac_email'].'
				    </td>
            <td>
              '.$qry['registrazione'].'
            </td>
          </tr>
        ';
      }
      /*
      <strong>'.strtoupper($qry['ac_cognome']).' '.strtoupper($qry['ac_nome']).'</strong> -
      '.strtoupper($qry['ac_indirizzo']).' -
      '.strtoupper($qry['ac_cap']).' '.strtoupper($qry['ac_citta']).'  '.strtoupper($qry['ac_pv']).'<br>
      <span class="fa fa-phone"></span> '.$qry['ac_telefono'].' - '.$qry['ac_cellulare'].'<br>
      */
      ?>

    </tbody>
  </table>
</div>
