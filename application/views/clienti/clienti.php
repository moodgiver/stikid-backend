<?php
  include_once('date_range.php');
  include_once('clienti_cerca.php');
?>

<div class="row table-content">
  <div class="col-lg-12">

  </div>
  <div class="col-lg-12">
    <table class="table table-striped table-bordered dataTable col-sm-10 whiteBG" role="grid" aria-describedby="example1_info" id="table-clienti">
		<thead>

			<tr role="row">
			<th class="sorting"aria-controls="example1"aria-label="Rendering engine: activate to sort column ascending">
			Cliente
			</th>
      <th>
        Data
      </th>
      <th>
        Sconto
      </th>
      <th>
        Coupon
      </th>
      <th>
        Spedizione
      </th>
      <th>
        Totale
      </th>
			</tr>
		</thead>
		<tbody>
      <?php
      $id = 0;
      $n = 0;
      $totale = 0;
      $ordini = $clienti;
      foreach ( $clienti AS $qry ){
        if ( $id != $qry['idregistrazione'] ){

        echo '
			    <tr class="cliente-vedi pointer" data-id="'.$qry['idregistrazione'].'">
				    <td colspan="6">
                <strong>'.strtoupper($qry['ac_cognome']).' '.strtoupper($qry['ac_nome']).'</strong> -
                '.strtoupper($qry['ac_indirizzo']).' -
                '.strtoupper($qry['ac_cap']).' '.strtoupper($qry['ac_citta']).'  '.strtoupper($qry['ac_pv']).'<br>
                <span class="fa fa-phone"></span> '.$qry['ac_telefono'].' - '.$qry['ac_cellulare'].'<br>
                <span class="fa fa-envelope"></span> '.$qry['ac_email'].'
				    </td>

          </tr>
          ';

          $id = $qry['idregistrazione'];
          foreach ( $ordini AS $ordine ){

            if ( $id == $ordine['idregistrazione'] ){
              $spedizione = $ordine['ac_spesespedizione'];
              $sconto = $ordine['ac_sconto'];
              if ( $ordine['ac_spesespedizione'] == '' ){
                $spedizione = 0;
              }
              if ( $sconto == '' ){
                $sconto = 0;
              }
                echo '
                <tr>

                <td>
                  <strong>'.$ordine['ac_nrordine'].'</strong><br>
                  <label class="label label-default">'.$ordine['ac_pagamento'].'</label>
                </td>
                <td>
                  '.$ordine['dt_pagamento'].'
                </td>
                <td align="right">
                  '.number_format($sconto,2).'
                </td>
                <td align="right">
                  '.$ordine['ac_coupon'].'
                </td>
                <td align="right">
                  '.number_format($spedizione,2).'
                </td>
                <td align="right">
                  '.number_format($ordine['ac_totale'],2).'
                </td>
                </tr>
                ';
                $totale += (float)$ordine['ac_totale'];
            }
          }
          echo '
          <tr>
            <td align="right" colspan="5"><h4>Totale EUR</h4></td>
            <td class="text-right"><h4>'.number_format($totale,2).'</h4></td>
          </tr>
          ';
          $totale = 0;

        }
        $id = $qry['idregistrazione'];
        $n++;
    }
    ?>
		</tbody>
	</table>
  </div>
</div>
<script>
$(document).ready(function(){

  /*

  $('.cliente-vedi').on('click',function(){
    var id =  $(this).data('id');
    $.post ( 'ajax/clienti' ,
      {
        action: 'cliente-vedi',
        id: id
      }, function ( result ){
        $('.modal-title-common').html('Modifica Cliente');
        $('.modal-body-common').html(result);
        $('.btn-modal-save').attr('data-route','form-submit');
        $('.btn-modal-save').attr('data-url','ajax/clienti');
        $('.btn-modal-save').attr('data-form','cliente-form');
        $('.btn-modal-save').attr('data-action','cliente-save');
        $('.btn-modal-save').attr('data-id',id);
        $('#modalCommon').modal('show');
      }
    )
  })


  $("#export_to_xls").click(function(e) {
    e.preventDefault();

    //getting data from our table
    var data_type = 'data:application/vnd.ms-excel';
    var table_div = document.getElementById('table-clienti');
    //var table_div = $(this).data('table');
    var table_html = table_div.outerHTML.replace(/ /g, '%20');

    var a = document.createElement('a');
    a.href = data_type + ', ' + table_html;
    a.download = $(this).data('title') + '.xls';
    a.click();
  });

  */
})
</script>
