
<div class="row">
<div class="col-lg-5">
  <div class="col-lg-10">
  <select class="form-control collezione-edit" data-action="categoria-modifica">
    <option value="">Seleziona una categoria ...</option>
    <?php
      $siti = $this->config->item('siti');
      $sito = '';
      if ( !isset($_POST['id']) ){
        $_POST['id'] = 0;
      }
      foreach ( $collezioni AS $co ){
        $selected = '';
        if ( $co['id_categoria'] == $_POST['id'] ){
          $selected = 'selected';
        }
        foreach ( $siti AS $s ){
          if ( $co['id_famiglia'] == $s['sito'] ){
            $sito = $s['domain'];
          }
        }
        $cat = $co['ac_categoria'];
        if ( $co['ac_categoria_lang'] != '' ){
          $cat = $co['ac_categoria_lang'];
        }
        echo '<option value="'.$co['id_categoria'].'" '.$selected.'>'.$sito.' => '.$cat.'</option>';
      }

      if ( !isset($collezione) ){
        $collezione[0] = array (
            'id_categoria'      => 0,
            'id_famiglia'       => 4,
            'ac_categoria'      => '',
            'ac_categoria_lang' => '',
            'ac_image'          => '',
            'ac_link'           => '',
            'ac_meta_title'     => '',
            'ac_meta_keys'      => '',
            'ac_meta_description' => '',
            'bl_attivo'         => 0
        );
        $cat = '';
      } else {
        $cat = $collezione[0]['ac_categoria'];
        if ( $collezione[0]['ac_categoria_lang'] != '' ){
          $cat = $collezione[0]['ac_categoria_lang'];
        }
      }
      $coll = $collezione;

    ?>
  </select>
  </div>
  <div class="col-lg-2">
    <button class="btn btn-primary btn-new-collezione"><span class="fa fa-plus"></span></button>
  </div>
  <div class="clearfix"></div>
  <br>
  <input type="hidden" name="id" class="id_categoria" value="<?=$coll[0]['id_categoria']?>">
  <label>Collezione</label>
  <input type="text" name="collezione" class="form-control collezione" value="<?=$cat?>">
  <br>
  <label>URL</label>
  <input type="text" name="url" class="form-control url" value="<?=$coll[0]['ac_link']?>">
  <br>
  <label>Sito</label>
  <select name="sito" class="form-control sito">
    <option value="">Seleziona</option>
    <?php

      foreach ($siti AS $sito){
        $selected = '';
        if ( $coll[0]['id_famiglia'] == $sito['sito'] ){
          $selected = 'selected';
        }
        echo '<option value="'.$sito['sito'].'" '.$selected.'>'.$sito['domain'].'</option>';
      }
     ?>
  </select>
  <br>
  <label>Ordine</label>
  <select class="form-control int_ordine">
    <?php
    for ( $n=0 ; $n<25 ; $n++ ){
      $selected = '';
      if ( $coll[0]['int_ordine'] == $n ){
        $selected = 'selected';
      }
      echo '<option value="'.$n.'" '.$selected.'>'.$n.'</option>';
    }
    ?>
  </select>
  <br>
  <label>Stato</label>
  <select class="form-control bl_attivo">
    <?php
    if ( $coll[0]['bl_attivo'] ){
      echo '<option value="0">Non attiva</option>';
      echo '<option value="1" selected>Attiva</option>';
    } else {
      echo '<option value="0" selected>Non attiva</option>';
      echo '<option value="1">Attiva</option>';
    }
     ?>
  </select>
  <br>
  <label>Immagine</label><br>
  <?php
    $app = $this->config->item('application');
    $target_field = 'ac_image';
    $target_preview = 'image';
    $target_folder = $app['collezione_image_path'];
    $target_uri = '//127.0.0.1/manager_stikid/public/img/';
    $mode = 'collezione';
    $img = 'public/img/noimage.png';
    $id = $coll[0]['id_categoria'];
    if ( $coll[0]['ac_image'] != '' ){
      $img = $coll[0]['ac_image'];
      $img = "//static.stikid.com/images/collezioni/".$img;
    }
  ?>

  <img class="image" src="<?=$img?>" style="width:50%;height:auto"/>
  <input type="hidden" class="ac_image" value="<?=$coll[0]['ac_image']?>">
</div>
<div class="col-lg-1"></div>
<div class="col-lg-6">
  <label>META Title</label>
  <br>
  <textarea style="width:100%;min-height:60px;" class="meta_title"><?=$coll[0]['ac_meta_title']?></textarea>
  <br>
  <label>META Description</label>
  <br>
  <textarea style="width:100%;min-height:120px;" class="meta_description"><?=$coll[0]['ac_meta_description']?></textarea>
  <br>
  <label>META Keyword</label>
  <br>
  <textarea class="meta_keys"  style="width:100%;min-height:60px;"><?=$coll[0]['ac_meta_keys']?></textarea>
  <br>
  <div class="col-lg-12 text-right">
    <button class="btn btn-success btn-save-collezione push-right">Salva</button>
  </div>

</div>
</div>

<?php
  include_once('dropzone.php');
 ?>

<script>
$(document).ready ( function(){

  //$("div#dropzone").dropzone({ url: "upload/collezione" });

  $('.btn-new-collezione').on('click',function(){
    $.post ( 'ajax/negozio' ,
      { action: 'negozio-collezioni' },function(result){
        $('.content').html ( result );
      }
    )
  })

  $('.collezione').on('change',function(){
    var url_type = '-adesivi-murali';
    if ( $('.sito').val() == '1' ){
      url_type = '-wall-stickers';
    }
    if ( $('.sito').val() == '8' ){
      url_type = '-quadri-per-bambini';
    }
    $('.url').val ( $(this).val().replace(/[^a-zA-Z0-9]/g,'-').toLowerCase() + url_type);
  })

  $('.sito').on('change',function(){
    var url_type = '-adesivi-murali';
    if ( $(this).val() == '1' ){
      url_type = '-wall-stickers';
    }
    if ( $(this).val() == '8' ){
      url_type = '-quadri-per-bambini';
    }
    $('.url').val ( $('.collezione').val().replace(/[^a-zA-Z0-9]/g,'-').toLowerCase() + url_type);
  })

  $('.collezione-edit').on('change',function(){
    var id = $(this).val();
    if ( id != 0 ){
      $.post ( 'ajax/negozio' ,
      {
        action: $(this).data('action'),
        id: id
      }, function( result ){
        $('.content').html ( result );
      }
      )
    }
  })

  $('.image').on('click',function(){
    $('#dropzoneModal').modal('show');
  })

  $('.btn-save-collezione').on('click',function(){
    $.post ( 'prodotti/save' ,
      {
        action: 'collezione',
        id: $('.id_categoria').val(),
        collezione: $('.collezione').val(),
        sito: $('.sito').val(),
        image: $('.ac_image').val(),
        link: $('.url').val(),
        meta_title: $('.meta_title').val(),
        meta_description: $('.meta_description').val(),
        meta_keys: $('.meta_keys').val(),
        attiva: $('.bl_attivo').val()
      }, function(result){
        doNotification('Collezioni','Collezione salvata');
        $.post ( 'ajax/negozio' ,
          {
            action: 'categoria-modifica',
            id: $('.id_categoria').val()
          }, function ( result ){
            $('.content').html(result);
          }
        )
      }
    )
  })


})
</script>
