<h5>Risultato ricerca</h5>
  <?php
  if ( !isset($_POST['extended']) ){
    echo '
    <select class="form-control new-upsell-product">';
    foreach ( $prodotti_search AS $row ){
      echo '<option value="'.$row['id_prodotto'].'">'.$row['ac_prodotto'].'</option>';
    }
    echo '</select>';
  }
  foreach ( $prodotti_search AS $row ){
    $border = 'border:2px solid green;';
    if ( $row['bl_stato'] != 1 ) {
      $border = 'border:2px solid red';
    }
    echo '<div class="col-lg-2 pointer edit-product" data-id="'.$row['id_prodotto'].'" data-categoria="'.$row['id_categoria'].'" style="margin-bottom:5px;"><img src="'.$row['uri_img'].'" style="max-width:100%;height:auto;'.$border.'" title="'.$row['ac_prodotto'].'"/><h5 style="min-height:45px;height:45px;max-height:45px">'.$row['ac_prodotto'].'</h5></div>';
  }
  ?>
  <br><br><br>
  <div class="data-prodotto">
  </div>

<script>
$(document).ready(function(){
  $('.new-upsell-product').on('click',function(){
    $.post ( 'ajax/negozio' ,
      {
        action: 'negozio-prodotto-dati',
        id_prodotto: $(this).val(),
        sito: 4,
      },function ( result ){
        $('.data-prodotto').html(result);
      }
    )
  })

  $('.edit-product').on('click',function(){
    $.post ( 'ajax/negozio' ,
      {
        action: 'negozio-prodotto-scheda',
        id_prodotto: $(this).data('id'),
        id: $(this).data('categoria'),
        sito: 4
        
      }, function ( result ){
        $('.data-prodotto').html(result);
        $('.origine').val('cerca');
        $('#prodottoModal').modal('show');
      }
    )
  })
})
</script>
