<p>Il simulatore usa di default l'immagine sticker. Se vuoi personalizzare il simulatore per questo prodotto carica ogni immagine separata indicando le misure minime reali e dimensioni dell'immagine (es 20cm x 10cm e 200x100px). Ricordati che la proporzione deve essere la stessa.</p>
<p><small>Seleziona le immagini da quelle disponibili</small></p>
<div class="col-lg-12" style="background:#fff;min-height:200px;height:200px;">
  <h3>Immagini Simulatore</h3>
  <div class="gallery-simulatore">
    <?php
      $si = 0;
      if ( count($simulatore) > 0 ){

        foreach ( $simulatore AS $gal ){
          echo '
          <div class="col-lg-1 img-'.$si.'" style="min-height:60px">
          <img src="'.$gal['ac_immagine'].'" style="width:100%;height:auto;border:1px solid #eaeaea;border-radius:5px;"><div style="position:absolute;right:0px;top:0;width:10px;height:10px;font-size:1.2em;color:#999;"><span class="fa fa-trash btn-remove-simulatore-immagine" data-id="'.$gal['id_simulatore'].'" data-number="'.$si.'"></span></div>
          </div>
          ';
          $si++;
        }
      }
    ?>
    <input type="hidden" class="si" value="<?=$si?>">
  </div>
</div>

<div class="col-lg-12" style="background:#fff;">
  <h4>Immagini disponibili</h4>
  <div class="row immagini-disponibili">
  <?php
    $n = 0;
    foreach ($map AS $file){
      list($width,$height) = getimagesize($app['temp_image_path'].''.$file);
      echo '<div class="col-sm-2 img_'.$n.'" style="margin-bottom:10px;"><img src="'.$temp_URI.''.$file.'" class="pointer image-simulatore-add" data-file="'.$file.'" data-id="'.$n.'" data-target="gallery" data-width="'.$width.'" data-height="'.$height.'" style="border:3px double #999;width:100%;heigh:auto;"/></div>';
      $n++;
    }
  ?>
  </div>
</div>

<div class="modal fade" id="simulatoreModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
		  <div class="col-md-12">
        <br>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="panel panel-primary">
          <div class="panel-heading">Imposta immagine simulatore</div>
          <div class="panel-body" style="overflow:auto;">
            <div class="col-lg-4">
              <img class="simulatore_img" style="width:100%;height:auto;">
            </div>
            <div class="col-lg-8">

                <input type="text" class="id_prodotto" value="<?=$prodotto[0]['id_prodotto']?>">
                <input type="text" class="simulatore_file" value="">
                <label>Dimensioni reali</label><br>
                <div class="col-lg-6">
                  <label>Larghezza cm. </label>
                  <input type="text" class="ac_width_cm form-control">
                </div>
                <div class="col-lg-6">
                  <label>Altezza cm</label>
                  <input type="text" class="ac_height_cm form-control">
                </div>
                <label>Dimensioni immagine</label><br>
                <div class="col-lg-6">
                  <label>Larghezza px </label>
                  <input type="text" class="ac_width_px form-control">
                </div>
                <div class="col-lg-6">
                  <label>Altezza px</label>
                  <input type="text" class="ac_height_px form-control">
                </div>

            </div>
          </div>
          <div class="panel-footer text-center">
            <button class="btn btn-default bnt-salva-immagine-simulatore" data-dismiss="modal">Salva</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function(){

  var prop72dpi = 0.0357142857142857;
  $('.image-simulatore-add').on('click',function(){
    $('#simulatoreModal').modal('show');
    $('.simulatore_img').attr('src',$(this).attr('src'));
    $('.simulatore_file').val($(this).data('file'));
    $('.ac_width_px').val($(this).data('width'));
    $('.ac_height_px').val($(this).data('height'));
    var w_cm = parseInt($(this).data('width'))*prop72dpi;
    var h_cm = parseInt($(this).data('height'))*prop72dpi;
    $('.ac_width_cm').val ( w_cm.toFixed(1) );
    $('.ac_height_cm').val ( h_cm.toFixed(1) );
  })


  $('body').delegate('.btn-remove-simulatore-immagine','click',function(){
    var id = $(this).data('id');
    var n = $(this).data('number');
    $.post ( 'ajax/negozio' ,
      {
        action: 'remove-image-simulatore',
        id : id
      },function(result){
        $('.img-' + n).remove();
      }
    )
  })

  $('.bnt-salva-immagine-simulatore').on('click',function(){
    var id_prodotto = $('.id_prodotto').val();
    var si = $('.si').val();
    $('.si').val(parseInt(si)+1);
    $.post ( 'ajax/negozio' ,
      {
        action: 'add-image-simulatore',
        id_prodotto : $('.id_prodotto').val(),
        ac_immagine : $('.simulatore_file').val(),
        ac_w_cm : $('.ac_width_cm').val(),
        ac_h_cm : $('.ac_height_cm').val(),
        ac_w_px : $('.ac_width_px').val(),
        ac_h_px : $('.ac_height_px').val()
      }, function(result){
        $('.gallery-simulatore').append('<div class="col-lg-1 img-' + si + '" style="border:1px solid #eaeaea;border-radius:5px;"><img src="' + $('.simulatore_img').attr('src') + '" style="width:100%;height:auto"><div style="position:absolute;right:0px;top:0;width:10px;height:10px;font-size:1.2em;color:#999;"><span class="fa fa-trash btn-remove-simulatore-immagine" data-id="' + result + '" data-number="' + si + '"></span></div></div>');
      }
    )
  })

})
</script>
