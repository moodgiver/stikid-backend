<div class="row">
  <div class="col-lg-12">
    <label>Cerca prodotto/codice</label>
    <input type="text" class="form-control prodotto-search" placeholder="inserire il nome o il codice prodotto">
    <div class="prodotto-search-result"></div>
  </div>
</div>
<script>
$(document).ready(function(){

  $('.prodotto-search').on('keyup',function(){
    if ( $(this).val().length > 2 ){
      $('.prodotto-search-result').html('');
      $.post ( 'ajax/negozio' ,
        {
          action: 'negozio-prodotto-search',
          search: $(this).val(),
          extended: true
        }, function ( result ){
          $('.prodotto-search-result').html(result);
        }
      );
    } else {
      $('.prodotto-search-result').html('');
    }
  })
})
</script>
