<?php
  $prod = $prodotto[0];
  $app = $this->config->item('application');
  $map = directory_map($app['temp_image_path']);
  $temp_URI = $app['temp_image_url'];
  asort($map);
?>
<div class="row">
  <div class="col-lg-12">
    <h3><?=$prodotto[0]['ac_prodotto']?> <button class="btn btn-primary btn-torna-ai-prodotti" data-id="<?=$prod['id_categoria']?>" data-action="negozio-prodotti" data-sito="<?=$_POST['sito']?>">Torna ai prodotti</button></h3>
  </div>

  <div class="col-lg-12">

    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation"><a href="#upload" aria-controls="evidenza" role="tab" data-toggle="tab">Carica Immagine</a></li>
        <li role="presentation" class="active"><a href="#evidenza" aria-controls="evidenza" role="tab" data-toggle="tab">Immagine in evidenza</a></li>
        <li role="presentation"><a href="#sticker" aria-controls="sticker" role="tab" data-toggle="tab">Immagine Sticker</a></li>
        <li role="presentation"><a href="#gallery" aria-controls="gallery" role="tab" data-toggle="tab">Gallery</a></li>
        <li role="presentation"><a href="#simulatore" aria-controls="immagini" role="tab" data-toggle="tab">Immagini Simulatore</a></li>
        <li role="presentation"><a href="#vettoriale" aria-controls="immagini" role="tab" data-toggle="tab">Vettoriale</a></li>
        <li role="presentation"><a href="#immagini" aria-controls="immagini" role="tab" data-toggle="tab">Immagini Caricate</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane" id="upload">
            <div class="col-lg-12" style="padding:20px;background:#fff">
                <small>Trascina o clicca dentro l'area sottostante per caricare i file. Solo i file utilizzati realmente saranno salvati. Gli altri file rimarranno in una cartella temporanea.</small>
                <div class="jumbotron" style="min-height:100vh;margin:0 auto;">
                    <form action="<?php echo base_url();?>upload/temp" id="myDropzone" class="dropzone">
                      <input type="hidden" name="folder" id="folder" value="<?=$app['temp_image_path']?>">
                    </form>
                </div>
            </div>
        </div>



        <div role="tabpanel" class="tab-pane active" id="evidenza">
          <div class="col-lg-12" style="background:#fff;">

            <?php
              $target = 'evidenza';
              $img = 'public/img/noimage.png';
              if ( $prod['uri_img'] != '' ){
                $img = $prod['uri_img'];
              }
            ?>
            <div class="col-lg-4">
              <h3>Immagine in evidenza</h3>
              <img class="evidenza" src="<?=$img?>" style="width:100%;height:auto;"><br>
              <input type="text" readonly class="ac_immagine" value="<?=$prod['ac_immagine']?>" style="border:0;background:transparent;"><br><br>
              <button class="btn btn-primary btn-changed-evidenza hide">Annulla</button>
              <button class="btn btn-primary btn-save-evidenza hide">Salva</button>
            </div>
            <div class="col-lg-8">
              <h3>Immagini disponibili</h3>
              <div class="row immagini-disponibili">
              <?php
                $n = 0;
                foreach ($map AS $file){
                  echo '<div class="col-lg-3 img_'.$n.'" style="margin-bottom:10px;"><img src="'.$temp_URI.''.$file.'" class="pointer image-change" data-file="'.$file.'" data-target="evidenza" data-field="ac_immagine" style="border:3px double #999;width:100%;heigh:auto;"/></div>';
                  $n++;
                }
              ?>
              </div>
            </div>
          </div>
        </div>

        <div role="tabpanel" class="tab-pane" id="sticker">
          <div class="col-lg-12" style="background:#fff;">
            <?php
              $target = 'sticker';
              $img = 'public/img/noimage.png';
              if ( $prod['uri_sticker'] != '' ){
                $img = $prod['uri_sticker'];
              }
            ?>
            <div class="col-lg-4">
              <h3>Immagine Sticker</h3>
              <img class="sticker" src="<?=$img?>" style="width:100%;height:auto;"><br>
              <input type="text" readonly class="ac_immage" value="<?=$prod['ac_image']?>" style="border:0;background:transparent;"><br><br>
              <button class="btn btn-primary btn-changed-sticker hide">Annulla</button>
              <button class="btn btn-primary btn-save-sticker hide">Salva</button>
            </div>
            <div class="col-lg-8">
              <h3>Immagini disponibili</h3>
              <div class="row immagini-disponibili">
              <?php
                $n = 0;
                foreach ($map AS $file){
                  echo '<div class="col-lg-3 img_'.$n.'" style="margin-bottom:10px;"><img src="'.$temp_URI.''.$file.'" class="pointer image-change" data-file="'.$file.'" data-target="sticker" data-field="ac_image" style="border:3px double #999;width:100%;heigh:auto;"/></div>';
                  $n++;
                }
              ?>
              </div>
            </div>
          </div>

        </div>
        <div role="tabpanel" class="tab-pane" id="gallery" style="max-height:95%;overflow-y:auto">

            <div class="col-lg-12" style="background:#fff;min-height:200px;height:200px;">
              <h3>Gallery</h3>
              <div class="gallery">
                <?php
                  if ( count($gallery) > 0 ){
                    foreach ( $gallery AS $gal ){
                      echo '
                      <div class="col-lg-3">
                      <img src="//cdn.stikid.com/images/gallery/large/'.$gal['ac_image'].'" style="width:100%;height:auto"><div style="position:absolute;right:0px;top:0;width:10px;height:10px;font-size:1.2em;color:#999;"><span class="fa fa-trash"></span></div>
                      </div>
                      ';
                    }
                  }
                ?>
              </div>
            </div>

            <div class="col-lg-12" style="background:#fff;">
              <h4>Immagini disponibili</h4>
              <div class="row immagini-disponibili">
            <?php
              $n = 0;
              foreach ($map AS $file){
                echo '<div class="col-sm-2 img_'.$n.'" style="margin-bottom:10px;"><img src="'.$temp_URI.''.$file.'" class="pointer image-gallery-add" data-file="'.$file.'" data-id="'.$n.'" data-target="gallery" style="border:3px double #999;width:100%;heigh:auto;"/></div>';
                $n++;
              }
            ?>
          </div>
            </div>
        </div>

        <div role="tabpanel" class="tab-pane" id="simulatore" style="max-height:95%;overflow-y:auto">

            <?php
              include_once('prodotto_simulatore.php');
            ?>


        </div>


        <div role="tabpanel" class="tab-pane" id="vettoriale">
          <div class="col-lg-12" style="background:#fff;">
            <?php
              $target = 'sticker';
              $img = 'public/img/noimage.png';
              if ( $prod['uri_eps'] != '' ){
                $img = $prod['uri_eps'];
              }
            ?>
            <div class="col-lg-4">
              <h3>Vettoriale</h3>
              <img class="vettoriale" src="<?=$img?>" style="width:100%;height:auto;"><br>
              <input type="text" readonly class="ac_sticker" value="<?=$prod['ac_sticker']?>" style="border:0;background:transparent;"><br><br>
              <button class="btn btn-primary btn-changed-vettoriale hide">Annulla</button>
              <button class="btn btn-primary btn-save-vettoriale hide">Salva</button>
            </div>
            <div class="col-lg-8">
              <h3>Immagini disponibili</h3>
              <div class="row immagini-disponibili">
              <?php
                $n = 0;
                foreach ($map AS $file){

                  $path_info = pathinfo($file);
                  if ( $path_info['extension'] == 'eps'){
                    echo '<div class="col-lg-3 img_'.$n.'" style="margin-bottom:10px;">
                    <p class="pointer image-change" src="'.$temp_URI.''.$file.'" data-file="'.$file.'" data-target="vettoriale" data-field="ac_sticker">'.$file.'</p>
                    </div>';
                  }
                  $n++;
                }
              ?>
              </div>
            </div>
          </div>

        </div>

        <div role="tabpanel" class="tab-pane" id="immagini" style="max-height:95%;overflow-y:auto">
            <div class="col-lg-12" style="background:#fff;">
              <h4>Immagini caricate</h4>
              <div class="row immagini-disponibili">
            <?php
              $n = 0;
              foreach ($map AS $file){
                echo '<div class="col-sm-2 img_'.$n.'" style="margin-bottom:10px;"><img src="'.$temp_URI.''.$file.'" class="pointer" data-file="'.$file.'" style="border:3px double #999;width:100%;heigh:auto;"/><div style="position:absolute;right:0px;top:0;width:10px;height:10px;font-size:1.2em;color:#999;"><span class="fa fa-trash delete-image" data-file="'.$file.'" data-indice="'.$n.'"></span></div></div>';
                $n++;
              }
            ?>
          </div>
            </div>
        </div>

    </div>
</div>
</div>
<input type="hidden" id="id_prodotto" value="<?=$prod['id_prodotto']?>">
<input type="hidden" id="evidenza-origine" value="<?=$prod['uri_img']?>">
<input type="hidden" id="evidenza-image" value="<?=$prod['ac_immagine']?>">
<input type="hidden" id="sticker-origine" value="<?=$prod['uri_sticker']?>">
<input type="hidden" id="sticker-image" value="<?=$prod['ac_image']?>">
<input type="hidden" id="vettoriale-origine" value="<?=$prod['uri_eps']?>">
<input type="hidden" id="vettoriale-image" value="<?=$prod['ac_sticker']?>">
<input type="hidden" id="temp_image_uri" value="<?=$temp_URI?>">
<script src="public/js/dropzone.js"></script>
<script src="public/js/custom.dropzone.js"></script>

<script>

$(document).ready(function(){
  $('.delete-image').on('click',function(){
    var pos = $(this).data('indice');
    $.post ( 'ajax/negozio' ,
      {
        action: 'delete-temp-image',
        image: $(this).data('file')
      }, function ( result ){
          $('.img_' + pos ).remove();
      }
    )
  })

  Dropzone.autoDiscover = false;
    $("#myDropzone").dropzone({
    dictDefaultMessage: "Clicca qui o trascina la tua immagine qui per caricarla",
    clickable: true,
    maxFilesize: 20,
		autoProcessQueue: true,
    uploadMultiple: false,
    addRemoveLinks: false,
		createImageThumbnails: true,
		acceptedFiles: 'image/*,application/postscript',
		dictFileTooBig: 'Immagine superiore a 20 MB. Impossibile caricare',
				init: function () {
          myDropzone = this;
					$('.dz-preview').remove();

          this.on("success", function(file) {
            $('.immagini-disponibili').prepend( '<div class="col-lg-2" style="margin-bottom:10px;"><img src="' + $('#temp_image_uri').val() + file['name'] + '" class="pointer" data-file="' + file['name'] + '" style="border:3px double #999;width:100%;heigh:auto;"/><div style="position:absolute;right:0px;top:0;width:10px;height:10px;font-size:1.2em;color:#999;"><span class="fa fa-trash"></span></div></div>' );
            $('.fa-spinner').addClass('hide');
            $('.error-image-scontrino').html('');
					  $('.dz-error-message').html('');
					  $('.dz-preview').remove();
        	  $('#dropzoneModal').modal('hide');
	        });

				  this.on("error" , function ( e , errorMsg ){
            $('.dz-error-message').removeClass('hide');
						$('.dz-error-message').html(errorMsg);
						$('.dz-details').html('');
					});

          this.on("addedfile" , function(){
            console.log ( 'added file ');
            $('.dz-error-message').addClass('hide');
            $('.fa-spinner').removeClass('hide');
          });

          this.on("uploadprogress" , function( file , progress ){
            $('.progress-uploaded').css ( 'width' , parseInt(progress) + '%' );
            $('.percentage').html ( parseInt(progress) + ' %' );
          });
      }
    });
});
</script>

<script>
$(document).ready ( function(){
  var id_prodotto = $('#id_prodotto').val()

  $('.btn-torna-ai-prodotti').on('click',function(){
    $.post ( 'ajax/negozio' ,
      {
        action: $(this).data('action'),
        id: $(this).data('id'),
        sito: $(this).data('sito')
      }, function ( result ){
        $('.content').html ( result );
      }
    )
  })

  $('.image-change').on('click',function(){
    var target = $(this).data('target');
    $('.' + target).attr('src',$(this).attr('src'));
    $('.' + $(this).data('field')).val($(this).data('file'));
    //console.log('field=',$(this).data('field'))
    //console.log('image=',$(this).attr('src'))
    //console.log('target=',target)
    $('#' + target + '-image').val($(this).data('file'))
    $('.btn-changed-' + target).removeClass('hide');
    $('.btn-save-' + target).removeClass('hide');
  })

  $('.btn-changed-evidenza').on('click',function(){
    $('.evidenza').attr('src',$('#evidenza-origine').val());
    $('.ac_immagine').val($('#evidenza-image').val());
    $('.btn-save-evidenza').addClass('hide');
    $(this).addClass('hide');
  })

  $('.btn-changed-sticker').on('click',function(){
    $('.sticker').attr('src',$('#sticker-origine').val());
    $('.ac_image').val($('#sticker-image').val());
    $('.btn-save-sticker').addClass('hide');
    $(this).addClass('hide');
  })

  $('.btn-changed-vettoriale').on('click',function(){
    //$('.vettoriale').attr('src',$('#vettoriale-origine').val());
    $('.ac_sticker').val($('#vettoriale-image').val());
    $('.btn-save-vettoriale').addClass('hide');
    $(this).addClass('hide');
  })

  $('.btn-save-evidenza').on('click',function(){
    save_new_image($('#evidenza-image').val(),'evidenza')
  })

  $('.btn-save-sticker').on('click',function(){
    save_new_image($('#sticker-image').val(),'sticker')
  })

  $('.btn-save-vettoriale').on('click',function(){
    save_new_image($('#vettoriale-image').val(),'vettoriale')
  })

  function save_new_image(image,tipo){
    $.post ( 'ajax/negozio' ,
      {
        action: 'negozio-prodotto-save-image',
        id_prodotto: id_prodotto,
        image: image,
        tipo: tipo
      },function(result){
        doNotification('Vettoriale prodotto','Modifica effettuata con successo')
      }
    )
  }

  $('.image-gallery-add').on('click',function(){
    $('.gallery').append(
      '<div class="col-lg-2" style="margin-bottom:10px;"><img src="' + $('#temp_image_uri').val()  + $(this).data('file') + '" class="pointer" style="border:3px double #999;width:100%;heigh:auto;"/><div style="position:absolute;right:0px;top:0;width:10px;height:10px;font-size:1.2em;color:#999;"><span class="fa fa-trash"></span></div></div>');
  })


})
</script>
