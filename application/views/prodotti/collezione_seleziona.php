<div class="col-lg-5">
  <label>Collezione</label>
<select class="form-control collezione-select" data-action="negozio-prodotti">
  <option value="">Seleziona una categoria ...</option>
  <?php
    $siti = $this->config->item('siti');
    $sito = '';
    $selected = '';
    $id = '';
    if ( $_POST['id'] ){
      $id = $_POST['id'];
    }
    foreach ( $collezioni AS $co ){
      $selected = '';
      if ( $id == $co['id_categoria'] ){
        $selected = 'selected';
      }
      foreach ( $siti AS $s ){
        if ( $co['id_famiglia'] == $s['sito'] ){
          $sito = $s['domain'];
        }
      }
      $cat = $co['ac_categoria'];
      if ( $co['ac_categoria_lang'] != '' ){
        $cat = $co['ac_categoria_lang'];
      }

      echo '<option value="'.$co['id_categoria'].'" '.$selected.'>'.$sito.' => '.$cat.'</option>';
    }
    ?>
</select>
</div>
<div class="col-lg-5">
    <input type="hidden" class="sito" value="<?=$_POST['sito']?>">
</div>
<div class="clearfix"></div>
<script>
$(document).ready ( function(){

  //$("div#dropzone").dropzone({ url: "upload/collezione" });

  $('.collezione-select').on('change',function(){
    var id = $(this).val();
    if ( id != 0 ){
      $.post ( 'ajax/negozio' ,
      {
        action: $(this).data('action'),
        id: id,
        sito: $('.sito').val()
      }, function( result ){
        $('.content').html ( result );
      }
      )
    }
  })
})
</script>
