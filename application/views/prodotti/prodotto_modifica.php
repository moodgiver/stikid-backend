<?php
$prod = $prodotto[0];
?>
<div class="modal fade in" tabindex="-1" id="prodottoModal" role="dialog">
  <div class="modal-dialog" style="width:80%">
    <div class="modal-header" style="background:#fff;">
      <label class="label label-primary" style="font-size:1.4em;"><?=$prod['ac_prodotto']?></label>
      da EUR <span class="badge bg-green" style="font-size:1.2em"><?=calcolo_prezzo($prod['ac_W_min'],$prod['ac_H_min'],1)?></span>
      <?php
        if ( (float)$prod['ac_coefficiente_sconto'] < 1 ){
          echo '<span class="badge bg-red">'.calcolo_prezzo($prod['ac_W_min'],$prod['ac_H_min'],$prod['ac_coefficiente_sconto']).'</span>';
        }
      ?>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><small>x</small></span></button>
    </div>
    <div class="modal-content">
      <form class="frm-prodotto">
		    <div class="box-body">
          <div class="box-group" id="accordion">
            <div class="panel box box-primary">
              <div class="box-header with-border">
                <h4 class="box-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" class="">Impostazioni</a>
                </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true">
                <div class="box-body form-inline">
                  <div class="input-group">
                    Prodotto
                    <input type="text" name="ac_prodotto" id="ac_prodotto" value="<?=$prod['ac_prodotto']?>" class="form-control">
                  </div>
                  <div class="input-group col-sm-2">
                    Codice
                    <input type="text" name="ac_codice_prodotto" id="ac_codice_prodotto" value="<?=$prod['ac_codice_prodotto']?>" class="form-control">
                  </div>
                  <div class="input-group">
                    Collezione
                    <select id="id_categoria" name="id_categoria" class="form-control">
                      <option value="0">nessuna</option>
                      <?php
                      foreach ( $collezioni AS $coll ){
                        $cat = $coll['ac_categoria'];
                        if ( $coll['ac_categoria_lang'] != '' ){
                          $cat = $coll['ac_categoria_lang'];
                        }
                        if ( $prod['id_categoria'] == $coll['id_categoria'] ){
                          echo '<option value="'.$coll['id_categoria'].'" selected>'.$cat.'</option>';
                        } else {
                          echo '<option value="'.$coll['id_categoria'].'">'.$cat.'</option>';
                        }
                      }
                      ?>
                    </select>
                  </div>
                  <div class="input-group">
                    Designer
                    <select id="id_designer" name="id_designer" class="form-control">
                      <option value="0">nessuno</option>
                      <?php
                      foreach ( $designers AS $designer ){
                        if ( $prod['id_designer'] == $designer['id_designer'] ){
                          echo '<option value="'.$designer['id_designer'].'" selected>'.$designer['ac_nome_designer'].' '.$designer['ac_cognome_designer'].'</option>';
                        } else {
                          echo '<option value="'.$designer['id_designer'].'" >'.$designer['ac_nome_designer'].' '.$designer['ac_cognome_designer'].'</option>';
                        }
                      }
                      ?>
                    </select>
                  </div>
                  <div class="input-group">
                  	Simul.
                  	<select id="bl_simulatore" name="bl_simulatore" class="form-control">
                  	<?php
                  	   $simulatore = '';
                  	   if ( $prod['bl_simulatore'] ) {
                  	   	$simulatore = 'selected';
                  	   }
                  	?>
                  	<option value="0" <?=$simulatore?>>Non attivo</option>
                  	<option value="1" <?=$simulatore?>>Attivo</option>
                  	</select>
                  </div>
                  <div class="clearfix"></div>
                  <br>
                  <div class="input-group col-sm-2">
                    Largh min cm <input type="text" name="ac_W_min" id="ac_W_min" class="form-control" value="<?=$prod['ac_W_min']?>">
                  </div>
                  <div class="input-group col-sm-2">
                    Altez min cm <input type="text" name="ac_H_min" id="ac_H_min" class="form-control" value="<?=$prod['ac_H_min']?>">
                  </div>
                  <div class="input-group col-sm-2">
                    Ratio <input type="text" name="ac_ratio" class="form-control" value="<?=$prod['ac_ratio']?>" readonly>
                  </div>
                  <div class="input-group col-sm-2">
                    Largh max cm <input type="text" name="ac_W_max" id="ac_W_max" class="form-control" value="<?=$prod['ac_W_max']?>">
                  </div>
                  <div class="input-group col-sm-2">
                    Altez max cm <input type="text" name="ac_H_max" id="ac_H_max" class="form-control" value="<?=$prod['ac_H_max']?>">
                  </div>
                  <div class="input-group col-sm-1">
                    Attivo
                    <select class="form-control" name="bl_stato" id="bl_stato">
                      <?php
                      if ( $prod['bl_stato'] ){
                        echo '<option value="1" selected>Attivo</option>';
                        echo '<option value="0">Non attivo</option>';
                      } else {
                        echo '<option value="1">Attivo</option>';
                        echo '<option value="0" selected>Non attivo</option>';
                      }
                      ?>
                    </select>
                  </div>

                  <div class="clearfix"></div>

                  <div class="input-group col-lg-2">
                    Multicolor
                    <select class="form-control" name="bl_colore" id="bl_colore">
                      <?php
                      if ( $prod['bl_colore'] ){
                        echo '<option value="1" selected>Monocolre</option>';
                        echo '<option value="0">Multicolor</option>';
                      } else {
                        echo '<option value="1">Monocolore</option>';
                        echo '<option value="0" selected>Multicolor</option>';
                      }
                      ?>
                    </select>
                  </div>

                  <div class="col-lg-4">
                    Sconto <br>
                    <input type="text" id="ac_coefficiente_sconto" name="ac_coefficiente_sconto" class="form-control" value="<?=$prod['ac_coefficiente_sconto']?>" style="width:70px">&nbsp;
                    <small>Es. 0.85=sconto 15%</small>
                  </div>
                  <div class="col-lg-6">
                    Prodotti UpSell <br>
                    <input type="text" id="prodotti_upsell" name="prodotti_upsell" class="form-control" value="" style="width:100%">
                  </div>
                </div><!--/.box-body-->
              </div><!--/.collapseone-->
            </div><!-- /.panel -->

            <div class="panel box box-success">
              <div class="box-header with-border">
                <h4 class="box-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="collapsed" aria-expanded="false">Descrizione</a>
                </h4>
              </div>
              <div id="collapseThree" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                <div class="box-body">
                  <textarea name="descrizione_prodotto" id="descrizione_prodotto" class="descrizione_prodotto" placeholder="" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?=$prod['descrizione']?></textarea>
                </div>
              </div><!--/.collapseThree-->
            </div><!--/.panel-->

            <div class="panel box box-success">
              <div class="box-header with-border">
                <h4 class="box-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" class="collapsed" aria-expanded="false">SEO</a>
                </h4>
              </div>
              <div id="collapseFour" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                <div class="box-body">
                  <div class="col-lg-12">
                    META TITLE <br>
                    <input type="text" name="metatitle_prodotto" id="metatitle_prodotto" class="form-control meta_title_prodotto" value="<?=$prod['ac_meta_title']?>" placeholder="<?=$prod['ac_prodotto']?>" style="width:100%">
                  </div>
                  <div class="col-lg-12">
                    META DESCRIZIONE<br>
                    <textarea name="metadescrizione_prodotto" id="metadescrizione_prodotto" class="metadescrizione_prodotto" placeholder="" style="min-height:100px;min-width:80%;width:80%;font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?=$prod['ac_meta_description']?></textarea>
                  </div>
                  <div class="col-lg-12">
                    META KEYWORDS<br>
                    <textarea name="metakeywords_prodotto" id="metakeywords_prodotto" class="metakeywords_prodotto" placeholder="" style="min-height:100px;min-width:80%!important;font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?=$prod['ac_meta_keys']?></textarea>
                  </div>
                </div>
              </div><!--/.collapseFour-->
            </div><!--/.panel-->
          </div>
        </div><!-- /.box-body -->
        <div class="modal-footer">
        <input type="hidden" class="origine" value="normale">
        <button type="button" class="btn btn-success btn-prodotto-salva" data-id="<?=$prod['id_prodotto']?>" data-action="prodotto-salva">Salva</button>
        </div>
      </form>
    </div>
  </div>
</div>
<script>
$(document).ready ( function(){
  $('#prodottoModal').modal('show');

      $(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        //CKEDITOR.replace('editor1');
        //bootstrap WYSIHTML5 - text editor
        $(".descrizione_prodotto").wysihtml5({
          toolbar: {
            "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
            "emphasis": true, //Italics, bold, etc. Default true
            "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
            "html": true, //Button which allows you to edit the generated HTML. Default false
            "link": true, //Button to insert a link. Default true
            "image": true, //Button to insert an image. Default true,
            "color": false, //Button to change color of font
            "blockquote": true, //Blockquote
            "size": 'sm' //default: none, other options are xs, sm, lg
          }
        });
      });

	  //$("div#dropzone").dropzone({ url: "/controller/dropzone.upload.cfm" });
    //$("div#dropzone").dropzone({ url: "http://vm6.indual.it/indual/upload.sticasa.image.cfm" });

  $('.btn-prodotto-salva').on('click',function(){
    var stato = 0;
    var colore = 1;
    var prodotto = $('#ac_prodotto').val();
    var id_categoria = $("#id_categoria").val();
    $('#prodottoModal').modal('hide');
    $.post ( 'ajax/negozio' ,
      {
        action: 'prodotto-save',
        id: $(this).data('id'),
        id_categoria: $('#id_categoria').val(),
        id_designer: $('#id_designer').val(),
        ac_prodotto: $('#ac_prodotto').val(),
        ac_codice_prodotto:$('#ac_codice_prodotto').val(),
        ac_W_min: $('#ac_W_min').val(),
        ac_H_min: $('#ac_H_min').val(),
        ac_W_max: $('#ac_W_max').val(),
        ac_H_max: $('#ac_H_max').val(),
        ac_ratio: $('#ac_ratio').val(),
        bl_colore: $('#bl_colore').val(),
        bl_simulatore: $('#bl_simulatore').val(),
        descrizione: $('#descrizione_prodotto').val(),
        meta_title: $('#metatitle_prodotto').val(),
        meta_description: $('#metadescrizione_prodotto').val(),
        meta_keywords: $('#metakeywords_prodotto').val(),
        bl_stato: $('#bl_stato').val(),
        ac_coefficiente_sconto: $('#ac_coefficiente_sconto').val(),
        prodotti_upsell: $('#prodotti_upsell').val()
      } , function ( result ){
      	if ( $('.origine').val() == 'normale' ){
        $.post ( 'ajax/negozio' ,
          {
            action: 'negozio-prodotti',
            id: id_categoria,
            sito: $('.sito').val()
          } , function ( result ){

            $('.content').html ( result );
            doNotification ( 'Prodotto' , prodotto + ' aggiornato!' );
          }
        );
        } else {
          doNotification ( 'Prodotto' , prodotto + ' aggiornato!' );
          $('#prodottoModal').modal('hide');
        }

      }
    );
  })

});
</script>
