<div class="row">
  <div class="col-lg-12">
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
          <th>ID</th>
          <th>Prodotto</th>
          <th>Collezione</th>
          <th>Upsell</th>
          <th>Collezione</th>
          <th></th>
        </tr>
      </thead>
      <tbody>

        <?php
        $prodotto = '';
        foreach ( $prodotti AS $row ){
          $collezione = $row['collezione'];
          $collezione_extra = $row['collezione_extra'];
          if ( $row['collezione_extra'] != '' ){
            $collezione = $row['collezione_extra'];
          }
          $collezione_upsell = $row['collezione_upsell'];
          if ( $row['collezione_upsell_extra'] != '' ){
            $collezione_upsell = $row['collezione_upsell_extra'];
          }
          $product = '';
          if ( $prodotto != $row['prodotto'] ){
            $product = $row['prodotto'];
          }
          echo '<tr>
            <td>'.$row['id'].'</td>
            <td><strong>'.$product.'</strong></td>
            <td>'.$collezione.'</td>
            <td><strong>'.$row['prodotto_upsell'].'</strong></td>
            <td>'.$collezione_upsell.'</td>
            <td><button class="btn btn-sm btn-success btn-add-upsell" data-id="'.$row['id'].'" data-prodotto="'.$product.'" data-toggle="modal" data-target="#upsellModal">Aggiungi</button>&nbsp;<button class="btn btn-sm btn-danger btn-delete-upsell" data-id="'.$row['id'].'" data-upsell="'.$row['id_prodotto_upsell'].'">Elimina</button></td>
          </tr>';
          $prodotto = $row['prodotto'];
        }
        ?>
      </tbody>
    </table>
  </div>
</div>

<div class="modal fade in" tabindex="-1" id="upsellModal" role="dialog">
  <div class="modal-dialog" style="width:80%">
    <div class="modal-content">
      <div class="modal-header" style="background:#fff;">
        <label class="label label-primary modal-title" style="font-size:1.4em;"></label>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><small>x</small></span></button>
      </div>
      <div class="modal-body">
        <div class="col-lg-12">
          <label>Cerca prodotto/codice</label>
          <input type="text" class="form-control prodotto-search" placeholder="inserire il nome o il codice prodotto">
          <div class="prodotto-search-result"></div>
        </div>
      </div>
      <div class="modal-footer">
        <br><br>
        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#upsellModal">Chiudi</button>
        <button type="button" class="btn btn-success btn-upsell-salva" data-id="" data-action="prodotto-salva">Salva</button>
      </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function(){

  $('.btn-add-upsell').on('click',function(){
    var id = $(this).data('id');
    $('.modal-title').html ( $(this).data('prodotto') );
    $('.btn-upsell-salva').attr('data-id',id);
  })

  $('.prodotto-search').on('keyup',function(){
    if ( $(this).val().length > 2 ){
      $('.prodotto-search-result').html('');
      $.post ( 'ajax/negozio' ,
        {
          action: 'negozio-prodotto-search',
          search: $(this).val()
        }, function ( result ){
          $('.prodotto-search-result').html(result);
        }
      );
    } else {
      $('.prodotto-search-result').html('');
    }
  })
})
</script>
