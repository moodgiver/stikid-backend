<div class="modal fade" id="dropzoneModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
		<div class="col-md-12">
			<br>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<div class="panel panel-primary">
				<div class="panel-heading">Carica la foto della tua parete</div>
				<div class="panel-body" style="overflow:auto;">
          <form action="<?php echo base_url();?>upload/<?=$mode?>" id="myDropzone" class="dropzone" style="min-height:250px;border:4px dashed #cecece;">
            <input type="hidden" class="field" value="<?=$target_field?>">
            <input type="hidden" class="preview" value="<?=$target_preview?>">
            <input type="hidden" class="folder" name="folder" value="<?=$target_folder?>">
            <input type="hidden" class="image_url" name="image_url" value="<?=$target_uri?>">
            <input type="hidden" class="id" name="id" value="<?=$id?>">
          </form>
				</div>
				<div class="panel-footer text-center">
          <span class="percentage"></span><br>
          <div class="col-lg-12"><span class="progress-uploaded" style="clear:both;min-height:15px;opacity:.8;background:#87dcf8;width:0%;display:block;border-radius:2px"></span></div>
					<small>Dopo aver selezionato l'immagine attendere che la finestra si chiuda automaticamente</small><br>
          <button class="btn btn-default" data-dismiss="modal">Annulla</button>
				</div>
			</div>

		</div>
    </div>
  </div>
</div>

<script src="public/js/dropzone.js"></script>
<script src="public/js/custom.dropzone.js"></script>

<script>

$(document).ready(function(){
  Dropzone.autoDiscover = false;
  Dropzone.autoDiscover = false;
    $("#myDropzone").dropzone({
    dictDefaultMessage: "Clicca qui o trascina la tua immagine qui per caricarla",
    clickable: true,
    maxFilesize: 2,
		autoProcessQueue: true,
    uploadMultiple: false,
    addRemoveLinks: false,
		createImageThumbnails: true,
		acceptedFiles: 'image/*',
		dictFileTooBig: 'Immagine superiore a 2 MB. Impossibile caricare',
				init: function () {
          myDropzone = this;
					$('.dz-preview').remove();

          this.on("success", function(file) {
            $('.fa-spinner').addClass('hide');
            $('.' + $('.target_field').val() ).val( file['name'] );
					  $('.' + $('.preview').val()).attr('src', $('.image_url').val() + file['name'] );
            $('.' + $('.field').val()).val(file['name']);
            $('.error-image-scontrino').html('');
					  $('.dz-error-message').html('');
					  $('.dz-preview').remove();
        	  $('#dropzoneModal').modal('hide');
	        });

				  this.on("error" , function ( e , errorMsg ){
            $('.dz-error-message').removeClass('hide');
						$('.dz-error-message').html(errorMsg);
						$('.dz-details').html('');
					});

          this.on("addedfile" , function(){
            console.log ( 'added file ');
            $('.dz-error-message').addClass('hide');
            $('.fa-spinner').removeClass('hide');
          });

          this.on("uploadprogress" , function( file , progress ){
            $('.progress-uploaded').css ( 'width' , parseInt(progress) + '%' );
            $('.percentage').html ( parseInt(progress) + ' %' );
          });
      }
    });
});
</script>
