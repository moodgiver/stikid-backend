
<?php
  if ( $_POST['action'] != 'negozio-prodotto-scheda' ){
  include_once('collezione_seleziona.php');
  if ( isset ( $prodotti ) ){

  echo '<div class="row">';
  foreach ( $prodotti AS $o ){
    $colore = 'green';
    if ( $o['bl_stato'] == 0 ){
      $colore ='red';
    }
    $img = $o['uri_img'];
    if ( $o['uri_img'] == '' ){
      $img = 'public/img/noimage.png';
    }
    echo '
    <div class="prodotti">
    <div class="col-lg-4 col-xs-12">
    <div class="small-box" style="min-height:110px;background:#fff;color:#000;">
      <div class="inner" style="padding-top:0px;padding:0px">
        <h4 class="'.$colore.'" style="background:'.$colore.';color:#fff;padding:5px;font-size:.8em;">'.$o['ac_prodotto'].'</h4>
        <div class="width:100%;padding:5px">
        <p style="padding-left:5px;">'.$o['ac_W_min'].' x '.$o['ac_H_min'].' cm</p>
        <p style="padding-left:5px;">Codice: '.$o['ac_codice_prodotto'].' (ID: '.$o['id_prodotto'].')</p>
        <p style="padding-left:5px;padding-right:5px;">Special: <select class="form-control selector" data-controller="add-to-special" data-id="'.$o['id_prodotto'].'">
              <option value=""></option>
            </select>
        </p>
        </div>
      </div>
      <div class="icon">
         <a href="#" style="font-size:1.2em;color:##888;" class="btn-prodotto-immagini" data-action="negozio-prodotto-immagini" data-id="'.$o['id_prodotto'].'" data-website="'.$_POST['sito'].'" data-collezione="'.$o['id_categoria'].'" data-prodotto="'.$o['ac_prodotto'].'" title="Gestisci immagini prodotto"><img src="'.$img.'" class="immagine_'.$o['id_prodotto'].'" width="80" style="position: absolute;right: -10px;top: 10px;min-width: 110px;" title="Gestisci immagini prodotto"></a>
      </div>
      <div class="small-box-footer" style="background:#999">

        <a href="#" style="font-size:1.2em;color:#fff;" class="btn-edit-prodotto" data-action="negozio-prodotto-modifica" data-id="'.$o['id_prodotto'].'" data-collezione="'.$o['id_categoria'].'" data-website="'.$_POST['sito'].'" data-prodotto="'.$o['ac_prodotto'].'" title="Modifica prodotto">
          <i class="fa fa-edit"></i> Modifica
        </a>&nbsp;&nbsp;
        <a href="#" style="font-size:1.2em;color:#fafafa;" class="btn-copy-prodotto" data-action="negozio-prodotto-copy" data-id="'.$o['id_prodotto'].'" data-prodotto="'.$o['ac_prodotto'].'" data-collezione="'.$o['id_categoria'].'"  data-website="'.$_POST['sito'].'" title="Duplica prodotto">
          <i class="fa fa-copy"></i> Duplica
        </a>
      </div>
    </div>
    </div>
    </div>
    ';

    }
    echo '</div>';
  }
  }
  if ( $mode == 'modifica' ){
    include_once ( 'prodotto_modifica.php' );
  }
?>

<script>
$(document).ready ( function() {
  $('.btn-edit-prodotto').on('click',function(){
    $.post ( 'ajax/negozio' ,
      {
        action: $(this).data('action'),
        id: $(this).data('collezione'),
        id_prodotto: $(this).data('id'),
        sito: $(this).data('website')
      }, function ( result ){
        $('.content').html(result);
      }
    )
  })

  $('.btn-copy-prodotto').on('click',function(){
    var id_categoria = $(this).data('collezione')
    var sito = $(this).data('website')
    $.post ( 'ajax/negozio' ,
      {
        action: $(this).data('action'),
        id_prodotto: $(this).data('id'),
        prodotto: $(this).data('prodotto'),
        id_categoria: id_categoria,
        sito: $(this).data('website')
      }, function ( result ){
        $.post ( 'ajax/negozio' ,
          {
          action: 'negozio-prodotti',
          id: id_categoria,
          sito: sito
          } , function ( result ){
            doNotification ( 'Prodotto' ,'Prodotto duplicato' );
            $('.content').html(result);
          }
          );
      }
    )
  })

  $('.btn-prodotto-immagini').on('click',function(){
    $.post ( 'ajax/negozio' ,
      {
        action: $(this).data('action'),
        id: $(this).data('collezione'),
        id_prodotto: $(this).data('id'),
        sito: $(this).data('website')
      }, function ( result ){
        $('.content').html(result);
      }
    )
  })
})
</script>
