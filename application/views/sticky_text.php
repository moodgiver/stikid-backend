<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <h2 class="title">STICKY TEXT</h1></div>
  <p><?=$stickytext_description[0]['stickytext_description']?></p>

  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center lightBD jumbotron padding5">
    <h3><span id="miotesto" class="blackFG">Inserisci il tuo testo</span></h3>
  </div>
  <div class="clearfix"></div>

  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
      <h5>Colore</h5>
      <?php
        foreach ( $colori AS $c ){
          echo '<div class="btn-colore-testo colore" data-action="color-canvas" data-hex="'.$c['ac_rgb'].'" data-color="'.$c['ac_colore'].'" name="'.$c['ac_rgb'].';'.$c['ac_colore'].'" style="background:#'.$c['ac_rgb'].';z-index:1000;border:1px solid #cecece;" title="'.$c['ac_colore'].'"></div>';
        }

      ?>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
      <h5>Altezza testo cm</h5>
      <h5><input type="radio" class="altezza" name="altezza" value="10" checked> 10
      <input type="radio" class="altezza" name="altezza" value="20"> 20
      <input type="radio" class="altezza" name="altezza" value="30"> 30
      <input type="radio" class="altezza" name="altezza" value="50"> 50
      </h5>
    </div>
  </div>

  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
    <h5>Testo</h5>
  </div>

  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
    <h5>Larg. cm</h5>
  </div>

  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
    <h5>Prezzo</h5>
  </div>

  <?php
    $righe = 4;
    for ( $r=1 ; $r<$righe ; $r++ ){
      echo '
        <div class="col-lg-4">
          <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">Riga '.$r.'</div>
              <input type="text" class="form-control testo testo_'.$r.'" name="'.$r.'" placeholder="Inserisci il tuo testo">
            </div>
          </div>
        </div>';
      echo '
        <div class="col-lg-4">
          <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon"><span class="fa fa-arrows-alt"></span></div>
              <input type="text" class="form-control width width_'.$r.'" readonly>
            </div>
          </div>
        </div>';
      echo '
        <div class="col-lg-4">
          <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon"><span class="fa fa-euro"></span></div>
              <input type="text" class="form-control prezzo prezzo_'.$r.'" readonly>
            </div>
          </div>
        </div>
        ';
    }
  ?>


    <div class="clearfix"></div>
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 jumbotron padding5">
    <h5>Clicca sulla font da applicare</h5>
    <?php
      $f = 0;
      $active = '';
      foreach ( $fonts AS $font ){
        if ( $f == 1 ){
          $active = 'square-active';
        }
        echo '
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 whiteBG pointer btn-font'.$active.'" name="'.$f.';'.$font['font'].';'.$font['ratio'].';'.$font['maxratio'].';'.$font['dim'].'">
          <img src="'.$this->config->item('static_url').'images/fonts/'.$font['img'].'.jpg" width="200"></div>';
      }
    ?>
    </div>


      <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">
        <p>Totale</p>
        <div class="input-group">
          <div class="input-group-addon"><span class="fa fa-euro"></span></div>
          <input type="text" class="form-control prezzo_sticker" readonly><br>
        </div>
        <button class="btn btn-add-stickytext-to-cart get btn-action" data-action="addToCart" data-id="2299" data-uuid="" data-sconto="1"><span class="fa fa-shopping-cart"></span>Carrello</button>
      </div>

</div>

  <input type="hidden" class="currentID" name="id_prodotto" value="2299">
  <input type="hidden" name="ac_prodotto" value="Sticky Text">
  <input type="hidden" class="thumb"		value="900000.jpg">
  <input type="hidden" class="currentTesto" name="testo" value="">
  <input type="hidden" class="currentWidth" name="w" value="">
  <input type="hidden" class="currentHeight" name="h" value="">
  <input type="hidden" class="currentFont" name="font" value="<?=$fonts[0]['font']?>">
  <input type="hidden" class="currentRatio" value="<?=$fonts[0]['ratio']?>">
  <input type="hidden" class="currentMaxRatio" value="<?=$fonts[0]['maxratio']?>">
  <input type="hidden" class="currentDim" value="<?=$fonts[0]['dim']?>">
  <input type="hidden" class="currentColor" value="##000000">
  <input type="hidden" class="currentColorName" value="Black">
  <input type="hidden" class="currentTotale" value="0">

  <script src="<?=base_url()?>public/js/cufon-yui.js" type="text/javascript"></script>
  <script src="<?=base_url()?>public/fonts/ErikaType_400.font.js" type="text/javascript"></script>
  <script src="<?=base_url()?>public/fonts/ErikaTypeBold_700.font.js" type="text/javascript"></script>
  <script src="<?=base_url()?>public/fonts/JackInput_400.font.js" type="text/javascript"></script>
  <script src="<?=base_url()?>public/fonts/Makisupa_400.font.js" type="text/javascript"></script>
  <script src="<?=base_url()?>public/fonts/Nouveau_IBM_500.font.js" type="text/javascript"></script>
  <script src="<?=base_url()?>public/fonts/NovaMono_400.font.js" type="text/javascript"></script>
  <script src="<?=base_url()?>public/fonts/White_Rabbit_400.font.js" type="text/javascript"></script>
  <!--- <script src="<?=base_url()?>public/fonts/fonts.js" type="text/javascript"></script> --->
  <script src="<?=base_url()?>public/js/stickytext.js"></script>
