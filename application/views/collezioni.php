<?php
  $static = $this->config->item('static_url');
  $imgURI = $static.'images/collezioni/';
  foreach ( $collezioni_home AS $c ){
    $cat = $c['ac_categoria'];
    $catURI =  str_replace(' ','-',$cat).'-wall-stickers';
    if ( $c['ac_categoria_lang'] != '' ){
      $cat = $c['ac_categoria_lang'];
      $catURI = str_replace(' ','-',$cat).'-adesivi-murali';
    }
    $catImage = $c['immagine'];
    if ( $this->config->item('sito') == 1){
      $imgURI = $static.'images/ambienti/small/';
      $catImage = $c['immagine_collezione'];
    }
    if ( $catImage == '' ){
      $aImages = explode(',',$c['ac_immagini_demo']);
      $catImage = $aImages[0];
      $imgURI = $static.'images/ambienti/medium/';
    }
    if ( $this->config->item('isqpb') ){
      $catURI = str_replace(' ','-',$cat).'-quadri-per-bambini';
      $imgURI = $static.'images/canvas/preview/';
      $aImages = explode(',',$c['ac_immagini_demo']);
      $catImage = $aImages[0];
    }



    echo '
    <div class="col-lg-3 col-sm-4 col-xs-6" style="padding:4px">
      <div class="product-image-wrapper collezione-home">
        <div class="single-products" itemscope itemtype="http://schema.org/Product" itemprop="product">
          <div class="productinfo text-center">
            <a href="'.base_url().''.$catURI.'" class="menu-categorie" name="'.$c['id_categoria'].';'.$cat.'"><img class="img-collezione" src="'.$imgURI.''.$catImage.'" alt="'.$cat.' adesivi murali" />
            <br><br>
            <p itemprop="name" content="WallSticker - Adesivo Murale - '.$cat.'">'.$cat.'</p>
            </a>
          </div>
        </div>
      </div>
    </div>';
  }
?>
