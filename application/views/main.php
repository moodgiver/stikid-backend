<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php


  switch ($mode){

    case 'collezione':
      $title = 'STIKID - '.$prodotti_collezione[0]['collezione_meta_title'];
      $meta[0]['content'] = $prodotti_collezione[0]['ac_meta_description'];
      $meta[1]['content'] = $prodotti_collezione[0]['ac_meta_keys'];
    break;

    case 'prodotto':
      $cat = $prodotto[0]['ac_categoria'];
      if ( $prodotto[0]['ac_categoria_lang'] != ''){
        $cat = $prodotto[0]['ac_categoria_lang'];
      }
      $title = 'STIKID - '.$cat.' - '.$prodotto[0]['ac_prodotto'].' - Adesivo Murale';
      $meta[0]['content'] = $prodotto[0]['ac_meta_description'];
      $meta[1]['content'] = $prodotto[0]['ac_meta_keys'];
    break;
  }

?>

<?php
  include_once('header.php');
  include_once('sidebar.php');
  include_once('content.php');
  include_once('footer.php');
?>
