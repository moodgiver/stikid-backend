<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">

    <!-- Sidebar Menu -->
    <ul class="sidebar-menu">

      <?php

        $sm = $this->config->item('menu');
        foreach ( $sm AS $m ){
          if ( $m['header'] ){
            if ( in_array($_SESSION['user']['role'],$m['role']) ){
              echo '<li class="header"><span>'.$m['header'].'</span></li>';
            }
          }
          if ( $m['submenu'] ){
            if ( in_array($_SESSION['user']['role'],$m['role']) ){

            echo '<li class="treeview"><a href="#"><i class="fa fa-'.$m['icon'].'"></i><span>'.$m['name'].'</span><i class="fa fa-angle-left pull-right"></i></a>';
            }
            echo '<ul class="treeview-menu">';
            $sub = $m['submenu'];
            foreach ( $sub AS $s ){
              $sito = '';
              if ( isset($s['sito']) ){
                $sito = $s['sito'];
              }

              if ( in_array($_SESSION['user']['role'],$s['role']) ){
                if ( !isset($s['link']) ){
                echo '<li class="menu" data-title="'.$m['name'].' '.$s['name'].'" data-mode="'.$m['mode'].'" data-action="'.$s['routing'].'" data-sito="'.$sito.'"><a href="#">'.$s['name'].'</a></li>';
                } else {
                    echo '<li class="menu"><a href="'.$s['link'].'">'.$s['name'].'</a></li>';
                }
              }
            }
            echo '</ul></li>';
          } else {
            if ( in_array($_SESSION['user']['role'],$m['role']) ){
              echo '<li class="treeview"><a href="#"><i class="fa   fa-'.$m['icon'].'"></i><span>'.$m['name'].'</span></a>';
            }
          }
        }

      ?>
    </ul><!-- /.sidebar-menu -->
  </section>
  <!-- /.sidebar -->
</aside>

<script>
$(document).ready ( function(){
  var content = $('.content');
  $('.menu').click(function(){
    $('.title-header').html ( $(this).data('title') );
    $.post( 'ajax/' + $(this).data('mode') ,
      {
        action: $(this).data('action'),
        sito: $(this).data('sito')
      }, function ( result ){
        content.html(result);
      }
    );
  });
});
</script>
