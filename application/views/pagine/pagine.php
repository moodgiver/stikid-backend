<div class="row">
  <!--<div class="col-lg-12">
    <div class="col-lg-4">
      <strong>Sito</strong> <select class="form-control filtra-sito">
        <option value="-1" selected>Tutte le pagine</option>
        <?php
        $selected = '';
        $currentSito = -1;
        if ( isset($_POST['sito'] ) ){
          $currentSito = $_POST['sito'];
        }

        foreach ( $siti AS $sito ){
          $selected = '';
          $site = $sito['sito'];
          if ( $currentSito == $sito['id'] ){
            $selected = 'selected';
          }
          echo '<option value="'.$sito['id'].'" '.$selected.'>'.$site.'</option>';
        }
        ?>
      </select>
    </div>
    <div class="col-lg-4">
      <strong>Posizione</strong>
      <select class="form-control menu_position">
        <option value="-1">Tutte</option>
        <?php
          foreach ( $menus AS $menu ){
            echo '<option value="'.$menu.'">'.$menu.'</option>';
          }
         ?>
      </select>
    </div>
    <div class="col-lg-2">
      <br>
      <button class="btn btn-primary btn-add-page">Aggiungi</button>
    </div>
    <div class="clearfix"></div>
  -->
    <div class="col-lg-12">
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
          <th>ID</th>
          <th>Titolo</th>
          <!--<th>Sito</th>-->
          <th>URL</th>
          <!--<th>Menu</td>
          <th>Ordine</th>-->
          <th>Stato</th>
        </tr>
      </thead>
      <tbody>
        <?php
        foreach ( $pagine AS $pagina ){
          $url = str_replace(' ','-',$pagina['ac_titolo']);
          $url = strtolower($url);
          $checked = '';
          if ( $pagina['bl_status'] ){
            $checked = 'checked';
          }
          echo '
            <tr class="riga row_'.$pagina['int_sito'].' pos_'.$pagina['ac_menu'].'">
              <input type="hidden" class="new_url_'.$pagina['id_pagina'].'" value="'.$url.'">
              <td class="pointer edit-page" data-id="'.$pagina['id_pagina'].'"  data-title="'.$pagina['ac_titolo'].'"  data-url="'.$pagina['ac_url'].'" data-ordine="'.$pagina['int_ordine'].'">'.$pagina['id_pagina'].'</td>
              <td class="pointer edit-page edit-page-titolo_'.$pagina['id_pagina'].'" data-id="'.$pagina['id_pagina'].'" data-title="'.$pagina['ac_titolo'].'" data-url="'.$pagina['ac_url'].'" data-ordine="'.$pagina['int_ordine'].'">'.$pagina['ac_titolo'].'</td>

            <td class="edit-page-url_'.$pagina['id_pagina'].'">'.$pagina['ac_url'].'</td>


            <td><input type="checkbox" class="pagina-data-status" data-field="bl_status" data-id="'.$pagina['id_pagina'].'" '.$checked.'></td>
            <td>
            <button class="btn btn-sm btn-primary btn-duplica-pagina" data-id="'.$pagina['id_pagina'].'">Duplica</button>
            <button class="btn btn-sm btn-danger btn-elimina-pagina" data-id="'.$pagina['id_pagina'].'"><span class="fa fa-trash"></span></button>
            </td>
          </tr>
          ';
        }
         ?>
      </tbody>
    </table>
  </div>
</div>
<!--
<td><select class="form-control pagina-data" data-field="int_sito" data-id="'.$pagina['id_pagina'].'">';
foreach ( $siti AS $sito ){
$site = $sito['sito'];
$selected = '';
if ( $sito['id'] == $pagina['int_sito'] ){
$selected = 'selected';
}
echo '<option value="'.$sito['id'].'" '.$selected.'>'.$site.'</option>';
}
echo '</select></td>

<td><select class="form-control pagina-data" data-field="ac_menu" data-id="'.$pagina['id_pagina'].'"><option value="">-- nessun menu --</option>'
;

foreach ( $menus AS $menu ){
$selected = '';
if ( $menu == $pagina['ac_menu'] ){
  $selected = 'selected';
}
echo '<option value="'.$menu.'" '.$selected.'>'.$menu.'</option>';
}
echo '</select></td>
  <td>'.$pagina['int_ordine'].'</td>
-->
<div class="modal fade" id="pageModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document" style="width:70vw;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong class="modal-title">Modifica pagina</strong>
      </div>
      <div class="modal-body">
            <div class="col-lg-5">
            	<label>Titolo</label>
                <input type="text" class="pagina_titolo form-control" value="">
            </div>
            <div class="col-lg-5">
              <label>URL</label>
                <input type="text" class="pagina_url form-control" value="">
            </div>
            <div class="col-lg-2">
              <label>Ordine</label>
              <select class="form-control pagina_ordine">
                <?php
                for ( $n=0 ; $n<15 ; $n++){
                  echo '<option value="'.$n.'">'.$n.'</option>';
                }
                ?>
              </select>
            </div>
        		<textarea name="pagina_testo" id="pagina_testo" class="pagina_testo" placeholder="" style="width: 100%; height: 350px; font-size: 14px; line-height: 18px; border: 1px solid ##dddddd; padding: 10px;"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>
        <button type="button" class="btn btn-primary btn-save-page" data-id="">Salva</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<script>
$(document).ready ( function(){


      $(function () {
    		// Enable local "abbr" plugin from /myplugins/abbr/ folder.
			//CKEDITOR.plugins.addExternal( 'abbr', '/myplugins/abbr/', 'plugin.js' );

			// extraPlugins needs to be set too.
			//CKEDITOR.replace( 'editor1', {
    		//extraPlugins: 'abbr'
        //bootstrap WYSIHTML5 - text editor
        $(".pagina_testo").wysihtml5({
          toolbar: {
            "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
            "emphasis": true, //Italics, bold, etc. Default true
            "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
            "html": true, //Button which allows you to edit the generated HTML. Default false
            "link": true, //Button to insert a link. Default true
            "image": true, //Button to insert an image. Default true,
            "color": false, //Button to change color of font
            "blockquote": true, //Blockquote
            "size": 'sm' //default: none, other options are xs, sm, lg
          }
        });
      });

  /*var sito = $('.filtra-sito').val();
      if ( sito == -1 ){
        $('.riga').removeClass('hide');
      } else {
        $('.riga').addClass('hide');
        $('.row_' + sito).removeClass('hide');
      }

  $('.filtra-sito').on('change',function(){
    var sito = $(this).val();
    if ( sito == -1 ){
      $('.riga').removeClass('hide');
    } else {
      $('.riga').addClass('hide');
      $('.row_' + sito).removeClass('hide');
    }
  })

  $('.menu_position').on('change',function(){
    var pos = $(this).val();
    if ( sito == '-1' ){
      $('.riga').removeClass('hide');
    } else {
      $('.riga').addClass('hide');
      $('.pos_' + pos).removeClass('hide');
    }
  })
  */

  $('.btn-add-page').on('click',function(){
    $('#pageModal').modal('show');
    $('.btn-save-page').attr('data-id',0);
  });

  $('.edit-page').on('click',function(){
    var id = $(this).data('id');
    var ordine = $(this).data('ordine');
    $('.pagina_titolo').val ( $(this).data('title') );
    $('.pagina_url').val( $(this).data('url') );
    $('.btn-save-page').attr('data-id',id);
    if ( $('.pagina_url').val() == '' ){
      $('.pagina_url').val ( $('.pagina_titolo').val().replace(/[^a-zA-Z0-9]/g,'-').toLowerCase() );
    }
    $('.pagina_ordine').val(ordine);
    $.post ( 'ajax/contenuti' ,
      {
        action: 'pagina_testo',
        id: $(this).data('id')
      }, function ( result ){
        $('iframe').contents().find('.wysihtml5-editor').html(result);
        $('#pageModal').modal('show');
        //$('#pagina_testo').data("wysihtml5").editor.setValue(result);
      }
    )
  })

  $('.btn-save-page').on('click',function(){
    var id = $(this).data('id');
    var titolo = $('.pagina_titolo').val();
    var url = $('.pagina_url').val();
    $.post( 'ajax/contenuti' ,
      {
        action: 'pagina-save',
        id: $(this).data('id'),
        title: $('.pagina_titolo').val(),
        url: $('.pagina_url').val(),
        ordine : $('.pagina_ordine').val(),
        text: $('iframe').contents().find('.wysihtml5-editor').html()
      }, function ( result ){
        $('.edit-page-titolo_' + id).html();
        $('.edit-page-titolo_' + id).html(titolo);
        $('.edit-page-url_' + id).html(url);
        save_result(result);
      }
    )
  })

  $('.btn-duplica-pagina').on('click',function(){
    $.post ( 'ajax/contenuti' ,
      {
        action: 'pagina-duplica',
        id: $(this).data('id')
      }, function ( result ){
        save_result(result);
      }
    )
  })

  $('.pagina-data-status').on('click',function(){
    var value = 0;
    var id = $(this).data('id');
    var field = $(this).data('field');
    if ( $(this).is(':checked') ){
      value = 1;
    }
    $.post ( 'ajax/contenuti' ,
      {
        action: 'pagina-field-save',
        field: field,
        value: value,
        id: id
      }, function ( result ){
        save_result(result);
      }
    )
  });

  $('.pagina-data').on('change',function(){
    var id = $(this).data('id');
    var field = $(this).data('field');
    var value = $(this).val();
    $.post ( 'ajax/contenuti' ,
      {
        action: 'pagina-field-save',
        field: field,
        value: value,
        id: id
      }, function ( result ){
        save_result(result);
      }
    )
  })

  $('.pagina_titolo').on('change',function(){
      $('.pagina_url').val ( $(this).val().replace(/[^a-zA-Z0-9]/g,'-').toLowerCase() );
  });

  function save_result(result){
    if ( result ){
      doNotification ( 'Salva pagina' , 'Pagina salvata correttamente' );
      $('#pageModal').modal('hide');
      $.post ( 'ajax/contenuti' ,
        {
          action: 'contenuti-pagine',
          sito: $('.filtra-sito').val()
        }, function(result){
          $('.content').html ( result );
        }
      )
    } else {
      doNotification ( 'Salva pagina' , 'Si e\' verificato un errore. Contattarel l\'amministratore' );
    }
  }

  $('.btn-elimina-pagina').on('click',function(){
    var id = $(this).data('id');
    $.post ( 'ajax/contenuti' ,
      {
        action: 'pagina-elimina',
        id: id
      }, function ( result ){
        $('.content').html(result);
      }
    )
  });
})
</script>
