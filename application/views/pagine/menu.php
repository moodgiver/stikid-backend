<div class="row">
  <div class="col-lg-12">
    <div class="col-lg-6">
      <strong>Sito</strong> <select class="form-control filtra-sito">
        <?php
        $selected = '';
        $currentSito = -1;
        if ( isset($_POST['sito'] ) ){
          $currentSito = $_POST['sito'];
        }

        foreach ( $siti AS $sito ){
          if ( $sito['id'] > 0 ){
          $selected = '';
          $site = $sito['sito'];
          if ( $currentSito == $sito['id'] ){
            $selected = 'selected';
          }
          echo '<option value="'.$sito['id'].'" '.$selected.'>'.$site.'</option>';
          }
        }
        ?>
      </select>
    </div>
    <div class="col-lg-6">
      <strong>Menu/Posizione</strong>
      <select class="form-control menu_position">
        <option value="-1">Seleziona menu/posizione</option>
        <?php
        $pos = '';
        if ( isset($_POST['position']) ){
          $pos = $_POST['position'];
        }
        foreach ( $menus AS $menu ){
          $selected = '';
          if ( $menu == $pos ){
            $selected = 'selected';
          }
          echo '<option value="'.$menu.'" '.$selected.'>'.$menu.'</option>';
        }
        ?>
      </select>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="col-lg-6">
    <h3>Pagine</h3>
    <table class="table table-striped table-bordered">
    <thead>
      <tr>
        <th>Titolo</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <?php
      foreach ( $pagine AS $pagina ){
        $url = str_replace(' ','-',$pagina['ac_titolo']);
        $url = strtolower($url);
        $checked = '';
        if ( $pagina['bl_status'] ){
          $checked = 'checked';
        }
        echo '
          <tr class="riga row_'.$pagina['int_sito'].' pos_'.$pagina['ac_menu'].'">
            <input type="hidden" class="new_url_'.$pagina['id_pagina'].'" value="'.$url.'">
            <td class="pointer edit-page edit-page-titolo_'.$pagina['id_pagina'].'" data-id="'.$pagina['id_pagina'].'" data-title="'.$pagina['ac_titolo'].'" data-url="'.$pagina['ac_url'].'" data-ordine="'.$pagina['int_ordine'].'">'.$pagina['ac_titolo'].'</td>
          <td><button class="btn btn-sm btn-primary btn-add-pagina-to-menu" data-id="'.$pagina['id_pagina'].'"><span class="fa fa-angle-right"></span></button>
        </tr>
        ';
       }
      ?>

    </tbody>
    </table>
  </div>
  <div class="col-lg-6">
    <h3>Menu <span class="menu-pos"><?=$pos?></span></h3>
    <table class="table table-striped table-bordered">
    <thead>
      <tr>
        <th>Titolo</th>
        <th>Ordine</th>
      </tr>
    </thead>
    <tbody>
    <?php
      if ( isset($_POST['position']) ){
        foreach ( $positions AS $pagina ){
          echo '
            <tr class="riga row_'.$pagina['int_sito'].' pos_'.$pagina['ac_menu'].'">
              <input type="hidden" class="new_url_'.$pagina['id_pagina'].'" value="'.$url.'">
              <td class="pointer edit-page edit-page-titolo_'.$pagina['id_pagina'].'" data-id="'.$pagina['id_pagina'].'" data-title="'.$pagina['ac_titolo'].'" data-url="'.$pagina['ac_url'].'" data-ordine="'.$pagina['int_ordine'].'">'.$pagina['ac_titolo'].'</td>
            <td>
            <select class="form-control menu_pagina_ordine" data-id="'.$pagina['id_posizione'].'">';
            for ( $n=0 ; $n<20 ; $n++ ){
              $selected = '';
              if ( $n == $pagina['int_ordine'] ){
                $selected = 'selected';
              }
              echo '<option value="'.$n.'" '.$selected.'>'.$n.'</option>';
            }
            echo '</select></td>
            <td><button class="btn btn-sm btn-danger btn-remove-pagina-from-menu" data-id="'.$pagina['id_posizione'].'"><span class="fa fa-trash"></span></button>
          </tr>
          ';
        }
      }
    ?>
  </div>
</div>

<script>
$(document).ready(function(){
  $('.filtra-sito').on('change',function(){
    var pos = $('.menu_position').val();
    var sito = $(this).val();
    $.post ( 'ajax/contenuti' ,
      {
        action: 'contenuti-menu',
        position: pos,
        sito: sito
      }, function(result){
        $('.content').html(result);
      }
    )
  })

  $('.menu_position').on('change',function(){
    var pos = $(this).val();
    $('.menu-pos').html ( pos );
    var sito = $('.filtra-sito').val();
    $.post ( 'ajax/contenuti' ,
      {
        action: 'contenuti-menu',
        position: pos,
        sito: sito
      }, function(result){
        $('.content').html(result);
      }
    )
  })

  $('.btn-add-pagina-to-menu').on('click',function(){
    var id = $(this).data('id');
    var pos = $('.menu_position').val();
    var sito = $('.filtra-sito').val();
    $.post ( 'ajax/contenuti' ,
      {
        action: 'contenuti-menu-add-pagina',
        pagina: id,
        position: pos,
        sito: sito
      }, function(result){
        $('.content').html(result);
      }
    )
  })

  $('.btn-remove-pagina-from-menu').on('click',function(){
    var id = $(this).data('id');
    var pos = $('.menu_position').val();
    var sito = $('.filtra-sito').val();
    $.post ( 'ajax/contenuti' ,
      {
        action: 'contenuti-menu-remove-pagina',
        id: id,
        position: pos,
        sito: sito
      }, function ( result ){
        $('.content').html(result)
      }
    )
  })

  $('.menu_pagina_ordine').on('change',function(){
    var id = $(this).data('id');
    var ordine = $(this).val();
    var pos = $('.menu_position').val();
    var sito = $('.filtra-sito').val();
    $.post ( 'ajax/contenuti' ,
      {
        action: 'contenuti-menu-ordine-pagina',
        id: id,
        ordine: ordine,
        position: pos,
        sito: sito
      }, function ( result ){
        $('.content').html(result)
      }
    )
  })

})
</script>
