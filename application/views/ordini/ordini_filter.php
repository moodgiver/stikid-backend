<?php
  $ordine = '';
  $cliente = '';
  $email = '';
  if ( isset($_POST['ordine']) ){
    $ordine = $_POST['ordine'];
  }
  if ( isset($_POST['cliente']) ){
    $cliente = $_POST['cliente'];
  }
  if ( isset($_POST['email']) ){
    $email = $_POST['email'];
  }
?>
<div class="col-lg-12 clearfix">
  <div class="col-lg-4 col-xs-6 cerca-ordine">
    <input type="text" class="form-control ordine_nr" placeholder="nr ordine > Es. 2022" value="<?=$ordine?>">
  </div>
  <div class="col-lg-6 col-xs-6 cerca-ordine">
    <input type="text" class="form-control ordine_cliente" placeholder="inserire il cognome del  cliente" value="<?=$cliente?>">
  </div>
</div>
<br><br>
<div class="col-lg-12 clearfix">
  <div class="col-lg-6 col-xs-6 cerca-ordine">
    <input type="text" class="form-control ordine_email" placeholder="inserire la mail del cliente" value="<?=$email?>">
  </div>

  <div class="col-lg-1 col-xs-2 btn-cerca">
    <button class="btn btn-primary btn-ordine-cerca">Cerca</button>
  </div>
</div>
<div class="clearfix"></div>

<script>
$(document).ready(function(){
  $('.btn-ordine-cerca').on('click',function(){
    var ordine = $('.ordine_nr').val();
    var cliente = $('.ordine_cliente').val();
    var email = $('.ordine_email').val();
    var cerca = false;
    if ( ordine.length > 3 ){
      cerca = true;
    }
    if ( cliente.length > 2 ){
      cerca = true;
    }
    if ( email.length > 2 ){
      cerca = true;
    }
    if ( cerca ){
      $.post ( 'ajax/ordini' ,
        {
          action: 'ordine-cerca',
          ordine: ordine,
          cliente: cliente,
          email: email
        }, function(result){
          $('.content').html ( result );
        }
      )
    }
  })
  /*$('#daterange').daterangepicker({
    "locale" : "it"
  });

  $('.btn-filter-status').on('click',function(){
    console.log(picker.startDate.format('YYYY-MM-DD'));
    console.log(picker.endDate.format('YYYY-MM-DD'));
  })
  $('#daterange').on('apply.daterangepicker', function(ev, picker) {

  });
  */

});

</script>
