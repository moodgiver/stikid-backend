<?php
  if ( $ricerca ){
    echo '<div class="row clearfix" style="margin:5px;padding:10px;">';
    include_once('ordini_filter.php');
    echo '</div>';
    //print_r($ordini);
    include_once ('ordini_status.php');
  }

  /*
  foreach ( $ordini AS $o ){
    $app = $this->config->item('application');
    $stati = $app['status']['stato'];
    $status = '';
    $os = $o['bl_stato'];
    if ( $os < 0 ){
      $os = 0;
    }
    $status = $stati[$os];

    $stato = $status['name'];
    $colore = $status['colore'];
    $palette = $status['palette'];
    $ordinenr = explode('/',$o['ac_nrordine']);

    $totale = number_format((float)$o['ac_totale']+(float)$o['ac_spesespedizione'],2);
    $sconto = '';
    if ( (float)$o['ac_sconto'] > 0 ){
      $coupon_usato = $o['coupon_usato'];
      $sconto = '<small><span class="fa fa-money"></span><span> -'.number_format($o['ac_sconto'],2).' ('.$coupon_usato.')</span></small>';
    }
    $cliente = '<strong>'
    .strtoupper($o['ac_nome']).' '
    .strtoupper($o['ac_cognome']).'</strong><br>'
    .strtoupper($o['ac_indirizzo']).' '
    .strtoupper($o['ac_cap']).' '
    .strtoupper($o['ac_citta']).' '
    .strtoupper($o['ac_pv']).'
    <span class="fa fa-phone"></span>'
    .$o['ac_telefono'].' '.$o['ac_cellulare'].'</span>';

    $spedisci_a = '';
    if ( $o['bl_delivery_delivery'] == 1 ){
      $spedisci_a = '<span style="color:red">'.strtoupper($o['ac_firstname_delivery']).' '.strtoupper($o['ac_lastname_delivery']).' - ' .strtoupper($o['ac_address_delivery']).' '  .strtoupper($o['ac_zip_delivery']).' '
      .strtoupper($o['ac_city_delivery']).' '
      .strtoupper($o['ac_state_delivery']).'
      <span class="fa fa-phone"></span> '
      .$o['ac_phone_delivery'].' '.$o['ac_mobile_delivery'].'</span></span>';
    }

    echo '
    <div class="row riga_'.$o['id_ordine_summary'].' status_'.$o['id_ordine_summary'].'_'.$o['bl_stato'].'" style="margin-left:0px;padding:0px;max-height:90px;width:100%;background:#fff">
    <div class="col-lg-1 btn-myapp pointer" data-controller="ordine-dettaglio" data-website="sticasa" data-id="'.$o['id_ordine_summary'].'" data-summary="'.$o['id_ordine_summary'].'" data-status="'.$o['bl_stato'].'"  style="margin-left:-15px;margin-right:15px">
      <span class="info-box-icon bg-'.$colore.'" style="font-size:1.2em"><strong>'.$ordinenr[0].'</strong></span>

    </div>
    <div class="col-lg-6 text-left btn-myapp pointer" data-controller="ordine-dettaglio" data-website="sticasa" data-id="'.$o['id_ordine_summary'].'" data-summary="'.$o['id_ordine_summary'].'" data-status="'.$o['bl_stato'].'" style="margin-right:0px">
      <span>'.$o['data_ordine'].'</span>
      <span class="label label-'.$palette.'">'.$stato.'</span>

      <span class="progress-description">
      '.$cliente.'
      <small>'.$spedisci_a.'</small>
    </div>

    <div class="col-lg-2 pull-right" style="margin:0 auto;height:90px;padding-top:20px;margin-right:5px">
      <button class="btn btn-primary btn-stato-ordine" data-controller="stato-ordine" data-status="'.$o['bl_stato'].'" data-ordine="'.$o['ac_nrordine'].'" data-id="'.$o['id_ordine_summary'].'" data-cliente="'.$o['idregistrazione'].'" style="min-width:150px">'.$status['button'].'</button>
      <br><br>
      <a href="#" class="pull-right elimina-ordine" data-id="'.$o['id_ordine_summary'].'" data-ordine="'.$o['ac_nrordine'].'"><span class="fa fa-trash"></span> <small>Elimina</small></a>
    </div>
    <div class="col-lg-2 pull-right" style="height:90px">
      <span style="margin-top:-20px;font-size:1.1em;">
          EUR '.number_format($o['ac_totale'],2).'<br>
          <span class="badge">'.$o['ac_pagamento'].'</span>
          <br>
          <span><span class="fa fa-truck"></span> '.number_format($o['ac_spesespedizione'],2).'</span><br>
          '.$sconto.'
      </span>
    </div>
    <div class="row ordine_dettaglio" id="ordine_'.$o['id_ordine_summary'].'" style="margin-top:-20px;margin-bottom:10px;">

    </div>
    </div>

    <div class="clearfix" style="margin-bottom:10px"></div>
    ';
    if ( $ricerca ){
      echo '
        </div>
        <div class="clearfix" style="margin-bottom:10px"></div>
      ';
      include_once('ordini_scelta_fornitore.php');
    }
  }

  */
?>

<script>
$(document).ready(function(){

  $('.btn-myapp').on('click',function(){
    var target = $('#ordine_' + $(this).data('id') );
    var id = $(this).data('id');
    $.post ( 'ajax/ordini' ,
      {
        action: $(this).data('controller'),
        ordine: $(this).data('id')
      }, function ( result ){
        $('.ordine_dettaglio').addClass('hide');
        target.removeClass('hide');
        target.html(result);
        target.show();
      }
    );
  })


})
</script>
