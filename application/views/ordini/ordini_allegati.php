<div class="row">
  <div class="col-lg-12">
    <table class="table table-striped table-bordered">
      <thead>
        <th>ID</th>
        <th>Ordine</th>
        <th>Allegati</th>
      </thead>
      <tbody>
        <?php
        $app = $this->config->item('application');
        foreach ( $allegati AS $allegato ){
          echo '
            <tr>
              <td>'.$allegato['id_allegato'].'</td>
              <td>'.$allegato['ac_nrordine'].'</td>
              <td><a href="'.$app['ordini_allegati_uri'].''.$allegato['ac_allegato'].'" target="_blank"><span class="fa fa-download"></span> '.$allegato['ac_allegato'].'</td>
            </tr>
          ';
        }
        ?>
        <tr>
          <td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
