<div class="modal fade in" tabindex="-1" id="pagamentoModal" role="dialog">
    <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
    <h1>Pagamento</h1>
    </div>

    <div class="modal-body form-inline">
    <table class="table">
    <tbody><tr>
    <td><label>Pagamento</label></td>
    <td>
    <select class="form-control pagamento" data-id="" data-ordine="">
      <option value="">...</option>
      <option value="BONIFICO">BONIFICO</option>
      <option value="PAYPAL">PAYPAL</option>
    </select></td>
    </tr>
    </tbody></table>
    </div>
    <div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-danger" >Annulla</button>
    </div>
    </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div>
