<?php
  $app = $this->config->item('application');
  foreach ( $ordini AS $o ){

    $stati = $app['status']['stato'];
    $status = '';
    //$os = $o['bl_stato'];
    $os = -1;
    if ( $os < 0 ){
      $os = 0;
    }
    $status = $stati[$os];
    echo $o['id_presente'];
    //$stato = $status['name'];
    $stato = 'SOSPESO';
    $inviaSub = '';
    if ( $os == 3 ){
      if ( !$o['bl_subfornitore'] ){
        $inviaSub = '<label class="badge pointer invia-sub-fornitore sub-fornitore_'.$o['id_ordine_summary'].'" data-ordine="'.$o['ac_nrordine'].'" data-id="'.$o['id_ordine_summary'].'" style="margin-top:5px">Inviato Sub fornitore</label>';
      } else {
        $inviaSub = '<label class="badge pointer label-danger sub-fornitore_'.$o['id_ordine_summary'].'" data-ordine="'.$o['ac_nrordine'].'" data-id="'.$o['id_ordine_summary'].'" style="margin-top:5px">Subfornitore</label>';
      }
    }
    $colore = $status['colore'];
    $palette = $status['palette'];
    $ordinenr = explode('/',$o['ac_nrordine']);
    $allegato = '';
    if ( $o['ac_allegato']!= '' ){
      $allegato = '<a href="'.$app['ordini_allegati_uri'].''.$o['ac_allegato'].'" class="label label-danger" target="_blank"><span class="fa fa-download"></span> Allegati</a>';
    }
    $totale = number_format((float)$o['ac_totale']+(float)$o['ac_spesespedizione'],2);
    $sconto = '';
    if ( (float)$o['ac_sconto'] > 0 ){
      $coupon_usato = $o['coupon_usato'];
      $sconto = '<small><span class="fa fa-money"></span><span> -'.number_format($o['ac_sconto'],2).' ('.$coupon_usato.')</span></small>';
    }

    $cliente = '<strong>'
    .strtoupper($o['ac_nome']).' '
    .strtoupper($o['ac_cognome']).'</strong><br>'
    .strtoupper($o['ac_indirizzo']).' '
    .strtoupper($o['ac_cap']).' '
    .strtoupper($o['ac_citta']).' '
    .strtoupper($o['ac_pv']).'
    <span class="fa fa-phone"></span>'
    .$o['ac_telefono'].' '.$o['ac_cellulare'].'</span>';

    $spedisci_a = '';
    if ( $o['bl_delivery_delivery'] == 1 ){
      $spedisci_a = '<span style="color:red">'.strtoupper($o['ac_firstname_delivery']).' '.strtoupper($o['ac_lastname_delivery']).' - ' .strtoupper($o['ac_address_delivery']).' '  .strtoupper($o['ac_zip_delivery']).' '
      .strtoupper($o['ac_city_delivery']).' '
      .strtoupper($o['ac_state_delivery']).'
      <span class="fa fa-phone"></span> '
      .$o['ac_phone_delivery'].' '.$o['ac_mobile_delivery'].'</span></span>';
    }

    echo '
    <div class="row riga_'.$o['id_ordine_summary'].' status_'.$o['id_ordine_summary'].'_'.$o['bl_stato'].'" style="margin-left:0px;padding:0px;max-height:90px;width:100%;background:#fff">
    <div class="col-lg-1 btn-myapp pointer" data-controller="ordine-recupero-dettaglio" data-website="sticasa" data-id="'.$o['id_ordine_summary'].'" data-summary="'.$o['id_ordine_summary'].'" data-status="'.$o['bl_stato'].'"  style="margin-left:-15px;margin-right:15px">
      <span class="info-box-icon bg-'.$colore.'" style="font-size:1.2em"><strong>'.$ordinenr[0].'</strong></span>

    </div>
    <div class="col-lg-6 text-left btn-myapp pointer" data-controller="ordine-dettaglio" data-website="sticasa" data-id="'.$o['id_ordine_summary'].'" data-summary="'.$o['id_ordine_summary'].'" data-status="'.$o['bl_stato'].'" style="margin-right:0px">
      <span>'.$o['data_ordine'].'</span>
      <span class="label label-'.$palette.'">'.$stato.'</span>

      <span class="progress-description">
      '.$cliente.'
      <small>'.$spedisci_a.'</small>
    </div>

    <div class="col-lg-2 pull-right" style="margin:0 auto;height:90px;padding-top:20px;margin-right:5px">
      <button class="btn btn-danger btn-recupera-ordine" data-status="'.$o['bl_stato'].'" data-ordine="'.$o['ac_nrordine'].'" data-id="'.$o['id_ordine_summary'].'" data-cliente="'.$o['idregistrazione'].'" style="min-width:150px;margin-top:-10px;">RECUPERA</button>
      <br>
      '.$inviaSub.'<br>
      <a href="#" style="margin-top:10px;" class="elimina-ordine" data-id="'.$o['id_ordine_summary'].'" data-ordine="'.$o['ac_nrordine'].'"><span class="fa fa-trash"></span> <small>Elimina</small></a>
    </div>
    <div class="col-lg-2 pull-right" style="height:90px">
      <span style="margin-top:-20px;font-size:1.1em;">
          EUR '.number_format($o['ac_totale'],2).'<br>
          <span class="badge">'.$o['ac_pagamento'].'</span>
          <br>
          <span><span class="fa fa-truck"></span> '.number_format((float)$o['ac_spesespedizione'],2).'</span><br>
          '.$sconto.'
      </span>
    </div>
    <div class="row ordine_dettaglio" id="ordine_'.$o['id_ordine_summary'].'" style="margin-top:-20px;margin-bottom:10px;">

    </div>
    </div>

    <div class="clearfix" style="margin-bottom:10px"></div>
    ';

  }
  include_once('ordini_scelta_pagamento.php');

  $allegati_uri = $app['ordini_allegati_uri'];
?>

<script>
$(document).ready(function(){

  $('.btn-myapp').on('click',function(){
    var target = $('#ordine_' + $(this).data('id') );
    var id = $(this).data('id');
    $.post ( 'ajax/ordini' ,
      {
        action: $(this).data('controller'),
        id: $(this).data('id')
      }, function ( result ){
        $('.ordine_dettaglio').addClass('hide');
        target.removeClass('hide');
        target.html(result);
        target.show();
      }
    );
  })

  $('.btn-recupera-ordine').on('click',function(){
    target = $(".riga_" + $(this).data('id'));
    var id = $(this).data('id')
    var ordine = $(this).data('ordine')
    $('.pagamento').attr('data-id',id)
    $('.pagamento').attr('data-ordine',ordine)
    $('#pagamentoModal').modal('show');
  })

  $('.pagamento').on('change',function(){
    if ( $(this).val() != '' ){
      $('#pagamentoModal').modal('hide');
      recupera_ordine();
    }
  })

  function recupera_ordine(){
    var id = $('.pagamento').data('id')
    var ordine = $('.pagamento').data('ordine')
    $.post ( 'ajax/ordini' ,
      {
        action: 'recupera-ordine',
        id: id,
        ordine: ordine,
        pagamento: $('.pagamento').val()
      }, function ( result ){
        $('.riga_' + id ).remove();
        doNotification('Recupero Ordine','L\'ordine ' + ordine + 'e\' stato recuperato.');
      }
    )
  }


  $('.elimina-ordine').on('click',function(){
    var ordine = $(this).data('ordine');
    var id = $(this).data('id');
    var conferma = window.confirm("Confermi cancellazione ordine " + ordine);
    if ( conferma ){
    $.post ( 'ajax/ordini',
      {
        action: 'ordine-temporaneo-elimina',
        ordine: ordine
      }, function(result){
        $('.riga_' + id ).remove();
        doNotification('Ordini','Ordine eliminato');
      }
    )
    }
  })

  $('.btn-allega-file').on('click',function(){
    $('#allegatoModal').modal('show');
    $('.btn-allegato-salva').attr('data-ordine',$(this).data('ordine'));
  })

  $('.btn-allegato-salva').on('click',function(){
    console.log ( $(this).data('ordine') , $(this).data('file'))
    var allegato = $(this).data('file');
    var allegato_uri = $('.allegato_uri').val();
    var ordine = $(this).data('ordine');
    $.post ( 'ajax/ordini' ,
      {
        action:'ordini-salva-allegato',
        ordine: $(this).data('ordine'),
        file: $(this).data('file')
      },function(result){
        doNotification('Allegato Ordine','Allegato salvato correttametne')
        $('#allegatoModal').modal('hide');
        $('.allegato-' + ordine).html('<a class="label label-danger" href="' + allegato_uri + allegato + '" target="_blank"><span class="fa fa-download"></span> Allegati</a>');
      }
    )
  })


})
</script>
