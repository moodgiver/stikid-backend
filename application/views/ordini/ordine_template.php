<body style="background:#fff">
<table style="border:1px black solid" cellpadding="10" cellspacing="4" width="100%">
  <thead>
  <tr>
    <th colspan="8" style="background:#eaeaea;color:black">
    STICASA/STIKID ORDINE NR {ordine_nr}
    </th>
  </tr>
  <tr style="background:#eaeaea;color:black">
    <th>
    Immagine
    </th>
    <th>
    Codice
    </th>
    <th>
    Prodotto
    </th>
    <th>
    Colore
    </th>
    <th>
    L cm
    </th>
    <th>
    H cm
    </th>
    <th>
    Q.t&agrave;
    </th>
    <th>
    Vettoriale
    </th>
  </tr>
  </thead>
  <tbody>
    {ordine_entries}
      <tr>
        <td>
          <img src="http:{image}"/>
        </td>
        <td>
          {ac_codice_prodotto}
        </td>
        <td>
          {ac_prodotto}
        </td>
        <td>
          {ac_colore}
        </td>
        <td>
          {ac_width}
        </td>
        <td>
          {ac_height}
        </td>
        <td>
          {ac_qty}
        </td>
        <td>
          <a href="http:{uri_eps}">{ac_sticker}</a>
        </td>
    {/ordine_entries}
  </tbody>
</table>
