<div class="modal fade in" tabindex="-1" id="fornitoriModal" role="dialog">
    <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
    <h1>Invia a Fornitore</h1>
    </div>

    <div class="modal-body form-inline">
    <table class="table">
    <tbody><tr>
    <td><label>Fornitore</label></td>
    <td>
    <select class="form-control fornitore_id">
    <?php
        foreach ( $fornitori AS $fo ){
          if ( $fo['bl_attivo'] ){
            echo '<option value="'.$fo['id_utente'].'">'.$fo['ac_cognome'].'</option>';
          }
        }
     ?>
    </select></td>
    </tr>
    </tbody></table>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-success btn-update-fornitore" data-id="" data-ordine="">Invia</button>
    </div>
    </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div>

  <script>
  $(document).ready ( function() {
    $('.btn-update-fornitore').on('click',function(){
      $('#fornitoriModal').modal('hide');
      $('.waitEnd').removeClass('hide');
      $.post ( 'ajax/ordini' ,
        {
          action: 'ordine-aggiorna-fornitore',
          id: $(this).data('id'),
          ordine: $(this).data('ordine'),
          fornitore: $('.fornitore_id').val()
        } , function(result){
          $('.waitEnd').addClass('hide');
          doNotification('Gestione Ordini','Ordine inviato al fornitore')
        }
      )
    })
  })
  </script>
