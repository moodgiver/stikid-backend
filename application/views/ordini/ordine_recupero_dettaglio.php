
<div class="col-lg-12 col-md-12">
  <table width="100%" class="table table-bordered table-striped whiteBG">
    <thead>
      <tr>
        <th>Prodotto</th>
        <th>Descrizione</th>
        <th>Dimensioni LxA cm</th>
        <th>Colore</th>
        <th>Q.ta</th>
        <th class="text-right">Prezzo &euro;</th>
      </tr>
    </thead>
    <tbody>
<?php

  foreach ( $ordine AS $row ){
    $img = imguri().'images/ambienti/small/'.$row['ac_immagine'];
    $tipo = 'Adesivo Murale';
    $misure = $row['ac_width'].'x'.$row['ac_height'];
    if ( $row['ac_telaio'] != '' ){
      $img = imguri().'images/canvas/preview/'.$row['ac_immagini_demo'];
      $tipo = 'Quadro per bambino';
      $misure = $misure.' telaio cm.: '.$row['ac_telaio'];
    }
    echo '
    <tr>
      <td>
        <img src="'.$row['uri_img'].'" width="80" height="80" border="0" />
      </td>
      <td>
      Codice: <strong>'.$row['ac_codice_prodotto'].'</strong><br>
      Prodotto: <strong>'.$row['ac_prodotto'].'</strong><br>
      <em>'.$tipo.'</em>
      <br>
      Vettoriale [<a href="'.imguri().'images/vettoriali/'.$row['ac_sticker'].'">'.$row['ac_sticker'].'</a>]
      </td>
      <td>
      '.$misure.'<br>
      Fornitore => <strong>'.$row['fornitore'].'</strong>
      </td>
      <td>
        '.$row['ac_colore'].'
      </td>
      <td>
        '.$row['ac_qty'].'
      </td>
      <td class="text-right">
        '.number_format((float)$row['ac_totale'],2).'
      </td>
    </tr>
    ';
  }
?>
  </tbody>
</table>

</div>
