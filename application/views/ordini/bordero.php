
<table width="100%" cellpadding="4" cellspacing="0" border="1">
	<tr>
		<td colspan="7"><br>
		<strong style="font-size:16px">BORDERO SPEDIZIONI DUAL (STICASA)</strong><br>
		</td>
	</tr>
	<tr>
		<td><strong>BDA</strong></td>
		<td><strong>Data</strong></td>
		<td><strong>Cliente</strong></td>
		<td><strong>Data Presa</strong></td>
		<td><strong>NOTE</strong></td>
		<td><strong>Controllo</strong></td>
	</tr>
  <?php
    foreach ( $bordero AS $riga ){

      echo '
        <tr id="intestazione_#n#">
          <td style="border-top:1px solid black" valign="top">
          '.$riga["ac_nrordine"].'
          </td>
          <td valign="top">
            '.date('d-m-Y').'
          </td>
          <td valign="top">
          ';
      if ( $riga["extra_delivery"] == 0 ){
      echo '<strong>'
        .strtoupper($riga["ac_nome"]).'  '
        .strtoupper($riga["ac_cognome"]).' </strong><br>'
        .$riga["ac_indirizzo"].'<br>'
        .$riga["ac_cap"].' '
        .strtoupper($riga["ac_citta"]).' '
        .strtoupper($riga["ac_pv"]).' '
        .$riga["ac_nazione"].'<br>'
        .$riga["ac_email"].' - '
        .$riga["ac_telefono"].' - '
        .$riga["ac_cellulare"].'<br>';

      } else {
        echo '
        <strong>'.strtoupper($riga["nome_d"]).'  '.strtoupper($riga["cognome_d"]).' </strong><br>
        '.$riga["company_d"].'<br>
        '.$riga["address_d"].'<br>
        '.$riga["zip_d"].' '.strtoupper($riga["city_d"]).' '.strtoupper($riga["state_d"]).' '.$riga["country_d"].'<br>
        '.$riga["phone_d"].' - '.$riga["mobile_d"];

    }
    echo '</td>
    <td align="center"><div style="border:1px solid black ; width:20px ; height:20px">&nbsp;</div></td><td></td><td></td>
    </tr>
    ';
   }
  ?>
</table>
