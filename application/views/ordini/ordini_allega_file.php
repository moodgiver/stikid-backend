
<div class="modal fade in" tabindex="-1" id="allegatoModal" role="dialog">
  <div class="modal-dialog" style="width:50%">
    <div class="modal-header" style="background:#fff;">
      <h2>Allega documento</h2>

    <div class="modal-content">
      <div class="jumbotron" style="250px;margin:0 auto;">
        <form action="<?php echo base_url();?>upload/temp" id="myDropzone" class="dropzone">
          <input type="hidden" name="folder" id="folder" value="<?=$app['temp_image_path']?>">
        </form>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-warning"  data-dismiss="modal">Chiudi</button>
      <button type="button" class="btn btn-success btn-allegato-salva" data-ordine="" data-file="">Salva</button>
    </div>
  </div>
  </div>
</div>
<style>
#myDropzone {
  height: 200px;
  background: #fff;
  margin: 10px;
  padding: 20px;
  border: 6px dashed #999;
}

.dz-message { text-align:center; color:#ababab; }
</style>
<script>
$(document).ready(function(){

  Dropzone.autoDiscover = false;
    $("#myDropzone").dropzone({
    dictDefaultMessage: "Clicca qui o trascina il tuo file qui per caricarlo",
    clickable: true,
    maxFilesize: 10,
		autoProcessQueue: true,
    uploadMultiple: false,
    addRemoveLinks: false,
		createImageThumbnails: true,
		acceptedFiles: 'application/pdf',
		dictFileTooBig: 'Documento superiore a 10 MB. Impossibile caricare',
				init: function () {
          myDropzone = this;
					$('.dz-preview').remove();

          this.on("success", function(file) {
            $('.fa-spinner').addClass('hide');
            $('.dz-error-message').html('');
					  $('.dz-success-mark').html('<span class="label label-success">File caricato con successo!</span>');
            $('.dz-error-mark').hide();
            $('.btn-allegato-salva').attr('data-file',file['name']);
	        });

				  this.on("error" , function ( e , errorMsg ){
            $('.dz-error-message').removeClass('hide');
						$('.dz-error-message').html(errorMsg);
						$('.dz-details').html('');
					});

          this.on("addedfile" , function(){
            console.log ( 'added file ');
            $('.dz-error-message').addClass('hide');
            $('.fa-spinner').removeClass('hide');
          });

          this.on("uploadprogress" , function( file , progress ){
            $('.progress-uploaded').css ( 'width' , parseInt(progress) + '%' );
            $('.percentage').html ( parseInt(progress) + ' %' );
          });
      }
    });

})
</script>
