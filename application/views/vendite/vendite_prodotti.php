<?php
  include_once('date_range.php');
?>

<div class="row">
  <div class="col-lg-12">
  <table class="table table-bordered table-striped">
    <thead>
      <th>Prodotto</th>
      <th>Collezione</th>
      <th>Designer</th>
      <th>Venduto</th>
      <th>Azioni</th>
    </thead>
    <tbody>
      <?php
        foreach ( $vendite AS $row ){
          $cat = $row['ac_categoria'];
          if ( $row['ac_categoria_lang'] != ''){
            $cat = $row['ac_categoria_lang'];
          }
          echo '
          <tr>
            <td>'.$row['ac_prodotto'].'</td>
            <td>'.$cat.'</td>
            <td>'.$row['designer'].'</td>
            <td>'.$row['venduti'].'</td>
            <td></td>
          </tr>
          ';
        }
      ?>
    </tbody>
  </table>
  </div>
</div>
      <!--
      <td><a href="#" style="font-size:1.2em;color:#888;" class="btn-action" data-controller="edit-product" data-id="#id_prodotto#" data-website="#form.sito#" data-prodotto="#ac_prodotto# - #ac_codice_prodotto#" title="Modifica prodotto"><button class="btn btn-primary"><span class="fa fa-edit"></span></button></td>-->
