<?php
  include_once('date_range.php');
?>
<div class="row">
  <div class="col-lg-12">
    <table class="table table-bordered table-striped">
      <thead>
        <th>Cliente</th>
        <th>Email</th>
        <th>Ordini</th>
        <th></th>
      </thead>
      <tbody>
        <?php
          foreach ( $vendite AS $row ){
            echo '<tr>
            <td>'.$row['ac_cognome'].' '.$row['ac_nome'].'</td>
            <td>'.$row['ac_email'].'</td>
            <td>'.$row['ordini'].'</td>
            <td><button class="btn btn-primary btn-action btn-open-modal-coupon" data-controller="customer-promo-coupon" data-customer="'.$row['ac_cognome'].' '.$row['ac_nome'].'" data-id="'.$row['idregistrazione'].'" data-email="'.$row['ac_email'].'"><span class="fa fa-envelope" title="Crea Email Coupon"></span> </button></td>
            </tr>
            ';

          }
         ?>
      </tbody>
    </table>
  </div>
</div>


<div class="modal modal-email-coupon">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">�</span></button>
                <h4 class="modal-title">Crea Coupon personalizzato</h4>
            </div>
            <div class="modal-body" style="display:inline-block">
                <div class="col-lg-12">
                <label>Oggetto Email invio coupon personale</label>
                <input type="text" class="form-control email-coupon-subject" value="Coupon STIKID/STICASA">
                <label>Testo email</label>
                <textarea class="form-control email-coupon-testo">Gentile Cliente, vogliamo premiare la sua fedelt� di acquisto dei nostri prodotti, inviandole un coupon personalizzato. Potr� utilizzare questo coupon fino al </textarea><br>
                </div>

                <div class="col-lg-4">
                <label>Codice Coupon</label>
                <input type="text" class="form-control email-coupon-codice" placeholder="codice coupon">
                </div>

                <div class="col-lg-2">
                <label>Sconto %</label>
                <input type="text" class="form-control email-coupon-sconto-percentuale" placeholder="10">
                </div>

                <div class="col-lg-2">
                <label>Sconto &euro;</label>
                <input type="text" class="form-control email-coupon-sconto-euro" placeholder="10">
                </div>

                <div class="col-lg-4">
                <label>Valido fino al</label>
                <input type="text" class="form-control email-coupon-scade" placeholder="dd/mm/yyyy">
                </div>
                <br><br>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Chiudi</button>
                <button type="button" class="btn btn-primary btn-action btn-save-prodotto" data-controller="save-product" data-id="">Salva</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<script>
$(document).ready(function(){
    $('.btn-open-modal-coupon').click(function(){
        $(".modal-title").html("Crea Coupon personalizzato - " + $(this).data("customer") );
        $(".modal-email-coupon").modal("show");
    });
});
</script>
