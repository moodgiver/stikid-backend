
<?php
  $n = 0;
  $areaDiff = 0;
  $prezzoDiff = 0;
  $startingPrice = $prezzi[0]['ac_prezzo_reale'];
  $cs = 1;
  if ( !isset($_POST['w']) ){
    $_POST['w'] = 50;
    $_POST['h'] = 50;
  }
  foreach ( $prezzi AS $row ){
    $area_minima = (int)($_POST['w']*$_POST['h'])/100;
    if ( (float)$area_minima <= (float)$row['ac_area'] ){
      //echo $area_minima .'=>'.$row['ac_area'].'=>'.$prezzi[$n-1]['ac_prezzo_reale'];
      $areaBase = $prezzi[$n-1]['ac_area'];
      $areaDiff = $area_minima-$areaBase;
      //echo $areaBase;
      $nextArea = $row['ac_area'];
      $prezzoBase = $prezzi[$n-1]['ac_prezzo_reale'];
      $nextPrezzo = $row['ac_prezzo'];
      $prezzoDiff = $row['ac_prezzo']*$areaDiff/100;
      $prezzoFinale = (int)($prezzoBase + $prezzoDiff);
      //$prezzoFinale = calcolo_prezzo($_POST['w'],$_POST['h'],1);
      $costo_calcolato = ($area_minima/100)*13;
      $costo = $costo_calcolato;
      if ( $costo < 7.5 ){
        $costo = 7.5;
      }
      break;
    }
    $n++;
  }
?>

<div class="row">
  <div class="col-lg-4">
    <table class="table table-bordered table-striped" width="100%">
      <tr>
        <th>Area</th>
        <th>Prezzo MQ</th>
        <th>Prezzo BASE Sticker</th>
      </tr>
      <?php
      foreach ( $prezzi AS $row ){
        echo '
        <tr class="row_'.$row['ac_area'].'">
        <td>'.$row['ac_area'].'</td>
        <td>'.$row['ac_prezzo'].'</td>
        <td>'.$row['ac_prezzo_reale'].'</td>
        </tr>
        ';
      }
      ?>
    </table>
  </div>
  <div class="col-lg-6">
    <h3 class="label label-primary">Inserire dimensioni sticker</h3>
    <form action="#script_name#" method="post">
      <div class="col-lg-6">
        <label>Larghezza</label>
        <input type="text" class="form-control w" name="w" value="<?=$_POST['w']?>">
      </div>
      <div class="col-lg-6">
        <label>Altezza</label>
        <input type="text" class="form-control h" name="h" value="<?=$_POST['h']?>">
      </div>
      <div class="col-lg-12 text-right">
        <button class="btn btn-default btn-calcolo-mol">Calcola</button>
      </div>
    </form>
    <div class="clearfix"></div>
    <div class="output-simulatore" data-row="<?=$areaBase?>" data-diff="<?=$nextArea?>" style="padding:20px">
      <table class="table table-bordered table-striped">
        <tr>
                    <td>AREA</td>
                    <td><strong><?=$area_minima?></strong></td>
                </tr>

                <tr>
                    <td>AREA DIFF</td>
                    <td><strong><?=$areaDiff?></strong></td>
                </tr>
                 <tr>
                    <td>PREZZO BASE STICKER</td>
                    <td><strong>&euro; <?=number_format($prezzoBase,2)?></strong> ( Area Calcolata = <?=$areaBase?> )</td>
                </tr>
                <tr>
                    <td>PREZZO DIFF</td>
                    <td>
                    <strong>&euro; <?=number_format($prezzoDiff,2)?></strong> (<?=($nextPrezzo *$areaDiff)/100?>)
            <small>(Prezzo mq*area diff)/100</small></td>
                </tr>
            </table>

            <div class="col-lg-6 prezzo_finale"><span class="label label-success" style="font-size:1.7em;margin-top:25px">PREZZO &euro;  <?=number_format($prezzoFinale,2)?></span></div>
            <div class="col-lg-6 costo">
            <span class="label label-danger" style="font-size:1.7em;margin-top:25px">COSTO &euro; <?=number_format($costo,2)?> (<?=number_format($costo,2)?>)</span>
            </div>
            <div class="col-lg-12">
            <h2>&nbsp;</h2>
            </div>
    <div class="col-lg-8" style="padding-top:5px">
      a. <strong>Prezzo (-IVA 22%)</strong>
    </div>
    <div class="col-lg-4">
      <input class="form-control prezzo-vendita" placeholder="0" value="<?=number_format($prezzoFinale/1.22,2)?>" readonly>
    </div>
    <div class="col-lg-8" style="padding-top:5px">
      b. <strong>Costo FTF</strong>
    </div>
    <div class="col-lg-4">
      <input class="form-control costo-fornitore" placeholder="0" value="<?=number_format($costo,2)?>" readonly>
    </div>
    <div class="col-lg-8" style="padding-top:5px">
      c. <strong>Costo GLS</strong>
    </div>
    <div class="col-lg-4">
      <input class="form-control costo-shipping" placeholder="0">
    </div>
    <div class="col-lg-8" style="padding-top:5px">
      d. <strong>Costo tubo</strong>
    </div>
    <div class="col-lg-4">
      <input class="form-control costo-tubo" placeholder="0" value="0">
    </div>
    <div class="col-lg-8" style="padding-top:5px">
      e. <strong>Costo Designer</strong>
    </div>
    <div class="col-lg-4">
      <input class="form-control costo-designer" placeholder="0" value="0">
    </div>
    <div class="col-lg-6" style="padding-top:5px">
      f. <strong>Affiliazione %</strong>
    </div>
    <div class="col-lg-2">
      <input class="form-control affiliazione" style="width:50px" value="0">
    </div>
    <div class="col-lg-4">
      <input class="form-control costo-affiliazione" placeholder="0" value="0">
    </div>
    <div class="col-lg-6" style="padding-top:5px">
      g. <strong>Sconto Coupon % su <?=number_format($prezzoFinale,2)?></strong>
    </div>
    <div class="col-lg-2"><input class="form-control coupon"  style="width:50px" value="0">
    </div>
    <div class="col-lg-4">
      <input class="form-control costo-coupon" placeholder="0" value="0">
    </div>
    <div class="col-lg-8" style="padding-top:5px">
      h. <strong>PAYPAL 3.4% <?=number_format($prezzoFinale,2)?> + 0.35</strong>
    </div>
    <div class="col-lg-4">
      <input class="form-control costo-paypal" placeholder="0" value="<?=number_format((($prezzoFinale*3.4)/100)+0.35,2)?>" readonly>
    </div>
    <div class="col-lg-8" style="padding-top:5px">
      h. <strong>Marketplace</strong>
    </div>
    <div class="col-lg-4">
      <input class="form-control costo-marketplace" placeholder="0" value="0">
    </div>
    <div class="clearfix" style="margin-bottom:30px"></div>
    <div class="col-lg-8" style="padding-top:5px">
      <strong class="label label-success" style="font-size:1.5em">MOL</strong>
      <h4>Marginalit&agrave;: <span class="marginalita"> <?=number_format(100-(costo/prezzoFinale)*100,2)?>%</span></h4>
    </div>
    <div class="col-lg-4">
      <input class="form-control success mol-calcolato" placeholder="0" style="font-size:1.4em;background:##99ff99;" readonly>
    </div>
        </div>
   </div>

   </cfoutput>
</div>

<script>
  $(document).ready(function(){
      var riga = $(".output-simulatore").data("row");
      var diff = $(".output-simulatore").data("diff");
      $(".row_" + riga).css("background","#c6ffc6");
      $(".row_" + diff).css("background","#ffb9b9");
      $('.title-header').html ( 'Simulatore Costi/Prezzi' );
      $('.title-description').html('');

  var mol = {
    "prezzo_vendita" : <?=number_format($prezzoFinale,2)?>,
    "prezzo" : parseFloat($('.prezzo-vendita').val()),
    "fornitore": <?=number_format($costo,2)?>,
    "shipping": 0,
    "tubo": 0,
    "designer" : 0,
    "affiliazione" : 0,
    "coupon" : 0,
    "paypal" : parseFloat($('.costo-paypal').val()),
    "marketplace" : 0,
    "calcolo" : 0
  }

  calculateMOL();

  $('.costo-shipping').keyup(function(){
    if ( $.isNumeric($(this).val()) ){
      mol.shipping = parseFloat($(this).val());
      calculateMOL();
    }
  });

  $('.costo-tubo').keyup(function(){
    if ( $.isNumeric($(this).val()) ){
      mol.tubo = parseFloat($(this).val());
      calculateMOL();
    }
  });

  $('.costo-designer').keyup(function(){
    if ( $.isNumeric($(this).val()) ){
      mol.designer = parseFloat($(this).val());
      calculateMOL();
    }
  });

  $('.affiliazione').keyup(function(){
    console.log ( $(this).val() );
    if ( $.isNumeric($(this).val()) ){
      var ca = parseFloat((mol.prezzo*parseFloat($(this).val()))/100);
      $('.costo-affiliazione').val ( ca.toFixed(2) );
      mol.affiliazione = parseFloat(ca);
      calculateMOL();
    }
  });
  $('.coupon').keyup(function(){
    console.log ( $(this).val() );
    if ( $.isNumeric($(this).val()) ){
      var co = parseFloat((mol.prezzo_vendita*parseFloat($(this).val()))/100);
      $('.costo-coupon').val ( co.toFixed(2) );
      mol.coupon = parseFloat(co);
      calculateMOL();
    }
  });

  $('.costo-marketplace').keyup(function(){
    if ( $.isNumeric($(this).val()) ){
      mol.marketplace = parseFloat($(this).val());
      calculateMOL();
    }
  });

  function calculateMOL(){
    mol.prezzo = parseFloat($('.prezzo-vendita').val());
    var calcolo = parseFloat(mol.prezzo - mol.fornitore - mol.shipping - mol.tubo - mol.designer - mol.affiliazione - mol.coupon - mol.paypal - mol.marketplace );
    mol.totale = calcolo;
    $('.mol-calcolato').val ( mol.totale.toFixed(2) );

    var margine = parseFloat((calcolo/mol.prezzo)*100);
    $('.marginalita').html ( margine.toFixed(2) + '%');
  }

  $('.btn-calcolo-mol').on('click',function(e){
    e.preventDefault();
    $.post ( 'ajax/vendite' ,
      {
        action: 'costi-simulatore',
        w: $('.w').val(),
        h: $('.h').val()
      }, function(result){
        $('.content').html(result);
      }
    )
  });

});
</script>
