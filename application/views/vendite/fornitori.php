<div class="col-lg-2">
  Mese <select class="form-control mese">

    <?php
    $mese_sel = '';
    $mesi = ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre'];
    if ( isset($_POST['mese']) ){
      $mese_sel = $_POST['mese'];
    }
    for ( $mese = 1 ; $mese < 13 ; $mese++ ){
      $selected = '';
      if ( $mese == $mese_sel ){
        $selected = 'selected';
      }
      echo '<option value="'.$mese.'" '.$selected.'>'.$mesi[$mese-1].'</option>';
    }
    ?>
  </select>
</div>

<div class="col-lg-2">
  Anno <select class="form-control anno">
    <?php
    $selected = '';
    $anno = '';
    if ( isset($_POST['anno']) ){
      $anno = $_POST['anno'];
    }
    for ( $a = 2016 ; $a < 2026 ; $a++ ){
      $selected = '';
      if ( $a == $anno ){
        $selected = 'selected';
      }
      echo '<option value="'.$a.'" '.$selected.'>'.$a.'</option>';
    }
    ?>
  </select>
</div>
<div class="col-lg-4">
  Fornitore
  <select class="form-control fornitore">
    <option value="">Fornitore ...</option>
    <?php
    $id = 0;
    if ( isset($_POST['id']) ){
      $id = $_POST['id'];
    }
    foreach ( $fornitori AS $fornitore ){
      $selected = '';
      if ( $id == $fornitore['id_utente'] ){
        $selected = 'selected';
      }
      echo '<option value="'.$fornitore['id_utente'].'" '.$selected.'>'.$fornitore['ac_cognome'].' '.$fornitore['ac_nome'].'</option>';
    }
    ?>
  </select>
</div>
<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
  <br>
  <button class="btn btn-primary btn-costo-fornitore">Vedi</button>
</div>

<script>
$(document).ready(function(){
  $('.btn-costo-fornitore').on('click',function(){
    $.post ( 'ajax/vendite' ,
      {
        action: 'vendite-costi-fornitore',
        anno: $('.anno').val(),
        mese: $('.mese').val(),
        id: $('.fornitore').val()
      }, function(result){
        $('.content').html(result);
      }
    )
  })
})
</script>
