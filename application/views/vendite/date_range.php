<?php
  $periodo_corrente = substr($from,6,2).'-'.substr($from,4,2).'-'.substr($from,0,4).' -  '.substr($to,6,2).'-'.substr($to,4,2).'-'.substr($to,0,4);
 ?>
<div class="col-lg-12 clearfix daterange">
  <div class="col-lg-1">Periodo</div>
  <div class="col-lg-5">
    <div class="input-group">
      <div class="input-group-addon">
        <i class="fa fa-calendar"></i>
      </div>
      <input type="text" class="form-control pull-right active date-range date-range-vendite" data-controller="vendite-<?=$mode?>" data-mode="vendite-<?=$mode?>" name="daterange" id="daterange" value="<?=$periodo_corrente?>">
    </div>
  </div>
  <?php
    if ( $mode == 'costi-fornitore' ){
      include_once('fornitori.php');
    }
  ?>
</div>
<div class="clearfix"></div>

<script>
$(document).ready(function(){
  $('#daterange').daterangepicker({
    "locale" : "it"
  });

  $('#daterange').on('apply.daterangepicker', function(ev, picker) {
  		console.log(picker.startDate.format('YYYY-MM-DD'));
  		console.log(picker.endDate.format('YYYY-MM-DD'));
      $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'))
      $.post ( 'ajax/vendite' ,
        {
          action: $(this).data('controller'),
          from: picker.startDate.format('YYYYMMDD'),
          to: picker.endDate.format('YYYYMMDD')
        }, function(result){
          $('.content').html ( result );
        }
      );
	});


});

</script>
