<?php
  include_once('fornitori.php');
?>

<table class="table table-bordered dataTable col-sm-10 whiteBG" role="grid" aria-describedby="example1_info">
  <thead>

    <tr role="row">
    <th class="sorting"aria-controls="example1"aria-label="Rendering engine: activate to sort column ascending">
    Data
    </th>
    <th class="sorting_asc"aria-controls="example1"	aria-label="Rendering engine: activate to sort column ascending">
    Ordini
    </th>
    <th>
    Prodotto
    </th>
    <th>
    Q.t&agrave;
    </th>

    <th>
    LxA
    </th>
    <th>
    Costo Totale &euro;
    </th>
    <th>
    Costo Imballaggio &euro;
    </th>
    <th class="text-right">
    Totale Ordine &euro;
    </th>
    <th class="right">
    % costo
    </th>
    </tr>
  </thead>
  <tbody>
    <?php
    if ( isset($_POST['id']) ){
      if ( count($costi) > 0 ){
        $qryCosti = $costi[0];
        $data['misura_minima'] = (float)$qryCosti['ac_valore_minimo']*100;
  			$data['costo_minimo'] = (float)$qryCosti['ac_costo_minimo'];
  			$data['costo'] = (float)$qryCosti['ac_costo'];
  			$data['costo_soglia'] = (float)$qryCosti['ac_costo_soglia'];
  			$data['imballaggi'] = [ (float)$qryCosti['ac_imballaggio_1'],(float)$qryCosti['ac_imballaggio_2'],(float)$qryCosti['ac_imballaggio_3']];
  			$data['imballaggi_costi'] = [ (float)$qryCosti['ac_costo_imballo_1'],(float)$qryCosti['ac_costo_imballo_2'],(float)$qryCosti['ac_costo_imballo_3']];
  			$data['ac_costo_quadri'] = (float)$qryCosti['ac_costo_quadri'];
  			$data['ac_costo'] = (float)$qryCosti['ac_costo'];
      }
      $totale_vendite = 0>
      $ordine = "">
      $totale_costi = 0>
      $totale_imballaggi = 0;
      $nrordine = '';
      foreach ( $vendite AS $row ){
        $width = $row['ac_width'];
        $height = $row['ac_height'];
        $bl_colore = $row['bl_colore'];
        $data['width'] = $width;
        $data['height'] = $height;
        $data['colore'] = $bl_colore;
        $costoSticker = number_format(calcolo_costo($data),2);
        $prezzo = number_format((float)$row['ac_qty']*(float)$row['ac_prezzo'],2);
        if ( $prezzo < 1 ){
          $prezzo = calcolo_prezzo($width,$height,$row['ac_coefficiente_sconto']);
        }
        $perc = number_format(($costoSticker/$prezzo)*100,2);
        $totale_vendite += $prezzo;
        $totale_costi += $costoSticker;
        $totale_imballaggi += 1;
        $ordine = '';
        if ( $row['ac_nrordine'] != $nrordine ){
          $ordine = $row['ac_nrordine'];
        }
        echo '
        <tr>
        <td>'.$row['dt_pagamento'].'</td>
        <td><strong>'.$ordine.'</strong></td>
        <td><strong>'.$row['ac_prodotto'].'</strong></td>
        <td class="text-center">'.$row['ac_qty'].'</td>
        <td class="text-right">'.$row['ac_width'].'x'.$row['ac_height'].'</td>
        <td class="text-right">'.$costoSticker.'</td>
        <td class="text-right">1.00</td>
        <td class="text-right">'.$prezzo.'</td>
        <td class="text-right">'.$perc.'%</td>
        </tr>
        ';
        $nrordine = $row['ac_nrordine'];
      }
      $perc = 0;
      if ( $totale_vendite > 0 ){
        $perc = number_format(($totale_costi/$totale_vendite)*100,2);
      }
      echo '
      <tfoot>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td class="text-right"><h4>Totale &euro;</h4></td>
          <td class="text-right"><h3 class="red">'.number_format($totale_costi,2).'</h3></td>
          <td class="text-right"><h3 class="red">'.number_format($totale_imballaggi,2).'</h3></td>
          <td class="text-right"><h3 class="green"><cfoutput>'.number_format($totale_vendite,2).'</cfoutput></h3></td>
          <td class="text-right"><h3 class="aqua">'.$perc.'%</h3></td>
        </tr>
      </tfoot>
      ';
    }
    ?>


    <!--
    <cfset totale_vendite = 0>
    <cfset ordine = "">
    <cfset totale_costi = 0>
    <cfset totale_imballaggi = 0>
    <cfloop query="qry">
      <cfscript>
      </cfscript>
      <cfif (costoSticker*ac_qty) LT form.costo_soglia>
        <cfset costoFornitore = DecimalFormat(form.costo_soglia)>
      <cfelse>
        <cfset costoFornitore = DecimalFormat(costoSticker*ac_qty)>
      </cfif>

      <cfset n = 1>
      <cfset myImballo = 0>
      <cfset costoImballo = 0>
      <cfset wMin = 0>
      <cfset nImballo = 0>
      <cfloop index="i" list="#form.imballaggi#" delimiters=";">
        <cfif wMin LTE i AND i NEQ 0>
          <cfset costoImballo = ListGetAt(form.imballaggi_costi,n,";")>
          <cfset myImballo = i>
          <cfset nImballo = nImballo + 1>
          <cfbreak>
        </cfif>
        <cfset n = n + 1>
      </cfloop>
      <cfif totale NEQ 0>
      <cfif ac_nrordine NEQ ordine>

      <cfelse>

        <tr>
          <td></td>
          <td></td>
          <td><strong>#ac_prodotto#</strong></td>
          <td class="text-center">#ac_qty#</td>
          <td class="text-right">#ac_width#x#ac_height#</td>
          <td class="text-right">#costoFornitore#</td>
          <td class="text-right">#DecimalFormat(costoImballo)#</td>
          <td class="text-right">#DecimalFormat(totale)#</td>
          <td class="text-right">#DecimalFormat((costoFornitore/totale)*100)#%</td>
        </tr>
      </cfif>

      <cfset ordine = ac_nrordine>
      <cfset totale_vendite = totale_vendite + totale>
      <cfset totale_costi = totale_costi + costoFornitore>
      <cfif nImballo EQ 1>
        <cfset totale_imballaggi = totale_imballaggi + costoImballo>
      </cfif></cfif>
    </cfloop>-->
  </tbody>
  <!--
  <tfoot>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td class="text-right"><h4>Totale &euro;</h4></td>
      <td class="text-right"><h3 class="red">#DecimalFormat(totale_costi)#</h3></td>
      <td class="text-right"><h3 class="red">#DecimalFormat(totale_imballaggi)#</h3></td>
      <td class="text-right"><h3 class="green"><cfoutput>#DecimalFormat(totale_vendite)#</cfoutput></h3></td>
      <td class="text-right"><h3 class="aqua">#DecimalFormat((totale_costi/totale_vendite)*100)#%</h3></td>
    </tr>
  </tfoot>
-->
</table>
