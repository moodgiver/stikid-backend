<?php
  include_once('date_range.php');
?>

<br><br>
<div class="row">
<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
  <table class="table table-striped table-bordered dataTable col-sm-10 whiteBG" role="grid" aria-describedby="example1_info">
		<thead>
			<tr role="row">
			<th class="sorting"aria-controls="example1"aria-label="Rendering engine: activate to sort column ascending">
			Data
			</th>
			<th class="sorting_asc"aria-controls="example1"	aria-label="Rendering engine: activate to sort column ascending">
			Ordini
			</th>
      <th class="text-right">
      Coupon
      </th>
			<th class="text-right">
			Totale &euro;
			</th>
			</tr>
		</thead>
		<tbody>
      <?php
        $totale_vendite = 0;
        $labels = [];
        $values = [];
        foreach ( $vendite AS $row ){
          $totale_vendite += (float)$row['totale'];
          array_push($labels,$row['dt_pagamento']);
          array_push($values,(float)$row['totale']);
          echo '
          <tr>
          <td>'.$row['dt_pagamento'].'</td>
          <td>'.$row['nr'].'</td>
          <td>'.number_format($row['sconto'],2).'</td>
          <td class="text-right"><h4>'.number_format($row["totale"],2).'</h4></td>
          </tr>
          ';
        }
      ?>
		</tbody>
		<tfoot>
			<tr>
				<td></td>
        <td></td>
				<td class="text-right"><h4>Totale &euro;</h4></td>
				<td class="text-right"><h3><?php echo number_format($totale_vendite,2);?></h3></td>
			</tr>
		</tfoot>
	</table>
</div>
<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 whiteBG">
		<h3 class="label label-primary">Analisi Vendite</h3>
		<div class="chart">
			<canvas id="areaChart" style="height:250px;"></canvas>
       	</div>
</div>
</div>
  <!-- ChartJS 1.0.1 -->
  <script src='<?=base_url()?>public/plugins/chartjs/Chart.min.js'></script>

  <script>

  $(document).ready (function(){

        //$(function () {

  	  	$('.daterange').removeClass('hide');

          /* ChartJS
           * -------
           * Here we will create a few charts using ChartJS
           */

          //--------------
          //- AREA CHART -
          //--------------

          // Get context with jQuery - using jQuery's .get() method.
          var areaChartCanvas = $("#areaChart").get(0).getContext("2d");
          // This will get the first returned node in the jQuery collection.
          var areaChart = new Chart(areaChartCanvas);

          var areaChartData = {
            labels: <?=json_encode($labels);?>,
            datasets: [

              {
                label: "Vendite",
                fillColor: "rgba(60,141,188,0.9)",
                strokeColor: "rgba(60,141,188,0.8)",
                pointColor: "#fff",
                pointStrokeColor: "rgba(60,141,188,1)",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(60,141,188,1)",
                data: <?=json_encode(array_values($values));?>
              }
            ]
          };

          var areaChartOptions = {
            //Boolean - If we should show the scale at all
            showScale: true,
            //Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines: true,
            //String - Colour of the grid lines
            scaleGridLineColor: "rgba(0,0,0,.05)",
            //Number - Width of the grid lines
            scaleGridLineWidth: 1,
            //Boolean - Whether to show horizontal lines (except X axis)
            scaleShowHorizontalLines: true,
            //Boolean - Whether to show vertical lines (except Y axis)
            scaleShowVerticalLines: true,
            //Boolean - Whether the line is curved between points
            bezierCurve: true,
            //Number - Tension of the bezier curve between points
            bezierCurveTension: 0.3,
            //Boolean - Whether to show a dot for each point
            pointDot: true,
            //Number - Radius of each point dot in pixels
            pointDotRadius: 4,
            //Number - Pixel width of point dot stroke
            pointDotStrokeWidth: 1,
            //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
            pointHitDetectionRadius: 20,
            //Boolean - Whether to show a stroke for datasets
            datasetStroke: true,
            //Number - Pixel width of dataset stroke
            datasetStrokeWidth: 2,
            //Boolean - Whether to fill the dataset with a color
            datasetFill: true,
            //String - A legend template
            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
            //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio: true,
            //Boolean - whether to make the chart responsive to window resizing
            responsive: true
          };

          //Create the line chart
          areaChart.Line(areaChartData, areaChartOptions);
  	});
  </script>
