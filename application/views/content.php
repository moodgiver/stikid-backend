<div class="content-wrapper">

  <section class="content-header">
    <h1 class="label label-primary">
      <span class="title-header">Dashboard</span>
      <span class="title-description"></span>
      <span class="fa fa-spinner fa-spin hide working"></span>
    </h1>
  </section>

  <section class="content">
    <?php
      if ( $_SESSION['user']['role'] == 'admin' ){
        include_once('homepage.php');
      }
    ?>
  </section>
</div>
