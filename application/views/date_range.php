<div class="col-lg-12 clearfix daterange">
  <div class="col-lg-1">Periodo</div>
  <div class="col-lg-5">
    <div class="input-group">
      <div class="input-group-addon">
        <i class="fa fa-calendar"></i>
      </div>
      <input type="text" class="form-control pull-right active date-range date-range-filter" data-controller="ordini-status-filter-date" data-mode="" name="daterange" id="daterange" value="<?=date('d-m-Y',time())?> - <?=date('d-m-Y',time())?>">
    </div>
  </div>
  <div class="col-lg-1 col-xs-2 btn-cerca">
    <button class="btn btn-primary">Filtra</button>
  </div>
  <div class="col-lg-4 col-xs-6 cerca-ordine">
    <input type="text" class="form-control ordine_nr" placeholder="nr ordine > Es. 2022">
  </div>
  <div class="col-lg-1 col-xs-2 btn-cerca">
    <button class="btn btn-primary">Cerca</button>
  </div>
</div>
<div class="clearfix"></div>

<script>
$(document).ready(function(){
  $('#daterange').daterangepicker({
    "locale" : "it"
  });
});

</script>
