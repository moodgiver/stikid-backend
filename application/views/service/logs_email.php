<table class="table table-striped table-bordered">
  <thead>
    <th>Data</th>
    <th>Inviata a</th>
    <th>Tipo</th>
    <th>Rif. Ordine</th>
    <th>Oggetto</th>
  </thead>
  <tbody>
<?php

  foreach ( $emails AS $email ){
    $classe = "success";
    $stato = 'OK';
    if ( !$email['status'] ){
      $classe = "warning";
      $stato = 'FALLITA';
    }
    echo '
      <tr class="pointer email-message" data-id="'.$email['id_email'].'" data-sendto="'.$email['email'].'">
        <td>
        '.$email['data_email'].'
        </td>
        <td>
        '.$email['email'].'
        </td>
        <td>
        '.$email['tipo'].'
        </td>
        <td>'
        .$email['ordine'].
        '</td>
        <td>
        '.$email['subject'].'
        </td>
        <td>
      <label class="label label-'.$classe.'">'.$stato.'</label>
      <textarea class="hide" id="message_'.$email['id_email'].'" readonly>'.$email['message'].'</textarea>
      </tr>
      ';
  }
 ?>
 </tbody>
 </table>
 <div class="modal fade in" tabindex="-1" id="emailModal" role="dialog">
     <div class="modal-dialog" style="width:80vw">
     <div class="modal-content">
     <div class="modal-header">
     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
     <h1>Email</h1>
     </div>

     <div class="modal-body">

     </div>
     <div class="modal-footer">
     <button type="button" class="btn btn-success btn-update-fornitore" data-id="" data-ordine="">Invia</button>
     </div>
     </div><!-- /.modal-content -->
     </div><!-- /.modal-dialog -->
   </div>
<script>
$(document).ready(function(){
  $('.email-message').on('click',function(){
    var msg = 'Inviata a: ' + $(this).data('sendto') + '<br>';
    msg += $('#message_' + $(this).data('id')).val();

    $('.modal-body').html( msg );
    $('#emailModal').modal('show');
  })
})
 </script>
