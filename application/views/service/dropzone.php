<div class="modal fade in" tabindex="-1" id="dropzoneModal" role="dialog">
    <div class="modal-dialog" style="width:80%">
    <div class="modal-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-delete"></span></span></button>
    <h1>Carica Immagine</h1>
    Clicca o trascina le immagini nell'area sottostante
    </div>
    <div class="modal-body form-inline" id="dropzone"  style="min-height:400px;border:8px dashed ##eaeaea">
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-success btn-action" data-controller="uploadimage" data-action="login">Carica</button>
    </div>
    </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<script src="#application.homedir#dist/js/dropzone.js"></script>
