<header class="main-header">

  <!-- Logo -->
  <a href="<?=base_url()?>" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini">S!</span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>STICASA/STIKID</b></span>
  </a>

  <!-- Header Navbar -->
  <nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>
    <!-- Navbar Right Menu -->
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <!-- Messages: style can be found in dropdown.less-->
        <li class="dropdown messages-menu">
          <!-- Menu toggle button -->
          <?php
            if ( $_SESSION['user']['role'] == 'admin' ){
            echo '
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-envelope-o"></i>
          </a>
          <ul class="dropdown-menu" style="width: 650px;background:#ecf0f5;border: 1px solid #c9c9c9;">

            <li class="header">Ultime 4 email inviate</li>
            <li>
              <!-- inner menu: contains the messages -->
              <ul class="menu">';
                  $n = 0;
                  foreach ( $emails AS $email ){
                    if ( $n < 4 ){
                      echo '<li>
                      <ul class="menu">
                        <li><!-- start notification -->
                          <p>
                          &nbsp;'.$email['data_email'].' - <strong>'.$email['ordine'].'</strong>
- '.$email['tipo'].'
                          </p>
                        </li><!-- end notification -->
                      </ul></li>';
                    }
                    $n++;
                  }
              echo '
              </ul><!-- /.menu -->
            </li>
            <li class="footer push-right"><a href="#" class="all-email-logs push-right">Vedi tutte le email inviate</a></li>
          </ul>
        </li><!-- /.messages-menu -->';
        }
        ?>



        <!-- User Account Menu -->
        <li class="dropdown user user-menu">
          <!-- Menu Toggle Button -->
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <!-- The user image in the navbar-->
            <img src="public/dist/img/avatar.png" class="user-image" alt="User Image">
                  <!-- hidden-xs hides the username on small devices so only the image appears. -->
            <span class="hidden-xs"><?=$_SESSION['user']['customer']?></span>
          </a>
          <ul class="dropdown-menu">
            <!-- The user image in the menu -->
            <li class="user-header">
              <img src="public/dist/img/avatar.png" class="user-image" alt="User Image">
              <p>
                <!--<cfoutput>#session.user.nome#</cfoutput> - <cfif session.user.role EQ 0>Shop Manager<cfelse>Fornitore</cfif>
                <small></small>-->
              </p>
            </li>
            <!---
            <li class="user-body">
              <div class="col-xs-4 text-center">
                <a href="#">Followers</a>
              </div>
              <div class="col-xs-4 text-center">
                <a href="#">Sales</a>
              </div>
              <div class="col-xs-4 text-center">
                <a href="#">Friends</a>
              </div>
            </li> --->
            <!-- Menu Footer-->
            <li class="user-footer">
             <!---  <div class="pull-left">
                <a href="#" class="btn btn-default btn-flat">Profile</a>
              </div> --->
              <div class="pull-right">
                <a href="logout" class="btn btn-default btn-flat">Sign out</a>
              </div>
            </li>
          </ul>
        </li>
        <!-- Control Sidebar Toggle Button -->

        <li>
          <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
        </li>

      </ul>
    </div>
  </nav>
</header>
<script>
$(document).ready(function(){
  $('.all-email-logs').on('click',function(){
    $.post('ajax/ordini',
      {
        action: 'ordini-emails',
        sito: null
      } , function(result){
        $('.content').html ( result );
      }
    )
  })
})
</script>
