<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'main';
$route['ajax/ordini'] = 'ordini';
$route['ajax/negozio'] = 'prodotti';
$route['ajax/vendite'] = 'vendite';
$route['ajax/settings'] = 'settings';
$route['ajax/clienti'] = 'clienti';
$route['ajax/contenuti'] = 'pagine';
$route['ajax/moduli'] = 'moduli';
$route['ajax/designers'] = 'designers';
$route['ajax/servizio'] = 'servizio';
$route['upload/(:any)'] = 'main/upload/$1';
$route['signup'] = 'signup';
$route['registrazione'] = 'main/registrazione';
$route['login'] = 'login';
$route['signin'] = 'login/signin';
$route['logout'] = 'ajax/clear_session';
$route['registrami'] = 'main/registrami';
$route['password/reset'] = 'password/reset';
$route['clear/session'] = 'ajax/clear_session';
$route['clear/cart'] = 'ajax/clear_cart_items';
$route['cerca'] = 'main/ricerca';
$route['cart/conferma'] = 'main/cart';
$route['cart/spedizione'] = 'main/cart_spedizione';
$route['cart/checkout'] = 'main/cart_checkout';
$route['cart/paypal/(:any)'] = 'main/paypal/$1';
$route['ordine/paypal'] = 'main/paypal_conferma/';
$route['ordine/bonifico'] = 'main/bonifico_conferma';
$route['ordine/paypal_notifiy'] = 'main/paypal_notify';
$route['profilo/ordine/(:any)/(:any)'] = 'main/ordine/$1/$2';
$route['profilo/(:any)'] = 'main/profilo/$1';
$route['pagina/(:any)/(:any)'] = 'main/pagina/$1/$2';
$route['designers/(:any)/(:any)'] = 'main/designer/$1/$2';
$route['simulatore/upload'] = 'main/upload_foto';
$route['simulatore/(:any)'] = 'main/simulatore/$1';
$route['test/mail'] = 'main/send_mail';
$route['ordine/paypal_notify'] = 'main/paypal_notify';
$route['(:any)-quadri-per-bambini'] = 'main/quadriperbambini/$1';
$route['(:any)-wall-stickers'] = 'main/collection/$1';
$route['(:any)-adesivi-murali'] = 'main/collection_new/$1';
$route['(:any)/(:any)-adesivo-murale/(:any)'] = 'main/prodotto/$3';
$route['(:any)/(:any)-wall-sticker/(:any)'] = 'main/prodotto/$3';
$route['(:any)/(:any)-quadro-per-bambino/(:any)'] = 'main/prodottoqpb/$3';
$route['admin/'] = 'admin';
$route['offerte'] = 'main/offerte';
$route['debug'] = 'main/debug';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['bordero'] = 'ordini/bordero';
$route['etichette'] = 'ordini/etichette';
$route['cron/cb366bm/(:any)'] = 'admin/cron/$1';
