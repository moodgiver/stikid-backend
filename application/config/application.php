<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['application']['status'] = array(

  'stato' => array (
    array (
      'name'    => 'Bonifico',
      'colore'  => 'red',
      'palette' => 'warning',
      'button'  => 'Pagamento ricevuto'
    ),
    array (
      'name'    => 'Ricevuto',
      'colore'  => 'aqua',
      'palette' => 'primary',
      'button'  => 'Invia al fornitore'
    ),
    array (
      'name'    => 'Inviato fornitore',
      'colore'  => 'yellow',
      'palette' => 'warning',
      'button'  => 'In lavorazione'
    ),
    array (
      'name'    => 'In lavorazione',
      'colore'  => 'yellow',
      'palette' => 'warning',
      'button'  => 'In Spedizione'
    ),
    array (
      'name'    => 'In Spedizione',
      'colore'  => 'yellow',
      'palette' => 'warning',
      'button'  => 'Spedito'
    ),
    array (
      'name'    => 'Spedito',
      'colore'  => 'green',
      'palette' => 'success',
      'button'  => 'Evaso'
    ),
    array (
      'name'    => 'Evaso',
      'colore'  => 'green',
      'palette' => 'success',
      'button'  => false
    )
  )
);

if ( strpos($_SERVER['SERVER_NAME'],'mooddemo') ){

  $config['application']['collezione_image_path'] = 'C:/ampps/www/static/images/collezioni/';
  $config['application']['product_image_path'] = 'C:/ampps/www/static/images/ambienti/';
  $config['application']['slides_path'] = 'C:/ampps/www/static/images/slides/';
  $config['application']['temp_image_path'] = 'C:/ampps/www/static/images/temp/';
  $config['application']['simulatore_path'] = 'C:/ampps/www/static/images/simulatore/';

  $config['application']['slides_uri'] = '//127.0.0.1/static/images/slides/';
  $config['application']['simulatore_uri'] = '//127.0.0.1/static/images/simulatore/';
  $config['application']['temp_image_url'] = '//127.0.0.1/static/images/temp/';
  $config['application']['backup'] = "C:/SERVER BACKUP/LINUX8/";

} else {
  //PATH CONFIG
  $config['application']['collezione_image_path'] = '/home/admin/public_html/static/images/collezioni/';
  $config['application']['product_image_path'] = '/home/admin/public_html/static/images/ambienti/';
  $config['application']['product_sticker_path'] = '/home/admin/public_html/static/images/stickers/';
  $config['application']['slides_path'] = '/home/admin/public_html/static/images/slides/';
  $config['application']['temp_image_path'] = '/home/admin/public_html/static/images/temp/';
  $config['application']['simulatore_path'] = '/home/admin/public_html/static/images/simulatore/';
  $config['application']['vettoriali_path'] = '/home/admin/public_html/static/images/vettoriali/';
  $config['application']['banners_path'] = '/home/admin/public_html/static/images/banners/';
  $config['application']['ordini_allegati_path'] = '/home/admin/public_html/static/docs/allegati/';


  //URI CONFIG
  $config['application']['product_image_uri'] = '//cdn.stikid.com/images/ambienti/';
  $config['application']['product_sticker_uri'] = '//cdn.stikid.com/images/stickers/';
  $config['application']['slides_uri'] = '//cdn.stikid.com/images/slides/';
  $config['application']['simulatore_uri'] = '//cdn.stikid.com/images/simulatore/';
  $config['application']['vettoriali_uri'] = '//cdn.stikid.com/images/vettoriali/';
  $config['application']['ordini_allegati_uri'] = '//cdn.stikid.com/docs/allegati/';
  $config['application']['temp_image_url'] = '//cdn.stikid.com/images/temp/';
  $config['application']['banners_uri'] = '//cdn.stikid.com/images/banners/';
  $config['application']['backup'] = "/home/admin/softaculous_backups/";
}

$config['application']['siti'] = array (
  array (
    'id' => 0,
    'sito' => 'visibile in tutti'
  ),
  array (
    'id' => 1,
    'sito' => 'sticasa.com'
  ),
  array (
    'id' => 4,
    'sito' => 'stikid.com'
  ),
  array (
    'id' => 8,
    'sito' => 'quadriperbambini.it'
  )
);
$config['application']['menus'] = array (
  'header_top_left', 'header_top_right' , 'header' , 'sidebar' , 'footer_col_1' ,'footer_col_2','footer_col_3','footer_col_4' , 'tab_prodotto'
);

$config['application']['email_tracking'] = '<p>Gentile Cliente, il tuo ordine e\' stato spedito.</p>
<p>Ti ricordiamo che le istruzioni per l\'applicazione del tuo sticker sono disponibili sul nostro sito sia in video che in file scaricabile.</p>
<p>
Buon divertimento!<br>
Lo STAFF STICASA/STIKID
</p>
<p><small>
Il link per verificare il tracking del tuo ordine sara\' attivo nelle prossime 24 ore.</small><br>
<a href="http://www.gls-italy.com/track_trace_user.asp?locpartenza=m1&numbda=_numero_ordine_&tiporicerca=numbda&codice=4521&cl=1" target="_blank">Clicca qui</a> per verificare lo Stato spedizione ordine</p>';
