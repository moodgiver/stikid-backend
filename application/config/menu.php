<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['menu']['dashboard'] = array (
    'header' => false,
    'name' => 'Dashboard',
    'icon' => 'dashboard',
    'submenu' => false,
    'role'  => array ( 'admin' )
);



$config['menu']['ordini'] = array(
        'header'  => 'ecommerce',
        'name'  => 'Ordini',
        'icon'  => 'shopping-cart',
        'mode'  => 'ordini',
        'role'  => array ( 'admin' , 'fornitore' ),
        'submenu' => array (
          array (
            'name'    => 'Ricevuti',
            'routing' => 'ordini-ricevuti',
            'role'    => array ( 'admin' )
          ),
          array (
            'name'      => 'Bonifico',
            'routing'   => 'ordini-bonifico',
            'role'      => array ( 'admin' )
          ),
          array (
            'name'      => 'Inviati fornitore',
            'routing'   => 'ordini-fornitore',
            'role'      => array ( 'admin' , 'fornitore' )
          ),
          array (
            'name'      => 'In Lavorazione',
            'routing'   => 'ordini-lavorazione',
            'role'      => array ( 'admin' , 'fornitore' )
          ),
          array (
            'name'      => 'In Spedizione',
            'routing'   => 'ordini-spedizione',
            'role'      => array ( 'admin' , 'fornitore' )
          ),
          array (
            'name'      => 'Spediti',
            'routing'   => 'ordini-spediti',
            'role'      => array ( 'admin' , 'fornitore' )
          ),
          array (
            'name'      => 'Evasi',
            'routing'   => 'ordini-evasi',
            'role'      => array ( 'admin' )
          ),
          array (
            'name'      => 'Cerca Ordine',
            'routing'   => 'ordini-cerca',
            'role'      => array ( 'admin' )
          ),
          array (
            'name'      => 'Non completati',
            'routing'   => 'ordini-recupero',
            'role'      => array ( 'admin' )
          ),
          array (
            'name'      => 'Bordero',
            'routing'   => 'ordini-bordero',
            'role'      => array ( 'admin' , 'fornitore'),
            'link'      => base_url().'ordini/bordero'
          ),
          array (
            'name'      => 'Etichette',
            'routing'   => 'ordini-etichette',
            'role'      => array ( 'admin' , 'fornitore'),
            'link'      => base_url().'ordini/etichette'
          )
        )
      );

$config['menu']['vendite'] = array(
        'header'  => false,
        'name'    => 'Vendite',
        'mode'    => 'vendite',
        'icon'    => 'money',
        'role'    => array('admin'),
        'submenu' => array (
          array (
            'name'    => 'Vendite Periodo',
            'routing' => 'vendite-periodo',
            'role'      => array ( 'admin' )
          ),
          array (
            'name'    => 'Vendite Coupon',
            'routing' => 'vendite-coupon',
            'role'      => array ( 'admin' )
          ),
          array (
            'name'    => 'Vendite Prodotti',
            'routing' => 'vendite-prodotti',
            'role'      => array ( 'admin' )
          ),
          array (
            'name'    => 'Vendite Clienti',
            'routing' => 'vendite-clienti',
            'role'      => array ( 'admin' )
          ),
          array (
            'name'    => 'Costi Fornitore',
            'routing' => 'vendite-costi-fornitore',
            'role'      => array ( 'admin' )
          ),
          array (
            'name'    => 'Simulatore Costi',
            'routing' => 'costi-simulatore',
            'role'      => array ( 'admin' )
          ),
        )
    );

$config['menu']['clienti'] = array(
  'header'  => false,
  'name'    => 'Clienti',
  'mode'    => 'clienti',
  'icon'    => 'users',
  'role'    => array ('admin'),
  'submenu' => array (
    array (
      'name'    => 'Gestione Clienti',
      'routing' => 'clienti-gestione',
      'role'      => array ( 'admin' )
    ),
    array (
      'name'    => 'Registrazioni',
      'routing' => 'clienti-registrazioni',
      'role'      => array ( 'admin' )
    ),
  )
);

$config['menu']['negozio'] = array(
  'header'  => 'Negozio',
  'name'    => 'Prodotti',
  'mode'    => 'negozio',
  'icon'    => 'cubes',
  'role'    => array('admin'),
  'submenu' => array (
    array (
      'name'    => 'Collezioni',
      'routing' => 'negozio-collezioni',
      'role'      => array ( 'admin' )
    ),
    array (
      'name'    => 'Cerca Prodotto',
      'routing' => 'negozio-prodotto-ricerca',
      'role'      => array ( 'admin' )
    ),
    array (
      'name'    => 'Stickers stikid.com',
      'routing' => 'negozio-prodotti',
      'role'    => array ( 'admin' ),
      'sito'    => 4
    ),
    array (
      'name'    => 'Stickers sticasa.com',
      'routing' => 'negozio-prodotti',
      'role'    => array ( 'admin' ),
      'sito'    => 1
    ),
    array (
      'name'    => 'quadriperbambini.com',
      'routing' => 'negozio-prodotti',
      'role'    => array ( 'admin' ),
      'sito'    => 8
    ),
    array (
      'name'    => 'Stickers 4tribe.com',
      'routing' => 'negozio-prodotti-4tribe',
      'role'      => array ( 'admin' )
    ),
    array (
      'name'    => 'Prodotti Upsell',
      'routing' => 'negozio-prodotti-upsell',
      'role'      => array ( 'admin' )
    ),
    array (
      'name'    => 'Gestione Colori',
      'routing' => 'negozio-prodotti-stikid',
      'role'      => array ( 'admin' )
    ),
  )
);

$config['menu']['impostazioni'] = array(
  'header'  => false,
  'name'    => 'Impostazioni',
  'mode'    => 'settings',
  'icon'    => 'cogs',
  'role'    => array ( 'admin' ),
  'submenu' => array (
    array (
      'name'    => 'Generali',
      'routing' => 'settings-generali',
      'role'      => array ( 'admin' )
    ),
    array (
      'name'    => 'Meta',
      'routing' => 'settings-meta',
      'role'      => array ( 'admin' )
    ),
    array (
      'name'    => 'Prezzi Sticker',
      'routing' => 'settings-prezzi-sticker',
      'role'      => array ( 'admin' )
    ),
    array (
      'name'    => 'Fornitori',
      'routing' => 'settings-fornitori',
      'role'      => array ( 'admin' )
    ),
    array (
      'name'    => 'Costi Fornitori',
      'routing' => 'settings-costi-fornitori',
      'role'      => array ( 'admin' )
    ),
    array (
      'name'    => 'Spedizioni',
      'routing' => 'settings-spedizioni',
      'role'      => array ( 'admin' )
    ),
    array (
      'name'    => 'Coupons',
      'routing' => 'settings-coupons',
      'role'      => array ( 'admin' )
    ),

  )
);

$config['menu']['moduli'] = array(
  'header'  => false,
  'name'    => 'Plug-in',
  'mode'    => 'moduli',
  'icon'    => 'clone',
  'role'    => array('admin'),
  'submenu' => array (
    array (
      'name'    => 'Slider Stikid',
      'routing' => 'moduli-slider-stikid',
      'role'      => array ( 'admin' )
    ),
    array (
      'name'    => 'Slider Sticasa',
      'routing' => 'moduli-slider-sticasa',
      'role'      => array ( 'admin' )
    ),
    array (
      'name'    => 'Slider QpB',
      'routing' => 'moduli-slider-qpb',
      'role'      => array ( 'admin' )
    ),
    array (
      'name'    => 'Banners',
      'routing' => 'moduli-banners',
      'role'      => array ( 'admin' )
    ),
    array (
      'name'    => 'Special',
      'routing' => 'moduli-special',
      'role'      => array ( 'admin' )
    ),
  )
);
$config['menu']['designers'] = array(
  'header'  => false,
  'name'    => 'Designers',
  'mode'    => 'designers',
  'icon'    => 'paint-brush',
  'role'    => array ( 'admin' , 'designer' ),
  'submenu' => array (
    array (
      'name'    => 'Profilo',
      'routing' => 'designers-profilo',
      'role'    => array ( 'admin'  )
    ),
    array (
      'name'    => 'Vendite',
      'routing' => 'designers-vendite',
      'role'     => array ( 'admin' , 'designer' )
    ),
    array (
      'name'    => 'Royalties',
      'routing' => 'designer-royalties',
      'role'      => array ( 'admin' )
    ),
  )
);

$config['menu']['contenuti'] = array(
  'header'  => false,
  'name'    => 'Contenuti',
  'mode'    => 'contenuti',
  'icon'    => 'html5',
  'role'    => array ('admin'),
  'submenu' => array (
    array (
      'name'    => 'Menu',
      'routing' => 'contenuti-menu',
      'role'     => array ( 'admin' )
    ),
    array (
      'name'    => 'Pagine',
      'routing' => 'contenuti-pagine',
      'role'     => array ( 'admin' )
    )
  )
);

$config['menu']['servizio'] = array(
  'header'  => false,
  'name'    => 'Servizio',
  'mode'    => 'servizio',
  'icon'    => 'cogs',
  'role'    => array ('admin'),
  'submenu' => array (
    array (
      'name'    => 'Log (tempo reale)',
      'routing' => 'servizio-log-current',
      'role'     => array ( 'admin' )
    ),
    array (
      'name'    => 'Immagini',
      'routing' => 'servizio-immagini',
      'role'     => array ( 'admin' )
    )
  )
);
