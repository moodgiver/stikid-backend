<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config = array (
  'login' => array (
      array(
        'field' => 'email_login',
        'label' => 'Email',
        'errors'=> array (
          'required'    => 'L\'indirizzo email e\' obbligatorio',
          'valid_email' => 'L\'indirizzo email deve essere un indirizzo valido'
        ),
        'rules' => 'required|valid_email'
      ),
      array(
        'field' => 'password_login',
        'label' => 'Password',
        'errors'=> array (
          'required'    => 'La password e\' obbligatoria'
        ),
        'rules' => 'required'
      ),
  ),
  'profile' => array(
    array(
    'field' => 'o_nome',
    'label' => 'Nome',
    'errors'=> array (
        'required'  => 'Il nome è obbligatorio',
        'alpha_numeric_spaces'     => 'Il nome deve contenere solo caratteri'
    ),
    'rules' => 'required|alpha_numeric_spaces'
  ),
  array(
    'field' => 'o_cognome',
    'label' => 'Cognome',
    'errors'=> array (
        'required'  => 'Il cognome è obbligatorio',
        'alpha_numeric_spaces'     => 'Il cognome deve contenere solo caratteri'
    ),
    'rules' => 'required|alpha_numeric_spaces'
  ),
  array(
    'field' => 'o_azienda',
    'label' => 'Azienda',
    'rules' => 'alpha_numeric_spaces'
  ),
  array(
    'field' => 'o_indirizzo',
    'label' => 'Indirizzo',
    'rules' => 'required'
  ),
  array(
    'field' => 'o_cap',
    'label' => 'CAP',
    'rules' => 'required|alpha_numeric_spaces|max_length[8]'
  ),
  array(
    'field' => 'o_citta',
    'label' => 'CITTA',
    'rules' => 'required|alpha_numeric_spaces'
  ),
  array(
    'field' => 'o_pv',
    'label' => 'PV',
    'rules' => 'required|alpha'
  ),
  array(
    'field' => 'o_nazione',
    'label' => 'NAZIONE',
    'rules' => 'required|alpha'
  ),
  array(
    'field' => 'o_telefono',
    'label' => 'TELEFONO',
    'rules' => 'numeric|max_length[15]'
  ),
  array(
    'field' => 'o_cellulare',
    'label' => 'CELLULARE',
    'rules' => 'required|numeric|max_length[15]'
  ),
  array(
    'field' => 'o_cf',
    'label' => 'Codice Fiscale',
    'errors'=> array (
        'required'      => 'Il codice fiscale è obbligatorio',
        'alpha_numeric' => 'Sono permessi solo caratteri e numeri',
        'exact_length'  => 'La lunghezza del Codice Fiscale deve essere di 16 caratteri'
    ),
    'rules' => 'required|alpha_numeric'
  ),
  array(
    'field' => 'o_piva',
    'label' => 'P.IVA',
    'rules' => 'alpha_numeric'
  )
  )
);
