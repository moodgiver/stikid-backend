<?php

$config['schema']['database'] = array(
  'fornitori' => array (
    'table'         => 'tbl_utenti',
    'key'           => 'id_utente',
    'fields'        => array (
      'id_utente'   => array (
        'label'     => '',
        'type'      => 'hidden',
        'default'   => 0,
      )
      ,
      'ac_nome'       => array(
        'label'       => 'Nome',
        'type'        => 'input',
        'cols'        => 4,
        'default'     => '',
      ),
      'ac_cognome'    => array(
        'label'       => 'Cognome',
        'type'        => 'input',
        'cols'        => 4,
        'default'     => '',
      ),
      'ac_indirizzo'  => array(
        'label'       => 'Indirizzo',
        'type'        => 'input',
        'cols'        => 10,
        'default'     => '',
      ),
      'ac_nrcivico'   => array(
        'label'       => 'Nr',
        'type'        => 'input',
        'cols'        => 2,
        'default'     => '',
      ),
      'ac_citta'      => array(
        'label'       => 'Comune',
        'type'        => 'input',
        'cols'        => 6,
        'default'     => '',
      ),
      'ac_zip'        => array(
        'label'       => 'CAP',
        'type'        => 'input',
        'cols'        => 4,
        'default'     => '',
      ),
      'ac_provincia'  => array(
        'label'       => 'PV',
        'type'        => 'input',
        'cols'        => 2,
        'default'     => '',
      ),
      'ac_telefono1'  => array(
        'label'       => 'Telefono',
        'type'        => 'input',
        'cols'        =>  6,
        'default'     => '',
      ),
      'ac_email'      => array(
        'label'       => 'Email',
        'type'        => 'input',
        'cols'        => 6,
        'default'     => '',
      ),
      'ac_username'  => array(
        'label'       => 'Nome utente',
        'type'        => 'input',
        'cols'        => 5,
        'default'     => '',
      ),
      'ac_password'  => array(
        'label'       => 'Password',
        'type'        => 'password',
        'cols'        => 5,
        'default'     => '',
      ),
      'bl_attivo'  => array(
        'label'       => 'Attivo',
        'type'        => 'select',
        'values'      => array ( 'Non attivo' , 'Attivo' ),
        'cols'        => 2,
        'default'     => 1,
      ),
      'bl_tipo'    => array(
        'label'    => '',
        'type'     => 'hidden',
        'value'    => 2,
        'default'  => 0,
      )
    )
  ),
  'costi-fornitori' => array (
    'table'         => 'tbl_costi_sticker',
    'key'           => 'id_fornitore',
    'fields'        => array (
      'id_fornitore'   => array (
        'label'     => '',
        'type'      => 'hidden',
        'default'   => 0,
      ),
      'ac_costo' => array(
        'label'       => 'Costo mq',
        'type'        => 'input',
        'cols'        => 2,
        'default'     => '',
      ),
      'ac_costo_minimo'  => array(
        'label'       => 'Costo minimo',
        'type'        => 'input',
        'cols'        => 2,
        'default'     => '',
      ),
      'ac_costo_soglia'  => array(
        'label'       => 'Costo soglia',
        'type'        => 'input',
        'cols'        => 2,
        'default'     => '',
      ),
      'ac_costo_quadri' => array(
        'label'       => 'Costo Quadri',
        'type'        => 'input',
        'cols'        => 2,
        'default'     => '',
      ),
      'ac_costo_minimo_quadri'  => array(
        'label'       => 'Costo minimo Quadri',
        'type'        => 'input',
        'cols'        => 2,
        'default'     => '',
      ),
      'ac_costo_soglia_quadri'  => array(
        'label'       => 'Costo soglia quadri',
        'type'        => 'input',
        'cols'        => 2,
        'default'     => '',
      ),

    )
  ),
  'spedizioni' => array (
    'table'         => 'tbl_spedizioni',
    'key'           => 'id_spedizione',
    'fields'        => array (
      'id_spedizione'   => array (
        'label'     => '',
        'type'      => 'hidden',
        'default'   => 0,
      ),
      'ac_prezzo_max' => array(
        'label'       => 'Ordini inferiore a &euro;',
        'type'        => 'input',
        'cols'        => 3,
        'default'     => '',
      ),
      'ac_spedizione'  => array(
        'label'       => 'Prezzo',
        'type'        => 'input',
        'cols'        => 3,
        'default'     => '',
      ),
      'ac_costo' => array(
        'label'       => 'Costo',
        'type'        => 'input',
        'cols'        => 3,
        'default'     => '',
      )
    )
  ),
  'slider' => array (
    'table'         => 'tbl_slider',
    'key'           => 'id_slider',
    'fields'        => array (
      'id_slider'   => array (
        'label'     => '',
        'type'      => 'hidden',
        'default'   => 0,
      ),
      'ac_slide_image' => array(
        'label'       => 'Immagine',
        'type'        => 'image-uri',
        'dropzone'    => true,
        'uri'         => '//cdn.stikid.com/images/slides/',
        'default'     => 'noimage.png',
        'cols'        => 2,
      ),
      'ac_slide_title' => array(
        'label'       => 'Slide',
        'type'        => 'input',
        'cols'        => 2,
        'default'     => 0,
      ),
      'int_sito'   => array (
        'label'     => 'Sito',
        'type'      => 'select',
        'values'    => array ( '' , 'sticasa.com' , '' , '' , 'stikid.com' , '','','','quadrixbamb' ),
        'cols'      => 2,
        'default'   => 0
      ),
      'int_order' => array(
        'label'       => 'Ordine',
        'type'        => 'select',
        'values'      => array ( 0 , 1 , 2 , 3 , 4 , 5 , 6 , 7 , 8 , 9 ),
        'cols'        => 1,
        'default'     => 0,
      ),
      'bl_image_type' => array(
        'label'       => 'Mobile',
        'type'        => 'select',
        'values'      => array ( 'Desktop' , 'Mobile '),
        'cols'        => 2,
        'default'     => 0,
      ),
      'bl_attivo' => array(
        'label'       => 'Stato',
        'type'        => 'select',
        'values'      => array ( 'Non attiva' , 'Attiva'),
        'cols'        => 2,
        'default'     => 1,
      ),
    )
  ),
  'designers' => array (
    'table'         => 'tbl_designers',
    'key'           => 'id_designer',
    'fields'        => array (
      'id_designer' => array (
        'label'     => '',
        'type'      => 'hidden',
        'default'   => 0,
      ),
      'ac_image_designer' => array(
        'label'       => 'Immagine',
        'type'        => 'image-uri',
        'uri'         => '//cdn.stikid.com/images/designers/',
        'dropzone'    => true,
        'cols'        => 2,
        'default'     => 'noimage.png',
      ),
      'ac_nome_designer' => array(
        'label'       => 'Nome',
        'type'        => 'input',
        'cols'        => 5,
        'default'     => '',
      ),
      'ac_cognome_designer' => array(
        'label'       => 'Cognome',
        'type'        => 'input',
        'cols'        => 5,
        'default'     => '',
      ),
      'ac_info' => array(
        'label'       => 'Informazioni',
        'type'        => 'textarea',
        'cols'        => 6,
        'default'     => '',
      ),
      'ac_username' => array(
        'label'       => 'Username',
        'type'        => 'input',
        'cols'        => 4,
        'default'     => '',
      ),
      'ac_password' => array(
        'label'       => 'Password',
        'type'        => 'input',
        'cols'        => 4,
        'default'     => '',
      ),
      'ac_email' => array(
        'label'       => 'Email',
        'type'        => 'input',
        'cols'        => 4,
        'default'     => '',
      ),
      'ac_value' => array(
        'label'       => 'Royalty x sticker',
        'type'        => 'input',
        'cols'        => 2,
        'default'     => '',
      ),
      'ac_value_max' => array(
        'label'       => 'Royalties max',
        'type'        => 'input',
        'cols'        => 2,
        'default'     => '',
      ),
      'bl_status' => array(
        'label'       => 'Attivo',
        'type'        => 'select',
        'values'      => array ( 'Non attivo' , 'Attivo'),
        'cols'        => 2,
        'default'     => '',
      ),
    )
  ),
  'coupons' => array (
    'table'         => 'tbl_coupons',
    'key'           => 'id_coupon',
    'fields'        => array (
      'id_coupon' => array (
        'label'     => '',
        'type'      => 'hidden',
        'default'   => 0,
      ),
      'ac_coupon' => array(
        'label'   => 'Nome',
        'type'    => 'input',
        'default' => '',
        'cols'    => 4
      ),
      'dt_start' => array(
        'label'   => 'Inizio',
        'type'    => 'input',
        'default' => '',
        'cols'    => 4,
        'placeholder' => 'aaaa-mm-gg'
      ),
      'dt_end' => array(
        'label'   => 'Fine',
        'type'    => 'input',
        'default' => '',
        'cols'    => 4,
        'placeholder' => 'aaaa-mm-gg'
      ),
      'ac_valore' => array(
        'label'   => 'Valore',
        'type'    => 'input',
        'default' => '',
        'cols'    => 2
      ),
      'bl_valore' => array(
        'label'   => 'Tipo',
        'type'    => 'select',
        'values'  => array ( '%' , 'Importo' ),
        'default' => '',
        'cols'    => 2
      ),
      'ac_codice' => array(
        'label'   => 'Codice',
        'type'    => 'input',
        'default' => '',
        'cols'    => 2
      ),
      'bl_attivo' => array(
        'label'   => 'Attivo',
        'type'    => 'select',
        'values'  => array ( 'Non attivo','Attivo'),
        'default' => '',
        'cols'    => 2
      ),
    )
  ),
  'banner' => array (
    'table'         => 'tbl_banners',
    'key'           => 'id_banner',
    'fields'        => array (
      'id_banner' => array (
        'label'     => '',
        'type'      => 'hidden',
        'default'   => 0,
      ),
      'banner' => array(
        'label'       => 'Immagine',
        'type'        => 'image-uri',
        'uri'         => '//cdn.stikid.com/images/banners/',
        'dropzone'    => true,
        'cols'        => 3,
        'default'     => 'noimage.png',
      ),
      'int_sito'   => array (
        'label'     => 'Sito',
        'type'      => 'select',
        'values'    => array ( 'tutti' , 'sticasa.com' , '' , '' , 'stikid.com' , '','','','quadrixbamb' ),
        'cols'      => 2,
        'default'   => 0
      ),
      'nome' => array(
        'label'   => 'Nome',
        'type'    => 'input',
        'default' => '',
        'cols'    => 2
      ),

      'posizione' => array(
        'label'   => 'Posizione',
        'type'    => 'select',
        'values'  => array ( 'header','sidebar','footer' , 'after-offerte'),
        'value_type' => 'string',
        'default' => '',
        'cols'    => 2
      ),
      'filtro' => array(
        'label'   => 'Filtro',
        'type'    => 'select',
        'values'  => array ( 'all','homepage','collezione','prodotto','carrello' ),
        'value_type' => 'string',
        'default' => '',
        'cols'    => 2
      ),
      'attivo' => array(
        'label'   => 'Attivo',
        'type'    => 'select',
        'values'  => array ( 'Non attivo','Attivo'),
        'default' => '',
        'cols'    => 1
      ),
      'url' => array(
        'label'   => 'link',
        'type'    => 'input',
        'default' => '',
        'cols'    => 8,
        'placeholder' => 'http://'
      ),
    )
  )
);
