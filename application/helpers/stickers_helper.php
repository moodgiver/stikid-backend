<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('calcolo_prezzo'))
{

    function common_data(){
      // Get a reference to the controller object
      $CI = get_instance();

      // You may need to load the model if it hasn't been pre-loaded
      $CI->load->model('Site_model');
      $CI->load->model('User_model');
      $data['header_hooks'] = $CI->Site_model->header_hooks();
      $data['collezioni'] = $CI->Site_model->collezioni();
      $data['collezioni_qpb'] = $CI->Site_model->collezioni_qpb();
      //$data['offerte'] = $CI->Site_model->offerte();
      $data['footer_1'] = $CI->Site_model->pagine_footer(1);
      $data['footer_2'] = $CI->Site_model->pagine_footer(2);
      $data['footer_3'] = $CI->Site_model->pagine_footer(3);
      $data['footer_4'] = $CI->Site_model->pagine_footer(4);

      return $data;

    }

    function set_period_1_month(){
      $data['from'] = date("Ymd", strtotime( date( "Ymd", strtotime( date("Ymd") ) ) . "-1 month" ) );
      $data['to'] = date("Ymd", strtotime( date( "Ymd")));
      return $data;
    }

    function imguri(){
      $CI = get_instance();
      return $CI->config->item('static_url');
    }

    function this_year(){
      return mdate('%Y',time());
    }

    function calcolo_prezzo($w,$h,$s)
    {
      // Get a reference to the controller object
      $CI = get_instance();

      // You may need to load the model if it hasn't been pre-loaded
      $CI->load->model('Site_model');
      $pricing = $CI->Site_model->pricing();
      $prezzoFinale = calcola_prezzo_finale($pricing,$w,$h,$s);
      /*
      $area_minima = (int)(($w*$h)/100);
      if ( $area_minima < 0.11 ){
        $area_minima = 0.11;
        $prezzoFinale = 0.11*200;
        return number_format((int)$prezzoFinale,2);
      } else {
        $cs = (float)$s;
      }
      $n = 0;
      foreach ( $pricing AS $pr ){
        if ( $area_minima < (float)$pr['ac_area'] ){
          $differenza_area = (float)$pr['ac_area']-(float)$area_minima;
          $prezzoMQDifferenza = $pr['ac_prezzo'];
          $prezzoPlus = ((float)$prezzoMQDifferenza*(float)$differenza_area)/100;
          $prezzoFinale = (float)$pricing[$n-1]['ac_prezzo_reale']+$prezzoPlus;
          $prezzoFinale = (float)$prezzoFinale*$cs;

          return number_format((int)$prezzoFinale,2);//.'->'.$area_minima.'->'.(float)$pr['ac_area'].'>'.$prezzoPlus;
          //.' <br> area = '.$area_minima.' <br>prezzo reale= '.$pricing[$n+1]['ac_prezzo_reale'].'<br>diff area = '.$differenza_area.'<br>prezzo MQ='.$prezzoPlus;
          break;

        }
        $n++;
        */
        return number_format($prezzoFinale,2);
      //}
    }

    function calcola_prezzo_finale($pricing,$w,$h,$s){
      $areaDiff = 0;
      $prezzoDiff = 0;
      $startingPrice = $pricing[0]['ac_prezzo_reale'];
      $area_minima = (int)((int)$w*(int)$h)/100;
      $prezzoFinale = $pricing[0]['ac_prezzo_reale'];
      $a = 0;
      $cs = 1;
      foreach ( $pricing AS $i ){
        //$nextArea = (int)$pricing[$a+1]['ac_area'];
        //$nextPrezzoMQ = $pricing[$a+1]['ac_prezzo'];
        $nextArea = (int)$i['ac_area'];
        $nextPrezzoMQ = $i['ac_prezzo'];

        if ( (float)$area_minima > (float)$i['ac_area']
              && $a > 0
              && (float)$area_minima < (float)$pricing[$a+1]['ac_area'] ) {
          $prevArea = (int)$pricing[$a-1]['ac_area'];
          $startingPrice = $pricing[$a-1]['ac_prezzo_reale'];
          $areaDiff = (int)$area_minima - (int)$pricing[$a-1]['ac_area'];
          //$startingPrice = $i['ac_prezzo_reale'];
          //$areaDiff = (int)$area_minima - (int)$i['ac_area'];
          $prezzoDiff = ((int)$areaDiff * (int)$nextPrezzoMQ)/100;
          $prezzoFinale = (int)((int)$startingPrice + $prezzoDiff)*$s;
          //echo $w.'x'.$h.'<br>'.$a.'->'.$area_minima.'->'.$prevArea.'<br>';
          //print_r($i);
					break;
        }
        $a++;
      }
      $prezzoFinale = abs($prezzoFinale);
      //if ( $prezzoFinale < 22 AND $s <> -1 ){
      //  $prezzoFinale = 22;
      //} else {
        if ( $prezzoFinale < 15 ){
          $prezzoFinale = 15;
        }
      //}
      return $prezzoFinale;
    }

    function uri_cat($c,$tipo){
      if ( $tipo == 0 ){
        return str_replace(' ','-',$c).'-wall-stickers';
      }
      if ( $tipo == 1 ){
        return str_replace(' ','-',$c).'-adesivi-murali';
      }
      if ( $tipo == 2 ){
        return str_replace(' ','-',$c).'-quadri-per-bambini';
      }
    }

    function uri_prodotto($categoria,$prodotto,$id,$tipo){
      if ( $tipo == 0 ){
        return str_replace(' ','-',$categoria).'/'.str_replace(' ','-',$prodotto).'-wall-sticker/'.$id;
      }
      if ( $tipo == 1 ){
        return str_replace(' ','-',$categoria).'/'.str_replace(' ','-',$prodotto).'-adesivo-murale/'.$id;
      }
      if ( $tipo == 2 ){
        return str_replace(' ','-',$categoria).'/'.str_replace(' ','-',$prodotto).'-quadro-per-bambino/'.$id;
      }
    }

    function prezzo_base_qpb($lista){
      // Get a reference to the controller object
      $CI = get_instance();

      // You may need to load the model if it hasn't been pre-loaded
      $CI->load->model('Site_model');
      $pricing = $CI->Site_model->prezzi_qpb($lista);
      return $pricing[0]['ac_prezzo'];
    }

    function calcolo_spedizioni($prezzo)
    {
      // Get a reference to the controller object
      $CI = get_instance();

      // You may need to load the model if it hasn't been pre-loaded
      $CI->load->model('Site_model');
      $pricing = $CI->Site_model->spedizioni();
      $spese = 0;

      foreach ( $pricing AS $price ){
        if ( (float)$price['ac_prezzo_max'] > (float)$prezzo  ){
          return (float)$price['ac_spedizione'];
          break;
        }
      }
      return $spese;
  	}

    function coupon($coupon)
    {
      // Get a reference to the controller object
      $CI = get_instance();
      // You may need to load the model if it hasn't been pre-loaded
      $CI->load->model('Site_model');
      $c = $CI->Site_model->coupon($coupon);
      return $c;
    }

    function ordine_nr(){
      // Get a reference to the controller object
      $CI = get_instance();

      // You may need to load the model if it hasn't been pre-loaded
      $CI->load->model('Order_model');
      $c = $CI->Order_model->numero_ordine();
      return $c;
    }

    function adesso(){
      $CI = get_instance();
      return date('Y-m-d H:i:s');
    }

    function calcolo_costo($data){
      $area = (float)$data['width']/100*(float)$data['height']/100;
      if ( $area <= $data['misura_minima'] ) {
          $costoSticker = $data['costo_minimo']*$area;
      } else {
        $costoSticker = $data['costo']*$area;
      }
      if ( $area > 1.11 ){
        if ( $data['colore'] == 1 ){
          $costoSticker = (float)$area * (float)$data['ac_costo'];
        } else {
          $costoSticker = (float)$area * (float)$data['ac_costo'];
        }
        if ( $costoSticker < 20 ){
          $costoSticker = 20;
        }
      } else {
        if  ( $data['colore'] != 1 ){
          $costoSticker = (float)$area * (float)$data['ac_costo_quadri'];
        } else {
          $costoSticker = (float)$area * (float)$data['ac_costo'];
        }
      }

      if ( $costoSticker < 6 && $_POST['id'] == 3 ){
        $costoSticker = 6;
      }
  		return $costoSticker;
    }

    function create_form($fields,$row,$multirow,$field_key){
      $n = 0;

      foreach ( $fields AS $key=>$value ){

        if ( $value['type'] == 'hidden' ){
          echo '<input type="hidden" class="'.$key.'" name="'.$key.'" value="'.$row[$key].'">';
        } else {

          if ( $value['type'] != 'attivo' ){
            echo '<div class="col-lg-'.$value['cols'].'"><label>'.$value['label'].'</label><input class="form-control '.$key.'" name="'.$key.'" type="'.$value['type'].'" value="'.$row[$key].'"></div>';
          } else {
            echo '<div class="col-lg-'.$value['cols'].'"><label>'.$value['label'].'</label>
            <select class="form-control '.$key.'" name="'.$key.'">';
            $select = '';
            $n = 0;
            foreach($stato AS $st){
              if ($row[$key] == $n ){
                $select = 'selected';
              }
              echo '<option value="'.$n.'" '.$select.'>'.$st.'</option>';
              $n++;
            }
            echo '</select></div>';
          }
        }
        if ( $multirow ){
          echo '<div class="col-lg-1"><button class="btn btn-primary btn-sm" data-id="'.$row[$field_key].'" data-action="">Salva</button>';
        }
        $n++;
      }
    }

    function _create_form($schema,$name,$row,$multirow,$n,$action){
      $table = $schema['database'][$name];
      $fields = $table['fields'];
      $field_key = $table['key'];
      $stato = ['Non attivo','Attivo' ];
      $route = explode('-',$action);
      //print_r($table);
      echo '<form action="ajax/'.$route[0].'" method="post" id="save-data-form">';
      echo '<input type="hidden" name="action" value="'.$action.'">';
      echo '<input type="hidden" name="table" value="'.$name.'">';
      foreach ( $fields AS $key=>$value ){
        //echo $key;table_html
        if ( $value['type'] == 'hidden' ){
          echo '<input type="hidden" class="'.$key.'" name="'.$key.'" value="'.$row[$key].'">';
        } else {

          if ( $value['type'] != 'attivo' ){
            if ( $value['type'] == 'select' ){
              $array = $value['values'];
              echo '<div class="col-lg-'.$value['cols'].'"><label>'.$value['label'].'</label>
              <select class="form-control '.$key.'" name="'.$key.'">';
              $x = 0;
              foreach ( $array AS $option ){
                $selected = '';
                if ( $row[$key] == $x ){
                  $selected = 'selected';
                }
                echo '<option value="'.$x.'" '.$selected.'>'.$option.'</option>';
                $x++;
              }
              echo '</select></div>';
            } else {
                        echo '<div class="col-lg-'.$value['cols'].'"><label>'.$value['label'].'</label><input class="form-control '.$key.'" name="'.$key.'" type="'.$value['type'].'" value="'.$row[$key].'"></div>';
            }
          } else {


            echo '<div class="col-lg-'.$value['cols'].'"><label>'.$value['label'].'</label>
            <select class="form-control '.$key.'" name="'.$key.'">';
            $select = '';
            $n = 0;
            foreach($stato AS $st){
              if ($row[$key] == $n ){
                $select = 'selected';
              }
              echo '<option value="'.$n.'" '.$select.'>'.$st.'</option>';
              $n++;
            }
            echo '</select></div>';
          }

        }

        $n++;
      }
      if ( $multirow ){
        echo '<div class="col-lg-1"><br><button class="btn btn-primary btn-sm btn-save-data" data-id="'.$row[$field_key].'" data-action="'.$action.'">Salva</button></div><div class="clearfix"></div>
        </form>';
      } else {
        echo '<div class="col-lg-12 text-right"><br><button class="btn btn-primary btn-sm btn-save-data" data-id="'.$row[$field_key].'" data-action="'.$action.'">Salva</button></div><div class="clearfix"></div></form>';
      }
    }

    function _create_form_record($schema,$name,$row,$multirow,$n,$action){
      //create a record edit form
      //@schema : database schema
      //@name: schema name
      //@row: record row
      //@multirow: multirow/single record
      //@n: current row (multirow)
      //@action: ajax action to save record
      $table = $schema['database'][$name];
      $fields = $table['fields'];
      $field_key = $table['key'];
      $stato = ['Non attivo','Attivo' ];
      $route = explode('-',$action);
      echo '<form action="ajax/'.$route[0].'" method="post" id="save-data-form-'.$n.'" style="margin-bottom:20px">';
      echo '<input type="hidden" name="action" value="'.$action.'">';
      echo '<input type="hidden" name="table" value="'.$name.'">';
      $hide = '';
      if ( $n > 0 ){
        $hide = 'hide';
      }

      foreach ( $fields AS $key=>$value ){
        $tipo = $value['type'];
        if ( $key == $field_key ){
          $id = $row[$key];
        }
        if ( $key == 'int_sito' ){
          $sito = $row[$key];
        }

        $placeholder = '';
        if ( isset($value['placeholder']) ){
          $placeholder = $value['placeholder'];
        }

        $dropzone = '';
        if ( isset($value['dropzone']) ){
          $dropzone = 'image-is-dropzone';
        }
        if ( isset ($value['path']) ){
          print_r($value['path'] );
        }
        switch ( $tipo ){

          case 'hidden':
            echo '<input type="hidden" class="'.$key.'" name="'.$key.'" value="'.$row[$key].'">';

          break;

          case 'input':
            echo '<div class="col-lg-'.$value['cols'].'"><label  class="'.$hide.'">'.$value['label'].'</label><input class="form-control '.$key.'" name="'.$key.'" type="'.$value['type'].'" value="'.$row[$key].'" placeholder="'.$placeholder.'"></div>';
          break;

          case 'select':
          $array = $value['values'];
          echo '<div class="col-lg-'.$value['cols'].'"><label class="'.$hide.'">'.$value['label'].'</label>
          <select class="form-control '.$key.'" name="'.$key.'">';
          $x = 0;
          foreach ( $array AS $option ){
            $selected = '';
            if ( !isset($value['value_type']) ){
              if ( $row[$key] == $x ){
                $selected = 'selected';
              }
              if ( $option != '' ){
                echo '<option value="'.$x.'" '.$selected.'>'.$option.'</option>';
              }
            } else {
              if ( $row[$key] == $option ){
                $selected = 'selected';
              }
              if ( $option != '' ){
                echo '<option value="'.$option.'" '.$selected.'>'.$option.'</option>';
              }
            }
            $x++;
          }
          echo '</select></div>';
          break;

          case 'image':
            echo '<div class="col-lg-'.$value['cols'].'"><label class="'.$hide.'">'.$value['label'].'</label><input class="form-control '.$key.'" name="'.$key.'" id="image-uri-'.$id.'" "type="hidden" value="'.$row[$key].'"><img src="'.$img_url.''.$row[$key].'" style="width:100%;height:auto;"/></div>';
          break;

          case 'image-uri':
            echo '<div class="col-lg-'.$value['cols'].'">
            <label class="'.$hide.'">'.$value['label'].'</label>
            <input class="form-control '.$key.'" name="'.$key.'" id="image-uri-'.$id.'" type="hidden" value="'.$row[$key].'">
            <img src="'.$value['uri'].''.$row[$key].'" style="width:100%;height:auto;" id="image-uri-'.$id.'" class="'.$dropzone.' image-uri-'.$id.'" data-id="image-uri-'.$id.'" data-type="'.$name.'" data-field="image-uri-'.$id.'" /></div>';
          break;
        }
      }
      if ( $multirow ){
        echo '<div class="col-lg-1"><label class="'.$hide.'">Azione</label><button class="btn btn-primary btn-sm btn-save-data" data-id="'.$n.'" data-action="'.$action.'">Salva</button></div><div class="clearfix"></div>
        </form>';
      } else {
        echo '<div class="col-lg-12 text-right"><br><button class="btn btn-primary btn-sm btn-save-data" data-id="'.$n.'" data-action="'.$action.'">Salva</button></div><div class="clearfix"></div></form>';
      }

    }

    function _create_empty_form_record($schema,$name,$multirow,$action){
      //create a new record empty form
      //@schema : database schema
      //@name: schema name
      //@multirow: multirow/single record
      //@action: ajax action to save record
      $n = 999;
      $table = $schema['database'][$name];
      $fields = $table['fields'];
      $field_key = $table['key'];
      $stato = ['Non attivo','Attivo' ];
      $route = explode('-',$action);
      echo '<form action="ajax/'.$route[0].'" class="hide new-record-form" method="post" id="save-data-form-'.$n.'" style="margin-bottom:20px">';
      echo '<input type="hidden" name="action" value="'.$action.'">';
      echo '<input type="hidden" name="table" value="'.$name.'">';
      $hide = '';
      foreach ( $fields AS $key=>$value ){
        $tipo = $value['type'];
        if ( $key == $field_key ){
          $id = 0;
        }

        $placeholder = '';
        if ( isset($value['placeholder']) ){
          $placeholder = $value['placeholder'];
        }

        $dropzone = '';
        if ( isset($value['dropzone']) ){
          $dropzone = 'image-is-dropzone';
        }

        switch ( $tipo ){

          case 'hidden':
            if ( $key != $field_key ){
              echo '<input type="hidden" class="'.$key.'" name="'.$key.'" value="'.$value['default'].'">';
            }
          break;

          case 'input':
            echo '<div class="col-lg-'.$value['cols'].'"><label  class="'.$hide.'">'.$value['label'].'</label><input class="form-control '.$key.'" name="'.$key.'" type="'.$value['type'].'" value="'.$value['default'].'" placeholder="'.$placeholder.'"></div>';
          break;

          case 'select':
          $array = $value['values'];
          echo '<div class="col-lg-'.$value['cols'].'"><label class="'.$hide.'">'.$value['label'].'</label>
          <select class="form-control '.$key.'" name="'.$key.'">';
          $x = 0;
          foreach ( $array AS $option ){
            $selected = '';
            if ( !isset($value['value_type']) ){
              if ( $option != '' ){
                echo '<option value="'.$x.'" '.$selected.'>'.$option.'</option>';
              }
            } else {
              if ( $option != '' ){
                echo '<option value="'.$option.'" '.$selected.'>'.$option.'</option>';
              }
            }
            $x++;
          }
          echo '</select></div>';
          break;

          case 'image':
            echo '<div class="col-lg-'.$value['cols'].'"><label class="'.$hide.'">'.$value['label'].'</label><input class="form-control '.$key.'" name="'.$key.'" id="image-uri-'.$id.'" type="hidden" value=""><img src="public/img/noimage.png" style="width:100%;height:auto;"/></div>';
          break;

          case 'image-uri':
            echo '<div class="col-lg-'.$value['cols'].'">
            <label class="'.$hide.'">'.$value['label'].'</label>
            <input class="form-control '.$key.'" name="'.$key.'" id="image-uri-'.$id.'" type="hidden" value="'.$value['default'].'">
            <img src="public/img/noimage.png" style="width:100%;height:auto;" id="image-uri-'.$id.'" class="'.$dropzone.' image-uri-'.$id.'" data-id="image-uri-'.$id.'" data-type="'.$name.'" data-field="image-uri-'.$id.'" /></div>';
          break;
        }
      }
      if ( $multirow ){
        echo '<div class="col-lg-1"><label class="'.$hide.'">Azione</label><button class="btn btn-primary btn-sm btn-save-data" data-id="'.$n.'" data-action="'.$action.'">Salva</button></div><div class="clearfix"></div>
        </form>';
      } else {
        echo '<div class="col-lg-12 text-right"><br><button class="btn btn-primary btn-sm btn-save-data" data-id="'.$n.'" data-action="'.$action.'">Salva</button></div><div class="clearfix"></div></form>';
      }

    }

    function array_group_by(array $array, $key)
	{
		if (!is_string($key) && !is_int($key) && !is_float($key) && !is_callable($key) ) {
			trigger_error('array_group_by(): The key should be a string, an integer, or a callback', E_USER_ERROR);
			return null;
		}
		$func = (!is_string($key) && is_callable($key) ? $key : null);
		$_key = $key;
		// Load the new array, splitting by the target key
		$grouped = [];
		foreach ($array as $value) {
			$key = null;
			if (is_callable($func)) {
				$key = call_user_func($func, $value);
			} elseif (is_object($value) && isset($value->{$_key})) {
				$key = $value->{$_key};
			} elseif (isset($value[$_key])) {
				$key = $value[$_key];
			}
			if ($key === null) {
				continue;
			}
			$grouped[$key][] = $value;
		}
		// Recursively build a nested grouping if more parameters are supplied
		// Each grouped array value is grouped according to the next sequential key
		if (func_num_args() > 2) {
			$args = func_get_args();
			foreach ($grouped as $key => $value) {
				$params = array_merge([ $value ], array_slice($args, 2, func_num_args()));
				$grouped[$key] = call_user_func_array('array_group_by', $params);
			}
		}
		return $grouped;
	}

  function application_log_read(){
    $CI = get_instance();
    $path = '/home/admin/codeigniter/stikid_dev/stikid/application/logs/';
    $filename = 'applog_'.date('Y-m-d').'.log';
    echo '<h3>LOG '.$filename.'</h3>';
    $logfile = $path.''.$filename;
    if ( file_exists($logfile) ){
      if ($file = fopen($path.''.$filename, "r")) {
        while(!feof($file)) {
          $line = fgets($file);
          echo $line.'<br>';
        }
        fclose($file);
      }  
    }
  }


}
