<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function general_email_sender($data){

  $CI = get_instance();
  print_r($data);
  $titolo = $data['titolo'];
  $from = $data['from'];
  $to = $data['to'];
  $bcc = $data['bcc'];
  $subject = $data['subject'];
  $msg = $data['msg'];
  $ordine_nr = $data['ordine'];
  $CI->load->library('email');
  $CI->email->from($from, 'STICASA / STIKID');
  $CI->email->bcc($bcc);
  $CI->email->to($to);
  $CI->email->subject($subject);
  $CI->email->message($msg);
  $result = $CI->email->send();
  //$result = 1;
  $params = array(
    'tipo'    => $titolo,
    'email'   => $to,
    'ordine'  => $ordine_nr,
    'subject' => $subject,
    'message' => $msg,
    'status'  => $result
  );
  $CI->db->insert('tbl_emails_logs',$params);
  return $result;
}

function email_sender($data,$sendto,$ordine_nr){
  $CI = get_instance();
  $from = $data[0]['email_from'];
  $bcc = $data[0]['email_bcc'];
  $subject = $data[0]['email_subject'];

    $message_template = $data[0]['template'];
    $msg = str_replace('_nr_ordine_',$ordine_nr,$message_template);
    $subject = str_replace('_nr_ordine_',$ordine_nr,$subject);
    $CI->load->library('email');
    $CI->email->from($from, 'STICASA / STIKID');
    //$CI->email->bcc($bcc);
    $CI->email->to($sendto);
    $CI->email->subject($subject);
    $CI->email->message($msg);
    $result = $CI->email->send();
    $params = array(
      'tipo'    => $data[0]['titolo'],
      'email'   => $sendto,
      'ordine'  => $ordine_nr,
      'subject' => $subject,
      'message' => $msg,
      'status'  => $result
    );
    $CI->db->insert('tbl_emails_logs',$params);
    return $result;

}
