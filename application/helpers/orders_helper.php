<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



    function send_order_confirm($pagamento){
      // Get a reference to the controller object
      $CI = get_instance();

      // You may need to load the model if it hasn't been pre-loaded
      $CI->load->model('Site_model');
      $CI->load->model('User_model');

      $CI->load->library('email'); // load email library

      $ordine = $CI->session->order;
      $cart = $CI->session->cart_items;
      $CI->email->from('servizioclienti@sticasa.com', 'STICASA / STIKID');
  		$CI->email->bcc('sticasa@sticasa.com');
  		$CI->email->to($CI->session->user['email']);
  		$CI->email->subject("ORDINE STICASA/STIKID ".$ordine['id'].'/'.this_year());

      $content = '';

      foreach ( $ordine AS $itm ){
        $imgURL = 'http://cdn.stikid.com/images/ambienti/small/';
        $telaio = '';
        if ( $itm['telaio'] != '' ){
          $imgURL = 'http://cdn.stikid.com/images/canvas/preview/';
          $telaio = '<br>Telaio '.$itm['telaio'].' cm';
        }
        $totale_riga = number_format((float)$itm['qty']*(float)$itm['prezzo'],2);

        $content .= '
            <tr>
              <td width="110">
                <img src="'.$imgURL.''.$itm['thumb'].'" width="100">
              </td>
              <td style="font-size:1em;font-family:Verdana,Helvetica" valign="top">'.$itm['prodotto'].'<br><small>Collezione: '.$itm['collezione'].'</small>'.str_replace('|','<br>',$itm['testo']).'</td>
              <td style="font-size:1em;font-family:Verdana,Helvetica" valign="top">'.$itm['colore'].'</td>
              <td style="font-size:1em;font-family:Verdana,Helvetica" valign="top"><cfif itm.telaio EQ "">'.$itm['w'].' x '.$itm['h'].''.$telaio.'</td>
              <td style="font-size:1em;font-family:Verdana,Helvetica" valign="top" align="center">'.$itm['qty'].'</td>
              <td style="font-size:1em;font-family:Verdana,Helvetica" valign="top" align="right">'.$itm['prezzo'].'</td>
              <td style="font-size:1em;font-family:Verdana,Helvetica" valign="top" align="right">'.$totale_riga.'</td>
            </tr>
            <tr>
              <td colspan="7"><hr></td>
            </tr>';
      }


      $message = '
      <!DOCUMENT html>
    	<html>
    	<head>
    		<link href="http://www.stikid.com/css/style.css" rel="stylesheet">
    		<style>
    			td , th , tbody , thead { font-family:Verdana,Helvetica; font-size:1em;}
    			.col-center { text-align:center; }
    			.col-right { text-align:right; }
    		</style>
    	</head>
    	<body style="background:#fff">
    	<img src="http://cdn.stikid.com/images/email-logo.jpg" alt="Home" title="Home"/>
    	<h2>STICASA/STIKID Ordine nr. '.$ordine['id'].'/'.this_year().'</h2>
    	<table width="100%" cellpadding="4" cellspacing="0" border="0" style="border:1px solid ##cecece;background:#fff">
    		<thead style="background:##cecece">
    			<th style="font-size:1em;font-family:Verdana,Helvetica;padding:5px" valign="top"><strong>Prodotto</strong></th>
    			<th style="font-size:1em;font-family:Verdana,Helvetica;padding:5px" valign="top"></th>
    			<th style="font-size:1em;font-family:Verdana,Helvetica;padding:5px" valign="top"><strong>Colore</strong></th>
    			<th style="font-size:1em;font-family:Verdana,Helvetica;padding:5px" valign="top"><strong>LxA cm</strong></th>
    			<th style="font-size:1em;font-family:Verdana,Helvetica;padding:5px" valign="top"><strong>Q.ta\'</strong></th>
    			<th style="font-size:1em;font-family:Verdana,Helvetica;padding:5px" valign="top"><strong>P.U.</strong></th>
    			<th style="font-size:1em;font-family:Verdana,Helvetica;padding:5px" valign="top"><strong>Totale</strong></th>
    		</thead>
    		<tbody class="print">';
            $message .= $content;

            $message .= '
    				<tr>
    					<td colspan="6" align="right" style="font-size:1em;font-family:Verdana,Helvetica" valign="top">Totale</td>
    					<td style="font-size:1em;font-family:Verdana,Helvetica" valign="top" align="right">'.number_format($ordine['CART_TOTAL'],2).'</td>
    				</tr>
    				<tr>
    					<td colspan="6" style="font-size:1em;font-family:Verdana,Helvetica" valign="top" align="right">Coupon</td>
    					<td style="font-size:1em;font-family:Verdana,Helvetica" valign="top" align="right">'.number_format($ordine['COUPON_VALUE'],2).'</td>
    				</tr>
    				<tr>
    					<td colspan="6" style="font-size:1em;font-family:Verdana,Helvetica" valign="top" align="right">Totale</td>
    					<td style="font-size:1em;font-family:Verdana,Helvetica" valign="top" align="right">'.number_format($ordine['SHIPPING'],2).'</td>
    				</tr>
    				<tr>
    					<td colspan="6" style="font-size:1em;font-family:Verdana,Helvetica" valign="top" align="right">Totale</td>
    					<td style="font-size:1em;font-family:Verdana,Helvetica" valign="top" align="right">'.number_format($ordine['ORDER_TOTAL'],2).'</td>
    				</tr>
    				<tr>
    					<td colspan="7"><hr></td>
    				</tr>

    		</tbody>
    		<tfoot class="print-footer">
    			<tr>
    				<td colspan="7" style="font-size:1em;font-family:Verdana,Helvetica" valign="top"><h3>Cliente</h3>
            '.$ordine['O_NOME'].'&nbsp;'.$ordine['O_COGNOME'].'<br>
    				'.$ordine['O_INDIRIZZO'].'<br>
    				'.$ordine['O_CAP'].' '.$ordine['O_CITTA'].' '.$ordine['O_PV'].' '.$ordine['O_NAZIONE'].'<br>
    				Telefono: '.$ordine['O_TELEFONO'].'<br>
    				Cellulare: '.$ordine['O_MOBILE'].'
    				</td>
    			</tr>';
          if ( $ordine['SHIPTO'] ){
            $message .= '
    				<tr>
            <td colspan="7" style="font-size:1em;font-family:Verdana,Helvetica" valign="top"><h3>Spedisci a</h3>
            '.$ordine['OS_NOME'].' '.$ordine['OS_COGNOME'].'<br>
            '.$ordine['OS_INDIRIZZO'].'<br>
            '.$ordine['OS_CAP'].' '.$ordine['OS_CITTA'].' '.$ordine['OS_PV'].' '.$ordine['OS_NAZIONE'].'<br>
            Telefono: '.$ordine['OS_TELEFONO'].'<br>
            Cellulare: '.$ordine['OS_MOBILE'].'
            </td>
    			  </tr>';
          }
    			$message .= '<tr>
    				<td colspan="7" style="font-size:1em;font-family:Verdana,Helvetica" valign="top">
    				<br><br>
    				Per qualsiasi richiesta o chiarimento si prega di contattare l\'assistenza clienti scrivendo un\'e-mail a  servizioclienti@sticasa.com e indicando il numero d\'ordine nr. '.$ordine['id'].'/'.this_year().'
    				</td>
    			</tr>';
          if ( $pagamento == 'BONIFICO' ){
    			  $message .= '
              <tr>
    				    <td colspan="7"  style="font-size:1em;font-family:Verdana,Helvetica" valign="top">
                <small>Dati Bonifico (per pagamenti tramite bonifico)<br>
                <strong>DUAL SRL - IBAN IT64Q0200801634000102842164 - UNICREDIT</strong><br>
    					  Indicare nel bonifico</br>
    					  ORDINE STICASA/STIKID NR. '.$ordine['id'].'/'.this_year().'</small>
    				    </td>
    			    </tr>';
          } else {
            $message .= '
            <tr>
            <td colspan="7"  style="font-size:1em;font-family:Verdana,Helvetica" valign="top">
            PAGAMENTO: CC/PAYPAL
            </td>
            </tr>';
          }
      $message .= '</tfoot>
    	</table>
    	</body>
    	</html>';
      $CI->email->message($message);
  		$noerr = $CI->email->send();
      if ( $noerr ){
        return true;
      } else {
        return $noerr;
      }
    }

    function email_ordine_fornitore($ordine){
      // Get a reference to the controller object
      $CI = get_instance();
      $CI->load->model('App_model');
      $CI->load->library('email'); // load email library

      $email_hook = $CI->App_model->email_hook('email_conferma_pagamento');
      $send_to = 'swina.allen@gmail.com';

      if ( count($email_hook) > 0 ){
        $data = array (
          'dt_lavorazione' => date('Y-m-d')
        );
        $CI->db->where('ac_nrordine',$ordine[0]['ac_nrordine']);
        $r = $CI->db->update('tbl_ordini_summary',$data);
        $ordine_nr = $ordine[0]['ac_nrordine'];
        $send_to = $ordine[0]['cliente_email'];
        email_sender($email_hook,$send_to,$ordine_nr);
      }

      $subject = "ORDINE LAVORAZIONE STICASA/STIKID ".$ordine[0]['ac_nrordine'];

      $message = '';
      $content = '';
      foreach ( $ordine AS $itm ){
        $imgURL = 'http://cdn.stikid.com/images/ambienti/small/';
        $telaio = '';
        if ( $itm['ac_telaio'] != '' ){
          $imgURL = 'http://cdn.stikid.com/images/canvas/preview/';
          $telaio = '<br>Telaio '.$itm['ac_telaio'].' cm';
        }
        $content .= '
        <tr>
          <td valign="top" style="border-bottom:1px solid ##cecece">
          <img src="'.$imgURL.''.$itm['ac_immagine'].'" width="120">
          </td>
          <td valign="top" style="border-bottom:1px solid ##cecece">
          '.$itm['ac_codice_prodotto'].'
          </td>
          <td valign="top" style="border-bottom:1px solid ##cecece">
          '.$itm['ac_prodotto'].'
          </td>
          <td valign="top" style="border-bottom:1px solid ##cecece">
          '.$itm['ac_colore'].'
          </td>
          <td valign="top" style="border-bottom:1px solid ##cecece">
          '.$itm['ac_width'].'
          '.$telaio.'
          </td>
          <td valign="top" style="border-bottom:1px solid ##cecece">
          '.$itm['ac_height'].'
          </td>
          <td valign="top" style="border-bottom:1px solid ##cecece">
          '.$itm['ac_qty'].'
          </td>
          <td valign="top" style="border-bottom:1px solid ##cecece">
          Vettoriale <a href="'.$CI->config->item('static_url').'/images/vettoriali/'.$itm['ac_sticker'].'">'.$itm['ac_sticker'].'</a>
          </td>
        </tr>';
      }

      $message .= '
      <!DOCTYPE html>
    	<html>
    	<head>
    		<link href="http://www.stikid.com/public/css/style.css" rel="stylesheet">
    		<style>
    			td , th , tbody , thead { font-family:Verdana,Helvetica; font-size:1em;}
    			.col-center { text-align:center; }
    			.col-right { text-align:right; }
    		</style>
    	</head>
    	<body style="background:#fff">
      <table style="border:1px black solid" cellpadding="10" cellspacing="4" width="100%">
        <thead>
        <tr>
          <th colspan="8" style="background:#eaeaea;color:black">
          STICASA/STIKID ORDINE NR '.$ordine[0]['ac_nrordine'].'
          </th>
        </tr>
        <tr style="background:#eaeaea;color:black">
          <th>
          Immagine
          </th>
          <th>
          Codice
          </th>
          <th>
          Prodotto
          </th>
          <th>
          Colore
          </th>
          <th>
          L cm
          </th>
          <th>
          H cm
          </th>
          <th>
          Q.t&agrave;
          </th>
          <th>
          Vettoriale
          </th>
        </tr>
        </thead>


        <tbody>';
        $message .= $content;
        $message .= '
            <tr>
              <td colspan="4" valign="top">
              <strong>Fatturare a</strong><br>
              <br>
              <strong>'.$ordine[0]['ac_nome'].' '.$ordine[0]['ac_cognome'].'</strong><br>
              '.$ordine[0]['ac_indirizzo'].'<br>
              '.$ordine[0]['ac_cap'].' '.$ordine[0]['ac_citta'].''.$ordine[0]['ac_pv'].'
              </td>
              <td colspan="4" valign="top">
              <strong>Spedire a</strong><br>
              <br>';

            if ( $ordine[0]['bl_delivery_delivery'] ){
              $message .= '
              <strong>'.$ordine[0]['ac_firstname_delivery'].' '.$ordine[0]['ac_lastname_delivery'].'</strong><br>
              '.$ordine[0]['ac_company_delivery'].'<br>
              '.$ordine[0]['ac_address_delivery'].'<br>
              '.$ordine[0]['ac_zip_delivery'].' '.$ordine[0]['ac_city_delivery'].'  '.$ordine[0]['ac_state_delivery'].'<br>
              <br>
              Telefono '.$ordine[0]['ac_phone_delivery'].' / '.$ordine[0]['ac_mobile_delivery'].'<br>';
            } else {
              $message .= '
              (stesso indirizzo fattura)
              </cfif>
              </td>';
            }
            $message .= '
            </tr>
        </tbody>
      </table>
      Inviare a fornitore: '.$ordine[0]['fornitore_email'].'<br>
      Conferma pagamento: '.$ordine[0]['ac_email'].'
      </body>
      </html>';
      //$to = "swina.allen@gmail.com";
      $to = $ordine[0]['fornitore_email'];
      $CI->email->from('servizioclienti@sticasa.com', 'STICASA / STIKID');
  		$CI->email->bcc('sticasa@sticasa.com');
      $CI->email->to($to);
      $CI->email->subject($subject);
      $CI->email->message($message);
  		$noerr = $CI->email->send();

      if ( $noerr ){
        $params = array(
          'tipo'    => 'ORDINE LAVORAZIONE',
          'email'   => $to,
          'ordine'  => $ordine[0]['ac_nrordine'],
          'subject' => $subject,
          'message' => $message,
          'status'  => 1
        );
        $CI->db->insert('tbl_emails_logs',$params);
        return true;
      } else {
        $params = array(
          'tipo'    => 'ORDINE LAVORAZIONE',
          'email'   => $to,
          'ordine'  => $ordine[0]['ac_nrordine'],
          'subject' => $subject,
          'message' => $message,
          'status'  => 0
        );
        $CI->db->insert('tbl_emails_logs',$params);
        return $noerr;
      }
    }

    function email_tracking_cliente(){
      $CI = get_instance();
      $CI->load->model('App_model');
      $ordini = $CI->App_model->ordini_tracking();
      $CI->load->library('email'); // load email library

      $CI->email->from('servizioclienti@sticasa.com', 'STICASA / STIKID');
      $CI->email->bcc('sticasa@sticasa.com');
      print_r($ordini);
      if ( count($ordini) > 0 ){

      foreach ( $ordini AS $ordine ){
        $nrordine = explode('/',$ordine['ac_nrordine']);
        $subject = "Spedizione ordine STICASA/STIKID ".$ordine['ac_nrordine'];
        $CI->email->to($ordine['ac_email']);
        $CI->email->subject($subject);
        $message = '
        <!DOCUMENT html>
      	<html>
      	<head>
      		<link href="http://www.stikid.com/public/css/style.css" rel="stylesheet">
      		<style>
      			td , th , tbody , thead { font-family:Verdana,Helvetica; font-size:1em;}
      			.col-center { text-align:center; }
      			.col-right { text-align:right; }
      		</style>
      	</head>
      	<body style="background:#fff">
      	<img src="http://static.stikid.com/images/email-logo.jpg" alt="Home" title="Home"/>
        <table width="650" cellpadding="4" cellspacing="0" border="0" style="border:1px solid black">
        <tr>
        <td>
        Gentile Cliente, il tuo ordine e\' stato spedito. <br><br> Ti ricordiamo che le istruzioni per l\'applicazione del tuo sticker sono disponibili sul nostro sito sia in video che in file scaricabile.<br><br>Buon divertimento!<br><br>Lo STAFF STICASA/STIKID
        <br>
        <br>
        Nelle prossime ore potrai verificare il tracking del tuo ordine qui<br>
        <a href="http://www.gls-italy.com/track_trace_user.asp?locpartenza=m1&numbda='.$nrordine[0].'&tiporicerca=numbda&codice=4521&cl=1">Stato spedizione ordine</a>
        </td>
        </tr>
        </table>
        ';
        $CI->email->message($message);
        $noerr = $CI->email->send();
        $stat = 1;
        if ( !$noerr ){
          $stat = 0;
        } else {
          //aggiorna stato ordine
          $CI->App_model->set_ordine_spedito($ordine['ac_nrordine']);
        }
        $params = array(
          'tipo'    => 'AVVISO SPEDIZIONE',
          'email'   => $ordine['ac_email'],
          'ordine'  => $ordine['ac_nrordine'],
          'subject' => $subject,
          'message' => $message,
          'status'  => $stat
        );
        $CI->db->insert('tbl_emails_logs',$params);
      }
    }
    }
